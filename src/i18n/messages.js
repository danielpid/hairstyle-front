import { defineMessages } from 'react-intl';

const messages = defineMessages({
    actualizando: {
        id: 'actualizando',
        defaultMessage: 'Actualizando...'
    },
    anterior: {
        id: 'anterior',
        defaultMessage: 'Anterior'
    },
    agenda: {
        id: 'agenda',
        defaultMessage: 'Agenda'
    },
    agendaXs: {
        id: 'agenda.xs',
        defaultMessage: 'A'
    },
    anadirCliente: {
        id: 'anadir.cliente',
        defaultMessage: 'Añadir cliente'
    },
    anadirProducto: {
        id: 'anadir.producto',
        defaultMessage: 'Añadir producto'
    },
    anadirServicio: {
        id: 'anadir.servicio',
        defaultMessage: 'Añadir servicio'
    },
    ayuda: {
        id: 'ayuda',
        defaultMessage: 'Ayuda'
    },
    ayudaLinkYoutube: {
        id: 'ayuda.link.youtube',
        defaultMessage: 'https://www.youtube.com/watch?v=9BazuxXxGUE&list=PLNUo2kMnovWIAVetMBEfYi5mhmrjBAER7&index=4'
    },
    borrando: {
        id: 'borrando',
        defaultMessage: 'Borrando...'
    },
    buscando: {
        id: 'buscando',
        defaultMessage: 'Buscando...'
    },
    buscar: {
        id: 'buscar',
        defaultMessage: 'Buscar'
    },
    caja: {
        id: 'caja',
        defaultMessage: 'Caja'
    },
    cambiarEmail: {
        id: 'cambiar.email',
        defaultMessage: 'Cambiar correo electrónico'
    },
    cambiarPassword: {
        id: 'cambiar.password',
        defaultMessage: 'Cambiar contraseña'
    },
    cancelar: {
        id: 'cancelar',
        defaultMessage: 'Nombre'
    },
    cantidad: {
        id: 'cantidad',
        defaultMessage: 'Cantidad'
    },
    cantidadIncorrecta: {
        id: 'cantidad.incorrecta',
        defaultMessage: 'Cantidad incorrecta'
    },
    casa: {
        id: 'casa',
        defaultMessage: 'Casa'
    },
    cerrar: {
        id: 'cerrar',
        defaultMessage: 'Cerrar'
    },
    cerrarSesion: {
        id: 'cerrar.sesion',
        defaultMessage: 'Cerrar sesión'
    },
    cita: {
        id: 'cita',
        defaultMessage: 'Cita'
    },
    citaActualizada: {
        id: 'cita.actualizada',
        defaultMessage: 'Cita actualizada correctamente'
    },
    citaBorrada: {
        id: 'cita.borrada',
        defaultMessage: 'Cita borrada correctamente'
    },
    citaCreada: {
        id: 'cita.creada',
        defaultMessage: 'Cita creada correctamente'
    },
    cliente: {
        id: 'cliente',
        defaultMessage: '{num, plural, one {cliente} other {clientes}}'
    },
    clienteActualizado: {
        id: 'cliente.actualizado',
        defaultMessage: 'Cliente actualizado correctamente'
    },
    clienteBorrarValor: {
        id: 'cliente.borrar.valor',
        defaultMessage: 'Borrar cliente'
    },
    clienteCreado: {
        id: 'cliente.creado',
        defaultMessage: 'Cliente creado correctamente'
    },
    clienteNombre: {
        id: 'cliente.nombre',
        defaultMessage: 'Cliente'
    },
    clienteObligatorio: {
        id: 'cliente.obligatorio',
        defaultMessage: 'Introduzca un cliente'
    },
    clientes: {
        id: 'clientes',
        defaultMessage: 'Clientes'
    },
    clientesBorrado: {
        id: 'clientes.borrados',
        defaultMessage: 'Clientes borrados correctamente'
    },
    clientesNoBorrados: {
        id: 'clientes.no.borrados',
        defaultMessage: 'Los siguientes clientes no se pudieron borrar por tener citas o facturas: {nombresClientes}'
    },
    confirmarBorrado: {
        id: 'confirmar.borrado',
        defaultMessage: 'Confirmar borrado'
    },
    confirmarBorradoMensaje: {
        id: 'confirmar.borrado.mensaje',
        defaultMessage: 'Va a borrar {num} {elementos}, ¿desea continuar?'
    },
    crearCliente: {
        id: 'crear.cliente',
        defaultMessage: 'Crear cliente'
    },
    crearCuenta: {
        id: 'crear.cuenta',
        defaultMessage: 'Registrarse'
    },
    crearProducto: {
        id: 'crear.producto',
        defaultMessage: 'Crear producto'
    },
    crearServicio: {
        id: 'crear.servicio',
        defaultMessage: 'Crear servicio'
    },
    credencialesInvalidas: {
        id: 'credenciales.invalidas',
        defaultMessage: 'Correo electrónico o contraseña incorrecta'
    },
    descripcion: {
        id: 'descripcion',
        defaultMessage: 'Descripción'
    },
    dia: {
        id: 'dia',
        defaultMessage: 'Día'
    },
    diaXs: {
        id: 'dia.xs',
        defaultMessage: 'D'
    },
    diaObligatorio: {
        id: 'dia.obligatorio',
        defaultMessage: 'Día obligatorio'
    },
    editar: {
        id: 'editar',
        defaultMessage: 'Editar'
    },
    editarCliente: {
        id: 'editar.cliente',
        defaultMessage: 'Editar cliente'
    },
    editarProducto: {
        id: 'editar.producto',
        defaultMessage: 'Editar producto'
    },
    editarServicio: {
        id: 'editar.servicio',
        defaultMessage: 'Editar servicio'
    },
    eliminar: {
        id: 'eliminar',
        defaultMessage: 'Eliminar'
    },
    eliminarClientes: {
        id: 'eliminar.clientes',
        defaultMessage: 'Eliminar clientes'
    },
    eliminarFactura: {
        id: 'eliminar.factura',
        defaultMessage: 'la factura de {nombreCliente}'
    },
    eliminarProductos: {
        id: 'eliminar.productos',
        defaultMessage: 'Eliminar productos'
    },
    eliminarServicios: {
        id: 'eliminar.servicios',
        defaultMessage: 'Eliminar servicios'
    },
    email: {
        id: 'email',
        defaultMessage: 'Correo electrónico'
    },
    emailEnUso: {
        id: 'email.en.uso',
        defaultMessage: 'Correo electrónico ya registrado. Pruebe otro'
    },
    emailIncorrecto: {
        id: 'email.incorrecto',
        defaultMessage: 'Correo electrónico incorrecto',
    },
    emailObligatorio: {
        id: 'email.obligatorio',
        defaultMessage: 'Introduzca un correo electrónico de contacto'
    },
    emailRepita: {
        id: 'email.repita',
        defaultMessage: 'Confirme el correo electrónico'
    },
    emailRepitaDiferente: {
        id: 'email.repita.diferente',
        defaultMessage: 'Las direcciones de correo electrónico no coinciden'
    },
    emailRepitaObligatorio: {
        id: 'email.repita.obligatorio',
        defaultMessage: 'Introduzca el correo electrónico de nuevo'
    },
    emailResetPasswordSent: {
        id: 'email.reset.password.sent',
        defaultMessage: 'El correo electrónico para restablecer la contraseña se envió correctamente'
    },
    entrar: {
        id: 'entrar',
        defaultMessage: 'Entrar'
    },
    enviando: {
        id: 'enviando',
        defaultMessage: 'Enviando'
    },
    envianosEmail: {
        id: 'envianos.email',
        defaultMessage: 'Envíanos un correo'
    },
    enviar: {
        id: 'enviar',
        defaultMessage: 'Enviar'
    },
    errorGenerico: {
        id: 'error.generico',
        defaultMessage: 'Se ha producido un error'
    },
    errorConnection: {
        id: 'error.connection',
        defaultMessage: 'Error de conexión con el servidor. Inténtelo de nuevo más tarde'
    },
    evento: {
        id: 'evento',
        defaultMessage: 'Evento'
    },
    factura: {
        id: 'factura',
        defaultMessage: 'Factura'
    },
    facturaActualizada: {
        id: 'factura.actualizada',
        defaultMessage: 'Producto actualizado correctamente'
    },
    facturaBorrada: {
        id: 'factura.borrada',
        defaultMessage: 'Factura borrada correctamente'
    },
    facturaCreada: {
        id: 'factura.creada',
        defaultMessage: 'Factura creada correctamente'
    },
    fecha: {
        id: 'fecha',
        defaultMessage: 'Fecha'
    },
    guardando: {
        id: 'guardando',
        defaultMessage: 'Guardando...'
    },
    guardar: {
        id: 'guardar',
        defaultMessage: 'Guardar'
    },
    hairstyleDescripcion: {
        id: 'hairstyle.descripcion',
        defaultMessage: 'HairStyle es una aplicación web gratuita que te permite gestionar tu peluquería de forma fácil y sencilla. Mediante ella podrás gestionar tus citas, tu facturación, tus clientes, tus productos y servicios. '
    },
    hairstyleVerVideo: {
        id: 'hairstyle.ver.video',
        defaultMessage: '¿Quieres ver un vídeo demostrativo?'
    },
    hora: {
        id: 'hora',
        defaultMessage: 'Hora'
    },
    horaCreacion: {
        id: 'hora.creacion',
        defaultMessage: 'Hora de creación'
    },
    horaInicio: {
        id: 'hora.inicio',
        defaultMessage: 'Hora de inicio'
    },
    horaInicioMayorFin: {
        id: 'hora.inicio.mayor.fin',
        defaultMessage: 'La hora de inicio es mayor que la de fin'
    },
    horaInicioObligatorio: {
        id: 'hora.inicio.obligatorio',
        defaultMessage: 'Introduzca una hora de inicio'
    },
    horaFin: {
        id: 'hora.fin',
        defaultMessage: 'Hora de fin'
    },
    horaFinObligatorio: {
        id: 'hora.fin.obligatorio',
        defaultMessage: 'Introduzca una hora de fin'
    },
    horario: {
        id: 'horario',
        defaultMessage: 'Horario'
    },
    horarioActualizado: {
        id: 'horario.actualizado',
        defaultMessage: 'Horario actualizado'
    },
    hoy: {
        id: 'hoy',
        defaultMessage: 'Hoy'
    },
    hoyXs: {
        id: 'hoy.xs',
        defaultMessage: 'H'
    },
    inicio: {
        id: 'inicio',
        defaultMessage: 'Inicio',
    },
    informacionActualizada: {
        id: 'informacion.actualizada',
        defaultMessage: 'Información actualizada correctamente'
    },
    invalidEmail: {
        id: 'invalid.email',
        defaultMessage: 'La dirección de correo dada no existe'
    },
    invalidToken: {
        id: 'invalid.token',
        defaultMessage: 'El email de reseteo de contraseña es incorrecto. Pruebe a enviar uno de nuevo'
    },
    limpiar: {
        id: 'limpiar',
        defaultMessage: 'Limpiar'
    },
    longitudMaxima: {
        id: 'longitud.maxima',
        defaultMessage: 'Longitud máxima {limit}'
    },
    medioComunicacion: {
        id: 'medio.comunicacion',
        defaultMessage: 'Medio comunicación'
    },
    mes: {
        id: 'mes',
        defaultMessage: 'Mes'
    },
    mesXs: {
        id: 'mes.xs',
        defaultMessage: 'M'
    },
    moneda: {
        id: 'moneda',
        defaultMessage: 'Moneda'
    },
    monedaObligatoria: {
        id: 'moneda.obligatoria',
        defaultMessage: 'Introduzca una moneda'
    },
    mostrarTotalDiario: {
        id: 'mostrar.total.diario',
        defaultMessage: 'Mostrar total diario'
    },
    movil: {
        id: 'movil',
        defaultMessage: 'Móvil'
    },
    no: {
        id: 'no',
        defaultMessage: 'No'
    },
    nombre: {
        id: 'nombre',
        defaultMessage: 'Nombre'
    },
    nombreObligatorio: {
        id: 'nombre.obligatorio',
        defaultMessage: 'Introduzca un nombre'
    },
    nombrePeluqueria: {
        id: 'nombre.peluqueria',
        defaultMessage: 'Nombre de la peluquería'
    },
    nombrePeluqueriaObligatorio: {
        id: 'nombre.peluqueria.obligatorio',
        defaultMessage: 'Introduzca el nombre de la peluquería'
    },
    noResultsText: {
        id: 'no.results.text',
        defaultMessage: 'No se encontraron resultados'
    },
    nueva: {
        id: 'nueva',
        defaultMessage: 'Nueva'
    },
    nuevaFactura: {
        id: 'nueva.factura',
        defaultMessage: 'Nueva factura'
    },
    nuevoCliente: {
        id: 'nuevo.cliente',
        defaultMessage: 'Nuevo cliente'
    },
    nuevoProducto: {
        id: 'nuevo.producto',
        defaultMessage: 'Nuevo producto'
    },
    nuevoServicio: {
        id: 'nuevo.servicio',
        defaultMessage: 'Nuevo servicio'
    },
    observaciones: {
        id: 'observaciones',
        defaultMessage: 'Observaciones'
    },
    olvidastePassword: {
        id: 'olvidaste.password',
        defaultMessage: '¿Olvidaste la contraseña?'
    },
    password: {
        id: 'password',
        defaultMessage: 'Contraseña'
    },
    passwordActual: {
        id: 'password.actual',
        defaultMessage: 'Contraseña actual'
    },
    passwordActualObligatorio: {
        id: 'password.actual.obligatorio',
        defaultMessage: 'Introduzca la contraseña actual'
    },
    passwordEmailObligatoria: {
        id: 'password.email.obligatoria',
        defaultMessage: 'Para cambiar el correo electrónico debe introducir su contraseña'
    },
    passwordIncorrecta: {
        id: 'password.incorrecta',
        defaultMessage: 'Contraseña incorrecta'
    },
    passwordLongitudMinima: {
        id: 'password.longitud.minima',
        defaultMessage: 'La contraseña debe tener al menos {limit} caracteres'
    },
    passwordNueva: {
        id: 'password.nueva',
        defaultMessage: 'Nueva contraseña'
    },
    passwordNuevaObligatorio: {
        id: 'password.nueva.obligatorio',
        defaultMessage: 'Introduzca la nueva contraseña'
    },
    passwordRepita: {
        id: 'password.repita',
        defaultMessage: 'Nueva contraseña (otra vez)'
    },
    passwordRepitaDiferente: {
        id: 'password.repita.diferente',
        defaultMessage: 'Las contraseñas no coinciden'
    },
    passwordRepitaObligatoria: {
        id: 'password.repita.obligatoria',
        defaultMessage: 'Introduzca la nueva contraseña otra vez'
    },
    passwordRestablecer: {
        id: 'password.restablecer',
        defaultMessage: 'Restablecer password'
    },
    passwordRestablecerMensaje: {
        id: 'password.restablecer.mensaje',
        defaultMessage: 'Introduzca su dirección de correo y le enviaremos un correo para restablecer la contraseña'
    },
    passwordRestablecerNuevaPassword: {
        id: 'password.restablecer.nueva.password',
        defaultMessage: 'Introduzca la nueva contraseña'
    },
    passwordRestablecida: {
        id: 'password.restablecida',
        defaultMessage: 'La contraseña se ha restablecido correctamente'
    },
    perfil: {
        id: 'perfil',
        defaultMessage: 'Perfil'
    },
    perfilActualizado: {
        id: 'perfil.actualizado',
        defaultMessage: 'Perfil actualizado correctamente'
    },
    precio: {
        id: 'precio',
        defaultMessage: 'Precio'
    },
    precioObligatorio: {
        id: 'precio.obligatorio',
        defaultMessage: 'Introduzca un precio'
    },
    precioPositivo: {
        id: 'precio.positivo',
        defaultMessage: 'El precio debe ser mayor que cero'
    },
    precioTotal: {
        id: 'precio.total',
        defaultMessage: 'Precio total'
    },
    precioUnidad: {
        id: 'precio.unidad',
        defaultMessage: 'Precio unidad'
    },
    producto: {
        id: 'producto',
        defaultMessage: '{num, plural, one {producto} other {productos}}'
    },
    productoActualizado: {
        id: 'producto.actualizado',
        defaultMessage: 'Producto actualizado correctamente',
    },
    productoBorrado: {
        id: 'producto.borrado',
        defaultMessage: 'Productos borrados correctamente'
    },
    productoBorrarValor: {
        id: 'producto.borrar.valor',
        defaultMessage: 'Borrar producto'
    },
    productoCreado: {
        id: 'producto.creado',
        defaultMessage: 'Producto creado correctamente'
    },
    productoNombre: {
        id: 'producto.nombre',
        defaultMessage: 'Producto'
    },
    productos: {
        id: 'productos',
        defaultMessage: 'Productos'
    },
    registrandomeAcepto: {
        id: 'registrandome.acepto',
        defaultMessage: "Registrándome, acepto los"
    },
    registrarse: {
        id: 'registrarse',
        defaultMessage: 'Registrarse'
    },
    reset: {
        id: 'reset',
        defaultMessage: 'Resetear'
    },    
    semana: {
        id: 'semana',
        defaultMessage: 'Semana'
    },
    searchPromptText: {
        id: 'search.prompt.text',
        defaultMessage: 'Escriba para buscar'
    },
    seleccioneCliente: {
        id: 'seleccione.cliente',
        defaultMessage: 'Seleccione cliente'
    },
    seleccioneProducto: {
        id: 'seleccione.producto',
        defaultMessage: 'Seleccione producto'
    },
    seleccioneServicio: {
        id: 'seleccione.servicio',
        defaultMessage: 'Seleccione servicio'
    },
    semanaXs: {
        id: 'semana.xs',
        defaultMessage: 'S'
    },
    servicio: {
        id: 'servicio',
        defaultMessage: 'Servicio'
    },
    servicios: {
        id: 'servicios',
        defaultMessage: 'Servicios'
    },
    servicioActualizado: {
        id: 'servicio.actualizado',
        defaultMessage: 'Servicio actualizado correctamente'
    },
    servicioBorrado: {
        id: 'servicio.borrado',
        defaultMessage: 'Servicios borrados correctamente'
    },
    servicioBorrarValor: {
        id: 'servicio.borrar.valor',
        defaultMessage: 'Borrar servicio'
    },
    servicioCreado: {
        id: 'servicio.creado',
        defaultMessage: 'Servicio creado correctamente'
    },
    servicioProductoObligatorio: {
        id: 'servicio.producto.obligatorio',
        defaultMessage: 'Debe al menos introducir un servicio o un producto'
    },
    si: {
        id: 'si',
        defaultMessage: 'Sí'
    },
    siguiente: {
        id: 'siguiente',
        defaultMessage: 'Siguiente'
    },
    sinFacturas: {
        id: 'sin.facturas',
        defaultMessage: 'Sin facturas para la fecha indicada'
    },
    sinResultados: {
        id: 'sin.resultados',
        defaultMessage: 'Sin resultados'
    },
    stock: {
        id: 'stock',
        defaultMessage: 'Stock'
    },
    stockEnteroPositivo: {
        id: 'stock.entero.positivo',
        defaultMessage: 'El stock debe ser un número entero mayor que cero'
    },
    terminosServicio: {
        id: 'terminos.servicio',
        defaultMessage: 'Términos del Servicio'
    },
    terminosServicioGarantiaTexto: {
        id: 'terminos.servicio.garantia.texto',
        defaultMessage: 'Hairstyle App garantiza que realizará esfuerzos razonables para mantener un rendimiento aceptable del sitio Web. Hairstyle App no da ninguna garantía de que el servicio esté libre de errores, sin embargo, cuando tales errores o interrupciones ocurran, hará los esfuerzos necesarios para restablecer el Servicio.'
    },
    terminosServicioGarantiaTitulo: {
        id: 'terminos.servicio.garantia.titulo',
        defaultMessage: 'Garantía y descargo de responsabilidad'
    },
    terminosServicioGeneralTexto: {
        id: 'terminos.servicio.general.texto',
        defaultMessage: 'El acceso al Servicio está únicamente disponible al Cliente y a los Usuario a los cuales el cliente ha facilitado acceso. Los nombres de usuario y contraseñas son personales, y como tal son consideradas parte de la Información Confidencial del Cliente. El Cliente es siempre completamente responsable por los actos y omisiones de los Usuario a los cuales el Cliente ha garantizado acceso y acepta indemnizar a Hairstyle App por reclamaciones y pérdidas relacionadas con tales actos u omisiones. El Cliente no usará el Servicio para ninguna actividad ilegal o no autorizada. El Cliente no debe, en el uso del Servicio, violar ninguna ley dentro de la jurisdicción del Cliente (incluidas, pero no limitado a leyes de copyright). Hairstyle App es el propietario de todos los Derechos de la Propiedad Intelectual en el Servicio.'
    },
    terminosServicioGeneralTitulo: {
        id: 'terminos.servicio.general.titulo',
        defaultMessage: 'Términos de Carácter General'
    },
    terminosServicioIntro: {
        id: 'terminos.servicio.intro',
        defaultMessage: 'Esto es un acuerdo entre Hairstyle App y la entidad que acepta los términos aquí ("Participante", "Usted", "usted", "Cliente"). Creando una cuenta, usted reconoce haber leído, entendido y aceptado todos los términos y condiciones. Si usted no acepta todos estos términos y condiciones, no debe usar o acceder el Servicio. Si usted está aceptando este acuerdo en nombre de una empresa, debe tener la autoridad para ligar dicha empresa a los términos de este acuerdo.'
    },
    terminosServicioPagoTexto: {
        id: 'terminos.servicio.pago.texto',
        defaultMessage: 'El Servicio es gratuito, pero esto puede cambiar en el futuro.'
    },
    terminosServicioPagoTitulo: {
        id: 'terminos.servicio.pago.titulo',
        defaultMessage: 'Pago'
    },
    terminosServicioPropiedadInfoTexto: {
        id: 'terminos.servicio.propiedad.info.texto',
        defaultMessage: 'Hairstyle App no es el dueño de los datos del cliente. El Cliente será el único responsable de veracidad, calidad, integridad, legalidad, fidelidad y propiedad intelectual o derecho a uso de toda la información de usuario. Para únicamente suministrarle servicios, usted da derecho a Hairstyle App para acceder, copiar, distribuir, almacenar, transmitir, reformatear, mostrar y ejecutar el contenido de su cuenta. La información personal que usted facilita a través del Servicio se rige por nuestra Política de Privacidad. La elección de usar el Servicio indica su aceptación de los términos de nuestra Política de Privacidad.'
    },
    terminosServicioPropiedadInfoTitulo: {
        id: 'terminos.servicio.propiedad.info.titulo',
        defaultMessage: 'Propiedad de la información y Privacidad'
    },
    terminosServicioResponsabilidadTexto: {
        id: 'terminos.servicio.responsabilidad.texto',
        defaultMessage: 'BAJO NINGUNA CIRCUNSTANCIA HARISTYLE APP SERA RESPONSABLE POR NINGUNA ACCIDENTAL, ESPECIAL, PUNITIVA, O OTRA PERDIDA O DAÑO DE NINGÚN TIPO O POR LA PERDIDA DE BENEFICIOS O INTERRUPCIONESS DE NEGOCIO, FALLO INFORMATICO, PERDIDA DE INFORMACION O CUALQUIER OTRA PERDIDA QUE SURJA POR EL USO O INCAPACIDAD PARA USAR EL SERVICIO.'
    },
    terminosServicioResponsabilidadTitulo: {
        id: 'terminos.servicio.responsabilidad.titulo',
        defaultMessage: 'Limitación de responsabilidad'
    },
    terminosServicioRestriccionesTexto: {
        id: 'terminos.servicio.restricciones.texto',
        defaultMessage: 'Además de todos los términos y condiciones de este Acuerdo, en el uso del servicio, Usted cumplirá con todas las leyes aplicables y regulaciones y directrices, procedimientos y políticas notificadas por Hairstyle App; Usted no transferirá o hará disponible a ninguna entidad externa el Servicio; Usted no proveerá ningún servicio basado en el Servicio sin consentimiento explícito; Usted no usará el Servicio para fines no legales; Usted no adoptará, reproducirá, copiará, almacenará, distribuirá, imprimirá, mostrará, ejecutará, publicará o creará trabajo derivado de ninguna de las partes del Servicio. '
    },
    terminosServicioRestriccionesTitulo: {
        id: 'terminos.servicio.restricciones.titulo',
        defaultMessage: 'Restricciones de Uso'
    },
    terminosServicioServicioTexto: {
        id: 'terminos.servicio.servicio.texto',
        defaultMessage: 'Hairstyle App provee un software sujeto a los términos y condiciones de este Acuerdo. El Cliente se conectará al Servicio usando un navegador de Internet o una aplicación móvil soportada. Hairstyle App se reserva el derecho a actualizar y cambiar los Términos del Servicio cuando así lo estime oportuno. Si continúa usando dicho Servicio después de cualquier cambio, estará aceptando tales cambios.'
    },
    terminosServicioServicioTitulo: {
        id: 'terminos.servicio.servicio.titulo',
        defaultMessage: 'El servicio'
    },
    tienesCuenta: {
        id: 'tienes.cuenta',
        defaultMessage: "¿Ya tienes cuenta?"
    },
    todoElDia: {
        id: 'todo.el.dia',
        defaultMessage: 'Todo el día'
    },
    total: {
        id: 'total',
        defaultMessage: 'Total'
    },
    trabajo: {
        id: 'trabajo',
        defaultMessage: 'Trabajo'
    },
    unprocessableEntity: {
        id: 'unprocessable.entity',
        defaultMessage: 'Entrada de datos incorrecta'
    },
    volver: {
        id: 'volver',
        defaultMessage: 'Volver'
    }
});

export default messages;