import { IntlProvider, addLocaleData } from 'react-intl';
import moment from 'moment';
import 'moment/locale/es';
import 'moment/locale/it';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import it from 'react-intl/locale-data/it';
import localeData from './data.json';

addLocaleData([...en, ...es, ...it]);

// Define user's language. Different browsers have the user locale defined on different fields on the `navigator` 
// object, so we make sure to account for these different by checking all of them
export const getLanguage = () =>
    (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;

// Split locales with a region code
export const getLanguageWithoutRegionCode = (language) =>
    language.toLowerCase().split(/[_-]+/)[0];

// Try full locale, try locale without region code, fallback to 'es'
export const getMessages = (language, languageWithoutRegionCode) => {
    return localeData[languageWithoutRegionCode] || localeData[language] || localeData.es;
}

const language = getLanguage();
const languageWithoutRegionCode = getLanguageWithoutRegionCode(language);
const messages = getMessages(language, languageWithoutRegionCode);
const {intl} = new IntlProvider({
    locale: language,
    messages: messages,
}, {}).getChildContext();

export const formatMessage = (msg, values) =>
    intl.formatMessage(msg, values)

export const formatDate = (value, options) =>
    intl.formatDate(value, options)

export const formatTime = (value, options) =>
    intl.formatTime(value, options)

export const locale = languageWithoutRegionCode;

export const getDateFormat = () => 
    moment().locale(locale).localeData().longDateFormat('L');