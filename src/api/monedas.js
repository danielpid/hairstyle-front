import * as api from './common';

const url = api.urlBase + '/api/monedas';

export const fetchMonedas = () =>
    api.fetchAll(url);