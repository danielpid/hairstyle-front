import fetch from 'isomorphic-fetch';
import { urlBase } from './common';

const url = urlBase + '/api/usuario';

export const checkEmailAvailability = (email) =>
    fetch(url + '/email', {
        method: 'POST',
        headers: { 'Authorization': localStorage.getItem('hs_tkn_session') },
        body: email
    }).then((response => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response.json();
    }));

export const resetUserPassword = (token, password) => 
    fetch(url + '/resetPassword', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            token,
            password
        })
    }).then((response => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response.text();
    }));