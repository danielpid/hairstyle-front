import * as api from './common';

const url = api.urlBase + '/api/clientes';

export const fetchClientes = () =>
    api.fetchAll(url);

export const fetchClientesByCriteria = (params) =>
    api.fetchByCriteria(url, params);

export const addCliente = (nombre, observaciones, descripcionMedioComunicacionMovil, descripcionMedioComunicacionCasa, 
    descripcionMedioComunicacionTrabajo, descripcionMedioComunicacionEmail) =>
    api.add(url, { nombre, observaciones, descripcionMedioComunicacionMovil, descripcionMedioComunicacionCasa, 
        descripcionMedioComunicacionTrabajo, descripcionMedioComunicacionEmail });

export const editCliente = (idCliente, nombre, observaciones, idMedioComunicacionMovil, 
    descripcionMedioComunicacionMovil, idMedioComunicacionCasa, descripcionMedioComunicacionCasa, 
    idMedioComunicacionTrabajo, descripcionMedioComunicacionTrabajo, idMedioComunicacionEmail, 
    descripcionMedioComunicacionEmail) =>
    api.edit(url, { idCliente, nombre, observaciones, idMedioComunicacionMovil, descripcionMedioComunicacionMovil, 
        idMedioComunicacionCasa, descripcionMedioComunicacionCasa, idMedioComunicacionTrabajo,
        descripcionMedioComunicacionTrabajo, idMedioComunicacionEmail, descripcionMedioComunicacionEmail });

export const removeClientes = (idsCliente) =>
    api.remove(url, idsCliente);