import fetch from 'isomorphic-fetch';

export const urlBase = process.env.REACT_APP_URL_BASE;

export const fetchAll = (url) =>
    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('hs_tkn_session')
        }
    }).then((response) => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response.json();
    });

export const fetchByCriteria = (url, criteria) =>
    fetch(url + '/query', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('hs_tkn_session')
        },
        body: JSON.stringify({ ...criteria })
    }).then((response) => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response.json();
    });

export const add = (url, fields) =>
    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('hs_tkn_session')
        },
        body: JSON.stringify({ ...fields })
    }).then((response) => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response.json();
    });

export const edit = (url, fields) =>
    fetch(url, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('hs_tkn_session')
        },
        body: JSON.stringify({ ...fields })
    }).then((response) => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response.json();
    });

export const remove = (url, ids) =>
    fetch(url + '/delete', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('hs_tkn_session')
        },
        body: JSON.stringify(ids)
    }).then((response) => {
        if (response.status >= 400) {
            throw new Error(response.status);
        } else if (parseInt(response.status, 10) !== 204) {
            return response.json();
        }
    });

export const removeById = (url, id) =>
    fetch(url + '/' + id, {
        method: 'DELETE',
        headers: {
            'Authorization': localStorage.getItem('hs_tkn_session')
        }
    }).then((response) => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
    });