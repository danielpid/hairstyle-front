import * as api from './common';

const url = api.urlBase + '/api/citas';

export const fetchCitasByCriteria = (fechaDesde, fechaHasta) =>
    api.fetchByCriteria(url, { fechaDesde, fechaHasta });

export const addCita = (idCliente, nombreCliente, fechaDesde, fechaHasta, observaciones) =>
    api.add(url, { idCliente, nombreCliente, fechaDesde, fechaHasta, observaciones });

export const editCita = (idCita, idCliente, nombreCliente, fechaDesde, fechaHasta, observaciones) =>
    api.edit(url, { idCita, idCliente, nombreCliente, fechaDesde, fechaHasta, observaciones });

export const removeCita = (idCita) =>
    api.removeById(url, idCita);