import * as api from './common';

const url = api.urlBase + '/api/facturas';

export const fetchFacturasByCriteria = (fechaDesde, fechaHasta) =>
    api.fetchByCriteria(url, {fechaDesde, fechaHasta});

export const addFactura = (idCliente, nombreCliente, fechaAlta, precio, detalle) =>
    api.add(url, {idCliente, nombreCliente, fechaAlta, precio, detalle});

export const editFactura = (idFactura, idCliente, nombreCliente, fechaAlta, precio, detalle) =>
    api.edit(url, {idFactura, idCliente, nombreCliente, fechaAlta, precio, detalle});

export const removeFactura = (idFactura) =>
    api.removeById(url, idFactura);