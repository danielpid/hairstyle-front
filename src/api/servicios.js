import * as api from './common';

const url = api.urlBase + '/api/servicios';

export const fetchServices = () => 
    api.fetchAll(url);

export const fetchServicesByCriteria = (params) => 
    api.fetchByCriteria(url, params);

export const addService = (nombre, precio) => 
    api.add(url, {nombre, precio});

export const editService = (idServicio, nombre, precio) => 
    api.edit(url, {idServicio, nombre, precio});

export const removeServices = (idsServicio) => 
    api.remove(url, idsServicio);