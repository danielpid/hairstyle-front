import fetch from 'isomorphic-fetch';
import * as api from './common';

const url = api.urlBase + '/api/peluqueria';

export const fetchPeluqueria = () => (
    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('hs_tkn_session')
        }
    }).then((response) => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response.json();
    })
);

export const addPeluqueria = (nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva, idMoneda) =>
    api.add(url, { nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva, idMoneda });

export const editPeluqueria = (nombre, email, passwordEmail, horaApertura, horaCierre, passwordActual, passwordNueva,
    idMoneda, mostrarTotalDiario) =>
    fetch(url, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('hs_tkn_session')
        },
        body: JSON.stringify({
            nombre, email, passwordEmail, horaApertura, horaCierre, passwordActual, passwordNueva,
            idMoneda, mostrarTotalDiario
        })
    }).then((response) => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response;
    });    