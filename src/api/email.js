import fetch from 'isomorphic-fetch';
import { urlBase } from './common';

const url = urlBase + '/api/email';

export const checkEmailAvailability = (email) =>
    fetch(url, {
        method: 'POST',
        body: email
    }).then((response => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response.json();
    }));

export const forgotPassword = (email) =>
    fetch(url + '/forgot', {
        method: 'POST',
        body: email
    }).then(response => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
    });
