import { schema } from 'normalizr';

export const service = new schema.Entity('services', {}, { idAttribute: 'idServicio' });
export const arrayOfServices = new schema.Array(service);

export const producto = new schema.Entity('productos', {}, { idAttribute: 'idProducto' });
export const arrayOfProductos = new schema.Array(producto);

export const cliente = new schema.Entity('clientes', {} , { idAttribute: 'idCliente' });
export const arrayOfClientes = new schema.Array(cliente);

export const cita = new schema.Entity('citas', {}, { idAttribute: 'idCita' });
export const arrayOfCitas = new schema.Array(cita);

export const factura = new schema.Entity('facturas', {}, { idAttribute: 'idFactura' });
export const arrayOfFacturas = new schema.Array(factura);

export const moneda = new schema.Entity('monedas', {}, { idAttribute: 'idMoneda' });
export const arrayOfMonedas = new schema.Array(moneda);

export const peluqueria = new schema.Entity('peluqueria', {}, { idAttribute: 'idPeluqueria' });