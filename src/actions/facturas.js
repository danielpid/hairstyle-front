import { normalize } from 'normalizr'
import * as schema from '../api/schema'
import * as api from '../api/facturas'
import * as apiProducto from '../api/productos'
import * as connector from '../reducers/facturas/facturas'
import alert from './alert'
import messages from './../i18n/messages'
import { formatMessage } from './../i18n'
import { handleError } from './common'

export const fetchFacturasByCriteria = (fechaDesde, fechaHasta) => (dispatch, getState) => {

    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_FACTURAS_REQUEST'
    });
    return api.fetchFacturasByCriteria(fechaDesde, fechaHasta).then(
        response => {
            dispatch({
                type: 'FETCH_FACTURAS_SUCCESS',
                response: normalize(response, schema.arrayOfFacturas)
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_FACTURAS_FAILURE');
        }
    );
};

export const addFactura = (idCliente, nombreCliente, fechaAlta, precio, detalle, callback) => (dispatch, getState) => {

    if (connector.getIsAdding(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'ADD_FACTURA_REQUEST'
    });
    api.addFactura(idCliente, nombreCliente, fechaAlta, precio, detalle).then(
        response => {
            alert('success', formatMessage(messages.facturaCreada));
            dispatch({
                type: 'ADD_FACTURA_SUCCESS',
                response: normalize(response, schema.factura)
            });
            callback();
        },
        error => {
            handleError(dispatch, error, 'ADD_FACTURA_FAILURE');
        }
    );
};

export const editFactura = (idFactura, idCliente, nombreCliente, fechaAlta, precio, detalle, callback) => (dispatch, getState) => {
    
        if (connector.getIsEditing(getState())) {
            return Promise.resolve();
        }
        dispatch({
            type: 'EDIT_FACTURA_REQUEST'
        });
        api.editFactura(idFactura, idCliente, nombreCliente, fechaAlta, precio, detalle).then(
            response => {
                alert('success', formatMessage(messages.facturaActualizada));
                dispatch({
                    type: 'EDIT_FACTURA_SUCCESS',
                    response: normalize(response, schema.factura)
                });
                callback();
            },
            error => {
                handleError(dispatch, error, 'EDIT_FACTURA_FAILURE');
            }
        );
    };

export const removeFactura = (idFactura) => (dispatch, getState) => {
    
    if (connector.getIsRemoving(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'REMOVE_FACTURA_REQUEST'
    });
    api.removeFactura(idFactura).then(
        response => {
            alert('success', formatMessage(messages.facturaBorrada));
            dispatch({
                type: "REMOVE_FACTURA_SUCCESS",
                idFactura
            });
        },
        error => {
            handleError(dispatch, error, 'REMOVE_FACTURA_FAILURE');
        }
    );
};

export const openModalFactura = (factura) => (dispatch) => {
    // antes de mostrar la modal recupero el stock actual para cada uno de los productos
    const idsProducto = factura.detalle && factura.detalle
        .map(lf => lf.idProducto)
        .filter(idProducto => idProducto !== null);
    if (idsProducto && idsProducto.length) {
        apiProducto.fetchProductosByCriteria({idsProducto}).then(
            response => {
                for (const lineaFactura of factura.detalle) {
                    if (lineaFactura.idProducto 
                            && idsProducto.includes(lineaFactura.idProducto)) {
                        const producto = response.find(p => p.idProducto === lineaFactura.idProducto);
                        lineaFactura.stockActual = producto.stock;
                    }
                }
                dispatch({type: "OPEN_MODAL_FACTURA",
                factura
            });
            }
        );
    } else {
        dispatch({type: "OPEN_MODAL_FACTURA",
        factura
    });
    }
};

export const closeModalFactura = () => ({
    type: "CLOSE_MODAL_FACTURA"
});