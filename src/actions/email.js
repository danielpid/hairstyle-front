import * as api from '../api/email';
import { getIsSendingEmailResetPassword } from '../reducers/email';
import { handleError } from './common';
import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';

export const sendEmailResetPassword = (email) => (dispatch, getState) => {
    if (getIsSendingEmailResetPassword(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'SEND_EMAIL_RESET_PASSWORD_REQUEST'
    });
    return api.forgotPassword(email).then(
        response => {
            dispatch({
                type: 'SEND_EMAIL_RESET_PASSWORD_SUCCESS'
            });
            alert('success', formatMessage(messages.emailResetPasswordSent));
        },
        error => {
            if (parseInt(error.message, 10) === 401) {
                alert('error', formatMessage(messages.invalidEmail));
            } else {
                handleError(dispatch, error, 'SEND_EMAIL_RESET_PASSWORD_FAILURE');
            }
        }
    );
}