import { normalize } from 'normalizr';
import * as schema from '../api/schema';
import * as api from '../api/monedas';
import * as connector from '../reducers/monedas';
import { handleError } from './common';

export const fetchMonedas = () => (dispatch, getState) => {
    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_MONEDAS_REQUEST'
    });
    return api.fetchMonedas().then(
        response => {
            dispatch({
                type: 'FETCH_MONEDAS_SUCCESS',
                response: normalize(response, schema.arrayOfMonedas)
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_MONEDAS_FAILURE');
        }
    );
};