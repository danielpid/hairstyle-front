import * as api from '../api/peluqueria';
import * as connector from '../reducers/peluqueria/peluqueria';
import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import { handleError } from './common';
import moment from 'moment';
import { login } from './login';

export const fetchPeluqueria = () => (dispatch, getState) => {
    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_PELUQUERIA_REQUEST'
    });
    return api.fetchPeluqueria().then(
        response => {
            response.horaApertura = moment(response.horaApertura, 'HH:mm');
            response.horaCierre = moment(response.horaCierre, 'HH:mm');
            dispatch({
                type: 'FETCH_PELUQUERIA_SUCCESS',
                response
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_PELUQUERIA_FAILURE');
        }
    );
};

export const addPeluqueria = (nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva, idMoneda, history) =>
    (dispatch, getState) => {
        if (connector.getIsAdding(getState())) {
            return Promise.resolve();
        }
        dispatch({
            type: 'ADD_PELUQUERIA_REQUEST'
        });
        api.addPeluqueria(nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva, idMoneda).then(
            response => {
                dispatch({
                    type: 'ADD_PELUQUERIA_SUCCESS'
                });
                login(email, passwordNueva, history)(dispatch, getState);
            },
            error => {
                handleError(dispatch, error, 'ADD_PELUQUERIA_FAILURE');
            }
        );
    };

export const editPeluqueria = (nombre, email, passwordEmail, horaApertura, horaCierre, passwordActual, passwordNueva,
    idMoneda, mostrarTotalDiario, message, callback) => (dispatch, getState) => {
        if (connector.getIsEditing(getState())) {
            return Promise.resolve();
        }
        dispatch({
            type: 'EDIT_PELUQUERIA_REQUEST'
        });
        api.editPeluqueria(nombre, email, passwordEmail, horaApertura, horaCierre, passwordActual, passwordNueva,
            idMoneda, mostrarTotalDiario).then(
                response => {
                    response.json().then(
                        json => {
                            json.horaApertura = moment(json.horaApertura, 'HH:mm');
                            json.horaCierre = moment(json.horaCierre, 'HH:mm');
                            json.passwordEmail = null;
                            message = message || messages.informacionActualizada;
                            // el token contiene el email, al actualizarlo debemos actualizar el token
                            if (json.emailActualizado === true) {
                                const authorization = response.headers.get('Authorization');
                                localStorage.setItem('hs_tkn_session', authorization.split(' ')[1]);
                                json.emailActualizado = false;
                            }
                            alert('success', formatMessage(message));
                            dispatch({
                                type: 'EDIT_PELUQUERIA_SUCCESS',
                                response: json
                            });
                            if (callback) {
                                callback();
                            }
                        }
                    )
                }
            ).catch(error => {
                if (parseInt(error.message, 10) === 401) {
                    alert('error', formatMessage(messages.passwordIncorrecta));
                    dispatch({
                        type: 'EDIT_PELUQUERIA_FAILURE'
                    });
                } else {
                    handleError(dispatch, error, 'EDIT_PELUQUERIA_FAILURE');
                }
            });;
    };

export const resetUserPassword = () => {

}

export const openModalProfile = (peluqueria) => ({
    type: "OPEN_MODAL_PROFILE",
    peluqueria
});

export const closeModalProfile = (peluqueria) => ({
    type: "CLOSE_MODAL_PROFILE"
});