import { normalize } from 'normalizr';
import * as schema from '../api/schema';
import * as api from '../api/productos';
import * as connector from '../reducers/productos/productos';
import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import { handleError } from './common';

export const fetchProductos = () => (dispatch, getState) => {
    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_PRODUCTOS_REQUEST'
    });
    return api.fetchProductos().then(
        response => {
            dispatch({
                type: 'FETCH_PRODUCTOS_SUCCESS',
                response: normalize(response, schema.arrayOfProductos)
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_PRODUCTOS_FAILURE');
        }
    );
};

export const fetchProductosByCriteria = (params) => (dispatch, getState) => {
    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_PRODUCTOS_REQUEST'
    });
    return api.fetchProductosByCriteria(params).then(
        response => {
            dispatch({
                type: 'FETCH_PRODUCTOS_SUCCESS',
                response: normalize(response, schema.arrayOfProductos)
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_PRODUCTOS_FAILURE');
        }
    );
};

export const sortChangeProductos = (columnKey, sortDir, productos) => (dispatch) => {
    dispatch({
        type: 'SORT_CHANGE_PRODUCTOS',
        columnKey,
        sortDir,
        productos
    });
};

export const addProducto = (nombre, precio, stock, reset) => (dispatch, getState) => {
    if (connector.getIsAdding(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'ADD_PRODUCTO_REQUEST'
    });
    api.addProducto(nombre, precio, stock).then(
        response => {
            alert('success', formatMessage(messages.productoCreado));
            dispatch({
                type: 'ADD_PRODUCTO_SUCCESS',
                response: normalize(response, schema.producto)
            });
            dispatch({
                type: 'SORT_CHANGE_PRODUCTOS',
                columnKey: connector.getColSortDirColumnKey(getState()),
                sortDir: connector.getColSortDirSortDir(getState()),
                productos: connector.getProductos(getState())
            });
            reset();
        },
        error => {
            handleError(dispatch, error, 'ADD_PRODUCTO_FAILURE');
        }
    );
};

export const editProducto = (idProducto, nombre, precio, stock) => (dispatch, getState) => {
    if (connector.getIsEditing(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'EDIT_PRODUCTO_REQUEST'
    });
    api.editProducto(idProducto, nombre, precio, stock).then(
        response => {
            alert('success', formatMessage(messages.productoActualizado));
            dispatch({
                type: 'EDIT_PRODUCTO_SUCCESS',
                response: normalize(response, schema.producto)
            });
            dispatch({
                type: 'SORT_CHANGE_PRODUCTOS',
                columnKey: connector.getColSortDirColumnKey(getState()),
                sortDir: connector.getColSortDirSortDir(getState()),
                productos: connector.getProductos(getState())
            });
        },
        error => {
            handleError(dispatch, error, 'EDIT_PRODUCTO_FAILURE');
        }
    );
};

export const removeProductos = (idsProducto) => (dispatch, getState) => {
    if (connector.getIsRemoving(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'REMOVE_PRODUCTOS_REQUEST'
    });
    api.removeProductos(idsProducto).then(
        response => {
            alert('success', formatMessage(messages.productoBorrado));
            dispatch({
                type: "REMOVE_PRODUCTOS_SUCCESS",
                idsProducto
            });
            dispatch({
                type: 'SORT_CHANGE_PRODUCTOS',
                columnKey: connector.getColSortDirColumnKey(getState()),
                sortDir: connector.getColSortDirSortDir(getState()),
                productos: connector.getProductos(getState())
            });
        },
        error => {
            handleError(dispatch, error, 'REMOVE_PRODUCTOS_FAILURE');
        }
    );
};

export const addAllProductosDelete = (productos) => ({
    type: "ADD_ALL_PRODUCTOS_DELETE",
    productos
});

export const removeAllProductosDelete = () => ({
    type: "REMOVE_ALL_PRODUCTOS_DELETE"
});

export const addIdProductoDelete = (idProducto) => ({
    type: "ADD_ID_PRODUCTO_DELETE",
    idProducto
});

export const removeIdProductoDelete = (idProducto) => ({
    type: "REMOVE_ID_PRODUCTO_DELETE",
    idProducto
});

export const openModalAddProducto = () => ({
    type: "OPEN_MODAL_ADD_PRODUCTO"
});

export const closeModalAddProducto = () => ({
    type: "CLOSE_MODAL_ADD_PRODUCTO"
});

export const openModalEditProducto = (producto) => ({
    type: "OPEN_MODAL_EDIT_PRODUCTO",
    producto
});

export const closeModalEditProducto = () => ({
    type: "CLOSE_MODAL_EDIT_PRODUCTO"
});