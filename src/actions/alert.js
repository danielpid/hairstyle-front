import Alert from 'react-s-alert';

const alert = (level, msg) => {
    switch (level) {
        case "success":
            return Alert.success(msg);
        case "info":
            return Alert.info(msg);
        case "warning":
            return Alert.warning(msg);
        case "error":
            return Alert.error(msg);
        default: // do nothing
    }
};

export default alert;