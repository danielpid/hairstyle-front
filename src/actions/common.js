import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import { logout } from './login';

export const handleError = (dispatch, error, action) => {
    if (parseInt(error.message, 10) === 403) {
        logout()(dispatch);
    } else if (parseInt(error.message, 10) === 422) {
        alert('error', formatMessage(messages.unprocessableEntity));
    } else {
        alert('error', formatMessage(messages.errorGenerico));
    }
    dispatch({
        type: action
    });
};