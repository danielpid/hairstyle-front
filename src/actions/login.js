import { getIsLoging } from '../reducers/login';
import * as api from '../api/login';

export const login = (email, password, history) => (dispatch, getState) => {
    if (getIsLoging(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'LOGIN_REQUEST',
        email,
        password
    });
    return api.login(email, password).then(
        response => {
            localStorage.setItem('hs_tkn_session', response.split(' ')[1]);
            dispatch({
                type: 'LOGIN_SUCCESS'
            });
            history.push("/home")
        },
        error => {
            if (parseInt(error.message, 10) === 401) {
                dispatch({
                    type: 'LOGIN_INVALID_CREDENTIALS',
                })
            } else {
                dispatch({
                    type: 'LOGIN_FAILURE'
                })
            }
        }
    );
};

export const hideInvalidCredentials = () => (dispatch) => (
    dispatch({
        type: 'LOGIN_HIDE_INVALID_CREDENTIALS'
    })
);

export const logout = () => (dispatch) => {
    localStorage.removeItem('hs_tkn_session');
    return dispatch({
        type: 'FORBIDDEN'
    });
}

export const loggedIn = () => (dispatch) =>
    dispatch({
        type: 'LOGIN_SUCCESS'
    })