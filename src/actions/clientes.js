import { normalize } from 'normalizr';
import * as schema from '../api/schema';
import * as api from '../api/clientes';
import * as connector from '../reducers/clientes/clientes';
import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import { handleError } from './common';

export const fetchClientes = () => (dispatch, getState) => {
    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_CLIENTES_REQUEST'
    });
    return api.fetchClientes().then(
        response => {
            dispatch({
                type: 'FETCH_CLIENTES_SUCCESS',
                response: normalize(response, schema.arrayOfClientes)
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_CLIENTES_FAILURE');
        }
    );
};

export const fetchClientesByCriteria = (params) => (dispatch, getState) => {

    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_CLIENTES_REQUEST'
    });
    return api.fetchClientesByCriteria(params).then(
        response => {
            dispatch({
                type: 'FETCH_CLIENTES_SUCCESS',
                response: normalize(response, schema.arrayOfClientes)
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_CLIENTES_FAILURE');
        }
    );
};

export const sortChangeClientes = (columnKey, sortDir, clientes) => (dispatch) => {
    dispatch({
        type: 'SORT_CHANGE_CLIENTES',
        columnKey,
        sortDir,
        clientes
    });
};

export const addCliente = (nombre, observaciones, descripcionMedioComunicacionMovil, descripcionMedioComunicacionCasa, 
    descripcionMedioComunicacionTrabajo, descripcionMedioComunicacionEmail, reset) => (dispatch, getState) => {
    if (connector.getIsAdding(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'ADD_CLIENTE_REQUEST'
    });
    api.addCliente(nombre, observaciones, descripcionMedioComunicacionMovil, descripcionMedioComunicacionCasa, 
    descripcionMedioComunicacionTrabajo, descripcionMedioComunicacionEmail).then(
        response => {
            alert('success', formatMessage(messages.clienteCreado));
            dispatch({
                type: 'ADD_CLIENTE_SUCCESS',
                response: normalize(response, schema.cliente)
            });
            dispatch({
                type: 'SORT_CHANGE_CLIENTES',
                columnKey: connector.getColSortDirColumnKey(getState()),
                sortDir: connector.getColSortDirSortDir(getState()),
                clientes: connector.getClientes(getState())
            });
            reset();
        },
        error => {
            handleError(dispatch, error, 'ADD_CLIENTE_FAILURE');
        }
    );
};

export const editCliente = (idCliente, nombre, observaciones, idMedioComunicacionMovil, 
    descripcionMedioComunicacionMovil, idMedioComunicacionCasa, descripcionMedioComunicacionCasa, 
    idMedioComunicacionTrabajo, descripcionMedioComunicacionTrabajo, idMedioComunicacionEmail, 
    descripcionMedioComunicacionEmail) => (dispatch, getState) => {
    if (connector.getIsEditing(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'EDIT_CLIENTE_REQUEST'
    });
    api.editCliente(idCliente, nombre, observaciones, idMedioComunicacionMovil, 
        descripcionMedioComunicacionMovil, idMedioComunicacionCasa, descripcionMedioComunicacionCasa, 
        idMedioComunicacionTrabajo, descripcionMedioComunicacionTrabajo, idMedioComunicacionEmail, 
        descripcionMedioComunicacionEmail).then(
        response => {
            alert('success', formatMessage(messages.clienteActualizado));
            dispatch({
                type: 'EDIT_CLIENTE_SUCCESS',
                response: normalize(response, schema.cliente)
            });
            dispatch({
                type: 'SORT_CHANGE_CLIENTES',
                columnKey: connector.getColSortDirColumnKey(getState()),
                sortDir: connector.getColSortDirSortDir(getState()),
                clientes: connector.getClientes(getState())
            });
        },
        error => {
            handleError(dispatch, error, 'EDIT_CLIENTE_FAILURE');
        }
    );
};

export const removeClientes = (idsCliente) => (dispatch, getState) => {
    if (connector.getIsRemoving(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'REMOVE_CLIENTES_REQUEST'
    });
    api.removeClientes(idsCliente).then(
        response => {
            alert('success', formatMessage(messages.clientesBorrado));
            dispatch({
                type: "REMOVE_CLIENTES_SUCCESS",
                idsCliente
            });
            dispatch({
                type: 'SORT_CHANGE_CLIENTES',
                columnKey: connector.getColSortDirColumnKey(getState()),
                sortDir: connector.getColSortDirSortDir(getState()),
                clientes: connector.getClientes(getState())
            });
        },
        error => {
            handleError(dispatch, error, 'REMOVE_CLIENTES_FAILURE');
        }
    );
};

export const addAllClientesDelete = (clientes) => ({
    type: "ADD_ALL_CLIENTES_DELETE",
    clientes
});

export const removeAllClientesDelete = () => ({
    type: "REMOVE_ALL_CLIENTES_DELETE"
});

export const addIdClienteDelete = (idCliente) => ({
    type: "ADD_ID_CLIENTE_DELETE",
    idCliente
});

export const removeIdClienteDelete = (idCliente) => ({
    type: "REMOVE_ID_CLIENTE_DELETE",
    idCliente
});

export const openModalAddCliente = () => ({
    type: "OPEN_MODAL_ADD_CLIENTE"
});

export const closeModalAddCliente = () => ({
    type: "CLOSE_MODAL_ADD_CLIENTE"
});

export const openModalEditCliente = (cliente) => ({
    type: "OPEN_MODAL_EDIT_CLIENTE",
    cliente
});

export const closeModalEditCliente = () => ({
    type: "CLOSE_MODAL_EDIT_CLIENTE"
});