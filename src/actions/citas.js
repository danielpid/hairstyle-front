import { normalize } from 'normalizr';
import * as schema from '../api/schema';
import * as api from '../api/citas';
import * as connector from '../reducers/citas/citas';
import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import { handleError } from './common';

export const fetchCitasByCriteria = (fechaDesde, fechaHasta)  => (dispatch, getState) => {

    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_CITAS_REQUEST'
    });
    return api.fetchCitasByCriteria(fechaDesde, fechaHasta).then(
        response => {
            for (const cita of response) {
                cita.fechaDesde = new Date(cita.fechaDesde);
                cita.fechaHasta = new Date(cita.fechaHasta);
            }
            dispatch({
                type: 'FETCH_CITAS_SUCCESS',
                response: normalize(response, schema.arrayOfCitas)
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_CITAS_FAILURE');
        }
    );
};

export const addCita = (idCliente, nombreCliente, fechaDesde, fechaHasta, observaciones) => (dispatch, getState) => {

    if (connector.getIsAdding(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'ADD_CITA_REQUEST'
    });
    api.addCita(idCliente, nombreCliente, fechaDesde, fechaHasta, observaciones).then(
        response => {
            alert('success', formatMessage(messages.citaCreada));
            response.fechaDesde = new Date(response.fechaDesde);
            response.fechaHasta = new Date(response.fechaHasta);
            dispatch({
                type: 'ADD_CITA_SUCCESS',
                response: normalize(response, schema.cita)
            });
        },
        error => {
            handleError(dispatch, error, 'ADD_CITA_FAILURE');
        }
    );
};

export const editCita = (idCita, idCliente, nombreCliente, fechaDesde, fechaHasta, observaciones) => (dispatch, getState) => {

    if (connector.getIsEditing(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'EDIT_CITA_REQUEST'
    });
    api.editCita(idCita, idCliente, nombreCliente, fechaDesde, fechaHasta, observaciones).then(
        response => {
            alert('success', formatMessage(messages.citaActualizada));
            response.fechaDesde = new Date(response.fechaDesde);
            response.fechaHasta = new Date(response.fechaHasta);
            dispatch({
                type: 'EDIT_CITA_SUCCESS',
                response: normalize(response, schema.cita)
            });
        },
        error => {
            handleError(dispatch, error, 'EDIT_CITA_FAILURE');
        }
    )
};

export const removeCita = (idCita) => (dispatch, getState) => {
    
    if (connector.getIsRemoving(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'REMOVE_CITA_REQUEST'
    });
    api.removeCita(idCita).then(
        response => {
            alert('success', formatMessage(messages.citaBorrada));
            dispatch({
                type: "REMOVE_CITA_SUCCESS",
                idCita
            });
        },
        error => {
            handleError(dispatch, error, 'REMOVE_CITA_FAILURE');
        }
    );
};

export const openModalCita = (cita) => ({
    type: "OPEN_MODAL_CITA",
    cita
});

export const closeModalCita = () => ({
    type: "CLOSE_MODAL_CITA"
});

export const setCita = (cita) => ({
    type: "SET_CITA",
    cita
});