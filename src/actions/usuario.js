import * as api from '../api/usuario';
import { getIsResettingPassword } from '../reducers/usuario';
import { login } from './login'
import { handleError } from './common';
import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';

export const resetUserPassword = (token, password, history) => (dispatch, getState) => {
    if (getIsResettingPassword(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'RESET_PASSWORD_REQUEST'
    });
    return api.resetUserPassword(token, password).then(
        email => {
            dispatch({
                type: 'RESET_PASSWORD_SUCCESS'
            });
            window.location.assign(window.location.origin + '/#/');
            dispatch(login(email, password, history));
        },
        error => {
            if (parseInt(error.message, 10) === 401) {
                alert('error', formatMessage(messages.invalidToken));
            } else {
                handleError(dispatch, error, 'RESET_PASSWORD_FAILURE');
            }
        }
    );
}

export const setDeviceSize = (deviceSize) => (dispatch) => 
    dispatch({
        type: 'SET_DEVICE_SIZE',
        deviceSize
    });