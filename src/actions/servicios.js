import { normalize } from 'normalizr';
import * as schema from '../api/schema';
import * as api from '../api/servicios';
import * as connector from '../reducers/services/services';
import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import { handleError } from './common';

export const fetchServices = () => (dispatch, getState) => {
    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_SERVICES_REQUEST'
    });
    return api.fetchServices().then(
        response => {
            dispatch({
                type: 'FETCH_SERVICES_SUCCESS',
                response: normalize(response, schema.arrayOfServices)
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_SERVICES_FAILURE');
        }
    );
};

export const fetchServicesByCriteria = (params) => (dispatch, getState) => {
    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_SERVICES_REQUEST'
    });
    return api.fetchServicesByCriteria(params).then(
        response => {
            dispatch({
                type: 'FETCH_SERVICES_SUCCESS',
                response: normalize(response, schema.arrayOfServices)
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_SERVICES_FAILURE');
        }
    );
};

export const sortChangeServices = (columnKey, sortDir, services) => (dispatch) => {
    dispatch({
        type: 'SORT_CHANGE_SERVICES',
        columnKey,
        sortDir,
        services
    });
};

export const addService = (nombre, precio, reset) => (dispatch, getState) => {
    if (connector.getIsAdding(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'ADD_SERVICE_REQUEST'
    });
    api.addService(nombre, precio).then(
        response => {
            alert('success', formatMessage(messages.servicioCreado));
            dispatch({
                type: 'ADD_SERVICE_SUCCESS',
                response: normalize(response, schema.service)
            });
            dispatch({
                type: 'SORT_CHANGE_SERVICES',
                columnKey: connector.getColSortDirColumnKey(getState()),
                sortDir: connector.getColSortDirSortDir(getState()),
                services: connector.getServices(getState())
            });
            reset();
        },
        error => {
            handleError(dispatch, error, 'ADD_SERVICE_FAILURE');
        }
    );
};

export const editService = (idServicio, nombre, precio) => (dispatch, getState) => {
    if (connector.getIsEditing(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'EDIT_SERVICE_REQUEST'
    });
    api.editService(idServicio, nombre, precio).then(
        response => {
            alert('success', formatMessage(messages.servicioActualizado));
            dispatch({
                type: 'EDIT_SERVICE_SUCCESS',
                response: normalize(response, schema.service)
            });
            dispatch({
                type: 'SORT_CHANGE_SERVICES',
                columnKey: connector.getColSortDirColumnKey(getState()),
                sortDir: connector.getColSortDirSortDir(getState()),
                services: connector.getServices(getState())
            });
        },
        error => {
            handleError(dispatch, error, 'EDIT_SERVICE_FAILURE');
        }
    );
};

export const removeServices = (idsServicio) => (dispatch, getState) => {
    if (connector.getIsRemoving(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'REMOVE_SERVICES_REQUEST'
    });
    api.removeServices(idsServicio).then(
        response => {
            alert('success', formatMessage(messages.servicioBorrado));
            dispatch({
                type: "REMOVE_SERVICES_SUCCESS",
                idsServicio
            });
            dispatch({
                type: 'SORT_CHANGE_SERVICES',
                columnKey: connector.getColSortDirColumnKey(getState()),
                sortDir: connector.getColSortDirSortDir(getState()),
                services: connector.getServices(getState())
            });
        },
        error => {
            handleError(dispatch, error, 'REMOVE_SERVICES_FAILURE');
        }
    );
}

export const addAllServicesDelete = (services) => ({
    type: "ADD_ALL_SERVICES_DELETE",
    services
});

export const removeAllServicesDelete = () => ({
    type: "REMOVE_ALL_SERVICES_DELETE"
});

export const addIdServiceDelete = (idServicio) => ({
    type: "ADD_ID_SERVICE_DELETE",
    idServicio
});

export const removeIdServiceDelete = (idServicio) => ({
    type: "REMOVE_ID_SERVICE_DELETE",
    idServicio
});

export const openModalAddService = () => ({
    type: "OPEN_MODAL_ADD_SERVICE"
});

export const closeModalAddService = () => ({
    type: "CLOSE_MODAL_ADD_SERVICE"
});

export const openModalEditService = (servicio) => ({
    type: "OPEN_MODAL_EDIT_SERVICE",
    servicio
});

export const closeModalEditService = () => ({
    type: "CLOSE_MODAL_EDIT_SERVICE"
});