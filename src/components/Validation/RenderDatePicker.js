import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import { getDateFormat, locale } from './../../i18n';

class RenderDatePicker extends Component {

    constructor(props) {
        super(props);
        this.state = {
            startDate: props.input.value
        };
    }

    handleOnChange(value) {
        this.setState({
            startDate: value
        });
        const { change, input: { name } } = this.props;
        change(name, value);
    }

    render() {
        const { label, meta: { touched, error, invalid } } = this.props;
        const dateFormat = getDateFormat();
        return (
            <div className={`form-group ${touched && invalid ? 'has-error' : ''}`}>
                <label className="control-label">{label}</label>
                <div>
                    <DatePicker
                        dateFormat={dateFormat}
                        selected={this.state.startDate}
                        className="form-control"
                        locale={locale}
                        onChange={(oMoment) => this.handleOnChange(oMoment)}
                    />
                    <div className="help-block">
                        {touched && error && <span>{error}</span>}
                    </div>
                </div>
            </div>
        );
    }
}

export default RenderDatePicker