import React, { Component } from 'react';
import Select from 'react-select';
import messages from '../../i18n/messages';
import { formatMessage } from '../../i18n';

class RenderSelect extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.input.value
        };
    }

    onChange(option) {
        const { change, input: { name }, valueKey } = this.props;
        const value = option ? option[valueKey] : null;
        this.setState({ value: value });
        change(name, value);
    }

    render() {
        const { label, valueKey, labelKey, placeholder, name, options, clearable, 
            meta: { touched, error } } = this.props;
        const labelHtml = label ? <label className="control-label">{label}</label> : '';
        return (
            <div className={`form-group ${touched && error ? 'has-error' : ''}`}>
                {labelHtml}
                <div>
                    <Select
                        value={this.state.value}
                        valueKey={valueKey}
                        labelKey={labelKey}
                        placeholder={placeholder}
                        name={name}
                        options={options}
                        onChange={(option) => this.onChange(option)}
                        clearable={clearable}
                        noResultsText={formatMessage(messages.noResultsText)}
                    />
                    <div className="help-block">
                        {touched && error && <span>{error}</span>}
                    </div>
                </div>
            </div>
        );
    }
}

export default RenderSelect;