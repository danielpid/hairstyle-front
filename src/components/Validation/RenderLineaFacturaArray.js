import React from 'react';
import Well from 'react-bootstrap/lib/Well';
import LineaFacturaEdit from './../Home/Facturas/LineaFacturaEdit';

const wellStyle = {
    display: 'grid',
    padding: '10px 5px 0px 5px'
};

const RenderLineaFacturaArray = ({ fields, meta: { submitFailed , error }, tipoLineaFactura, fetchByName, valueKey, labelKey,
    clearValueText, placeholder, onChangeCantidad, change, touch, name }) => 
        <Well style={wellStyle} className={`form-group ${submitFailed && error ? 'has-error' : ''}`}>
            {fields.map((lineaFactura, index, fields) =>
                <LineaFacturaEdit
                    key={lineaFactura}
                    nameLineaFactura={lineaFactura}
                    tipoLineaFactura={tipoLineaFactura}
                    fetchByName={fetchByName}
                    valueKey={valueKey}
                    labelKey={labelKey}
                    clearValueText={clearValueText}
                    placeholder={placeholder}
                    change={change}
                    touch={touch}
                    nameFieldArray={name}
                    onChangeCantidad={onChangeCantidad}
                    fields={fields}
                    index={index}
                />
            )}
            <div className="help-block">
                {submitFailed && error && <span>{error}</span>}
            </div>
        </Well>

export default RenderLineaFacturaArray;