import React, { Component } from 'react';
import { AsyncCreatable } from 'react-select';
import messages from './../../i18n/messages';
import { formatMessage } from './../../i18n';

class RenderSelectCreatable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.input.value
        };
    }

    onChange(value) {
        value = value || '';
        this.setState({
            value: value,
        });
        const { change, input: { name }, additionalActions } = this.props;
        change(name, value);
        if (additionalActions) {
            additionalActions(value);
        }
    }

    render() {
        const { label, placeholder, meta: { touched, error }, loadOptions, valueKey, labelKey, clearValueText, 
            isOptionUnique, promptTextCreator } = this.props;
        const labelHtml = label ? <label className="control-label">{label}</label> : '';
        return (
            <div className={`form-group ${touched && error ? 'has-error' : ''}`}>
                {labelHtml}
                <div>
                    <AsyncCreatable
                        value={this.state.value}
                        onChange={(value) => this.onChange(value)}
                        loadOptions={loadOptions}
                        valueKey={valueKey}
                        labelKey={labelKey}
                        clearValueText={clearValueText}
                        noResultsText={formatMessage(messages.noResultsText)}
                        placeholder={placeholder}
                        searchPromptText={formatMessage(messages.searchPromptText)}
                        loadingPlaceholder={formatMessage(messages.buscando)}
                        ignoreCase={false}
                        filterOption={() => true}
                        isOptionUnique={isOptionUnique}
                        promptTextCreator={promptTextCreator}
                    />
                    <div className="help-block">
                        {touched && error && <span>{error}</span>}
                    </div>
                </div>
            </div>
        );
    }
}

export default RenderSelectCreatable;