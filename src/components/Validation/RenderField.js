import React, { Component } from 'react';

class RenderField extends Component {

    constructor(props) {
        super(props);
        this.state = {
            disabled: this.props.disabled
        };
    }

    handleChange(e) {
        const value = e.currentTarget.value || '';
        const { change, input: { name } } = this.props;
        change(name, value);
        const { additionalActions } = this.props;
        if (additionalActions) {
            additionalActions(name, value);
        }
    }

    render() {
        const { input, label, placeholder, type, step, meta: { touched, error, invalid, warning, }, style, change,
            className } = this.props;
        const inputClassName = className ? "form-control " + className : "form-control";
        const labelHtml = label ? <label className="control-label">{label}</label> : '';
        let inputHtml = '';
        if (change) {
            // with onChange
            inputHtml = <input {...input} className={inputClassName} placeholder={placeholder} type={type} step={step}
                ref="inputRef" onChange={(e) => this.handleChange(e)} style={style}
                disabled={this.state.disabled} />;
        } else {
            // without onChange
            inputHtml = <input {...input} className={inputClassName} placeholder={placeholder} type={type} step={step}
                ref="inputRef" style={style} disabled={this.state.disabled} />;
        }
        return (
            <div className={`form-group ${touched && invalid ? 'has-error' : ''}`}>
                {labelHtml}
                <div>
                    {inputHtml}
                    <div className="help-block">
                        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
                    </div>
                </div>
            </div>
        );
    }
}

export default RenderField;