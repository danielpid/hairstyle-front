import React, { Component } from 'react'
import { Checkbox } from 'react-bootstrap'

class RenderCheckBox extends Component {

    render() {
        const {label, input} = this.props;
        return (
            <Checkbox {...input}>
                {label}
            </Checkbox>
        );
    }
}

export default RenderCheckBox;