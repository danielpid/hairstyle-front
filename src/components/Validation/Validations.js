import { formatMessage } from '../../i18n';
import messages from '../../i18n/messages';

export const isEmpty = (value) =>
    !value || value === '' || (typeof value === "string" && value.trim() === '');

export const maxlength = (value, limit) =>
    typeof value === "string" && value.trim().length > limit;

export const minlength = (value, limit) =>
    typeof value === "string" && value.trim().length < limit;

export const emailER = (value) =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value);

export const isInteger = (value) =>
    !isEmpty(value) && Number.isInteger(+value);

export const isPositive = (value) =>
    !Number.isNaN(value) && parseInt(value, 10) > 0;

export const isNegative = (value) =>
    !Number.isNaN(value) && parseInt(value, 10) < 0;

export const validateNombrePeluqueria = (errors, nombrePeluqueria, ) => {
    const maxlengthNombrePeluqueria = 100;
    if (isEmpty(nombrePeluqueria)) {
        errors.nombre = formatMessage(messages.nombrePeluqueriaObligatorio);
    } else if (maxlength(nombrePeluqueria, maxlengthNombrePeluqueria)) {
        errors.nombre = formatMessage(messages.longitudMaxima, { limit: maxlengthNombrePeluqueria });
    }
}

export const validateEmail = (errors, email, dispatch) => {
    const maxlengthEmail = 100;
    if (isEmpty(email)) {
        errors.email = formatMessage(messages.emailObligatorio);
    } else if (maxlength(email, maxlengthEmail)) {
        errors.email = formatMessage(messages.longitudMaxima, { limit: maxlengthEmail });
    } else if (emailER(email)) {
        errors.email = formatMessage(messages.emailIncorrecto);
    }
}

export const validateEmailAvailability = (checkEmailAvailability, email) =>
    checkEmailAvailability(email)
        // first because if not, catches the emailEnUSo error
        .catch(error => { 
            throw { email: formatMessage(messages.errorConnection) }
        })
        .then(function (validEmail) {
            if (!validEmail) {
                throw { email: formatMessage(messages.emailEnUso) }
            }
        })

export const validatePasswordLength = (errors, password, key) => {
    const maxlengthPassword = 40;
    const minlengthPassword = 6;
    if (minlength(password, minlengthPassword)) {
        errors[key] = formatMessage(messages.passwordLongitudMinima, { limit: minlengthPassword });
    } else if (maxlength(password, maxlengthPassword)) {
        errors[key] = formatMessage(messages.longitudMaxima, { limit: maxlengthPassword });
    }
}

export const validatePasswordNueva = (errors, passwordNueva) => {
    if (isEmpty(passwordNueva)) {
        errors.passwordNueva = formatMessage(messages.passwordNuevaObligatorio);
    } else {
        validatePasswordLength(errors, passwordNueva, 'passwordNueva');
    }
}

export const validatePasswordRepita = (errors, passwordNueva, passwordRepita) => {
    if (!isEmpty(passwordNueva)
        && isEmpty(passwordRepita)) {
        errors.passwordRepita = formatMessage(messages.passwordRepitaObligatoria);
    } else if (!isEmpty(passwordNueva)
        && !isEmpty(passwordRepita)
        && passwordNueva !== passwordRepita) {
        errors.passwordRepita = formatMessage(messages.passwordRepitaDiferente);
    }
}