import React, { Component } from 'react';
import TimePicker from 'rc-time-picker';

class RenderTimePicker extends Component {

    constructor(props) {
        super(props);
        this.disabledMinutes = this.calculateDisabledMinutes();
        this.state = {
            value: ''
        };
    }

    onChange(value) {
        this.setState({
            value: value,
        });
        const { change, input: { name } } = this.props;
        change(name, value);
    }

    calculateDisabledMinutes() {
        let disabledMinutes = [];
        for (let i = 0; i < 60; i++) {
            if (i !== 0 && i % 5 !== 0) {
                disabledMinutes.push(i);
            }
        }
        return disabledMinutes;
    }

    getDisabledMinutes() {
        return this.disabledMinutes;
    }

    render() {
        const { input, label, meta: { touched, error, invalid } } = this.props;
        return (
            <div className={`form-group ${touched && invalid ? 'has-error' : ''}`}>
                <label className="control-label">{label}</label>
                <div>
                    <TimePicker
                        defaultValue={input.value ? input.value : null}
                        showSecond={false}
                        disabledMinutes={() => this.getDisabledMinutes()}
                        hideDisabledOptions={true}
                        allowEmpty={false}
                        onChange={value => this.onChange(value)}
                    />
                    <div className="help-block">
                        {touched && error && <span>{error}</span>}
                    </div>
                </div>
            </div>
        );
    }

}

export default RenderTimePicker;