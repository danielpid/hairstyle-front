import React, { Component } from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import * as connector from '../../reducers/productos/productos';
import { getProductosDeleteSorted } from '../../reducers/productos/productosDelete';
import * as connectorsPeluqueria from '../../reducers/peluqueria/peluqueria';
import * as actions from '../../actions/productos';
import * as confirmRemove from '../../actions/confirmRemove';
import messages from './../../i18n/messages';
import { Row, Col, Button, Glyphicon, ListGroup, ListGroupItem } from 'react-bootstrap';
import ProductosFixedDataTable from './ProductosFixedDataTable';
import './ProductosTableContainer.css';

class ProductosTableContainer extends Component {

    fetchProductos() {
        const { fetchProductos } = this.props;
        fetchProductos();
    }

    componentDidMount() {
        this.fetchProductos();
    }

    hasIdsProductos() {
        const { productosDelete } = this.props
        return productosDelete.length > 0;
    }

    onClickRemoveProductos() {
        const { openModalConfirmRemove, removeProductos, productosDelete } = this.props;
        const { formatMessage } = this.props.intl;
        openModalConfirmRemove(removeProductos, productosDelete,
            formatMessage(messages.producto, { num: productosDelete.length }));
    }

    render() {
        const { isFetching, productos, openModalAddProducto } = this.props;
        const { formatMessage } = this.props.intl;
        if (isFetching) {
            return (<p></p>);
        }
        let msgNoRows = "";
        if (productos.length === 0) {
            msgNoRows = (
                <ListGroup>
                    <ListGroupItem className="ProductosTableContainer-msg-no-row">{formatMessage(messages.sinResultados)}</ListGroupItem>
                </ListGroup>
            );
        }
        return (
            <div>
                <Row bsClass="row-fluid">
                    <Col md={12} sm={12} className="ProductosTableContainer-col">
                        <ProductosFixedDataTable {...this.props} />
                        {msgNoRows}
                    </Col>
                </Row>
                <Row bsClass="row-fluid">
                    <Col md={12} sm={12} className="ProductosTableContainer-col">
                        <Button bsStyle="danger" onClick={() => this.onClickRemoveProductos()} disabled={this.hasIdsProductos() ? false : true}>
                            <Glyphicon glyph="minus-sign" /> {formatMessage(messages.eliminarProductos)}
                        </Button>
                        <Button bsStyle="success" className="pull-right" onClick={() => openModalAddProducto()}>
                            <Glyphicon glyph="plus-sign" /> {formatMessage(messages.anadirProducto)}
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    productos: connector.getProductos(state),
    productosListWrapper: connector.getProductosListWrapper(state),
    allIds: connector.getAllIdsSorted(state),
    colSortDir: connector.getColSortDir(state),
    isFetching: connector.getIsFetching(state),
    productosDelete: getProductosDeleteSorted(state),
    peluqueria: connectorsPeluqueria.getDataPeluqueria(state)
});

ProductosTableContainer = connect(
    mapStateToProps,
    Object.assign(actions, confirmRemove)
)(ProductosTableContainer);

export default injectIntl(ProductosTableContainer);