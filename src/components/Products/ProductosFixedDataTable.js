import React, { Component } from 'react';
import Dimensions from 'react-dimensions';
import { Table, Column, Cell } from 'fixed-data-table';
import { injectIntl } from 'react-intl';
import isEqual from 'lodash/isEqual';
import messages from './../../i18n/messages';
import TextCell from './../FixedDataTable/TextCell';
import MoneyCell from './../FixedDataTable/MoneyCell';
import CheckboxCell from './../FixedDataTable/CheckboxCell';
import IconCell from './../FixedDataTable/IconCell';
import SortHeaderCell from './../FixedDataTable/SortHeaderCell';
import {getHeightFixedDataTable} from '../FixedDataTable/utils'

class ProductosFixedDataTable extends Component {

    constructor(props) {
        super(props);
        this.onClickCheckbox = this.onClickCheckbox.bind(this);
    }

    onClickCheckbox(input) {
        const {addIdProductoDelete, removeIdProductoDelete, productosDelete} = this.props;
        const idProducto = input.value * 1;
        if (productosDelete.includes(idProducto)) {
            removeIdProductoDelete(idProducto);
        } else {
            addIdProductoDelete(idProducto);
        }
    }

    toggleCheckAll() {
        const {addAllProductosDelete, removeAllProductosDelete, productos, allIds, productosDelete} = this.props;
        if (allIds.length > 0) {
            if (isEqual(allIds, productosDelete)) {
                removeAllProductosDelete();
            } else {
                addAllProductosDelete(productos);
            }
        }
    }

    render() {
        const {productos, productosListWrapper, productosDelete, allIds, containerWidth, containerHeight,
            sortChangeProductos, colSortDir, openModalEditProducto, peluqueria} = this.props;
        const {formatMessage} = this.props.intl;
        const cursorStyle = {
            cursor: 'pointer'
        }
        return (
            <Table
                rowHeight={40}
                headerHeight={40}
                rowsCount={productosListWrapper.getSize()}
                width={containerWidth}
                maxHeight={containerHeight}>
                <Column
                    header={
                        <Cell style={cursorStyle} onClick={() => this.toggleCheckAll()}>
                            <input
                                type="checkbox"
                                checked={allIds.length > 0 && isEqual(allIds, productosDelete)}
                                style={cursorStyle}
                            />
                        </Cell>}
                    cell={
                        <CheckboxCell
                            dataListWrapper={productosListWrapper}
                            onClickCheckbox={this.onClickCheckbox}
                            arrayDelete={productosDelete}
                            col="idProducto" />
                    }
                    fixed={true}
                    width={30} 
                    style={cursorStyle}/>
                <Column
                    columnKey="nombre"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeProductos}
                            sortDir={colSortDir.nombre}
                            data={productos}>
                            {formatMessage(messages.productos)}
                        </SortHeaderCell>}
                    cell={
                        <TextCell
                            data={productosListWrapper}
                            col="nombre" />}
                    flexGrow={3}
                    width={300} />
                <Column
                    columnKey="precio"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeProductos}
                            sortDir={colSortDir.precio}
                            data={productos}>
                            {formatMessage(messages.precio)}
                        </SortHeaderCell>}
                    cell={
                        <MoneyCell
                            data={productosListWrapper}
                            col="precio"
                            moneySymbol={peluqueria.simboloMoneda} />}
                    flexGrow={1}
                    width={100} />
                <Column
                    columnKey="stock"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeProductos}
                            sortDir={colSortDir.stock}
                            data={productos}>
                            {formatMessage(messages.stock)}
                        </SortHeaderCell>}
                    cell={
                        <TextCell
                            data={productosListWrapper}
                            col="stock" />}
                    flexGrow={1}
                    width={100} />
                <Column
                    columnKey="edit"
                    cell={
                        <IconCell
                            dataListWrapper={productosListWrapper}
                            icon="edit"
                            title={formatMessage(messages.editar)}
                            onClick={openModalEditProducto} />}
                    width={55} />
            </Table>
        );
    }
}

export default Dimensions({
    getHeight: getHeightFixedDataTable
})(injectIntl(ProductosFixedDataTable));