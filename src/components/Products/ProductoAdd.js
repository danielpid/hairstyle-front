import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import * as actions from '../../actions/productos';
import messages from './../../i18n/messages';
import validate from './ProductoValidate';
import ProductoModal from './ProductoModal';

class ProductoAdd extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const {addProducto, reset} = this.props;
        this.refs.productoModal.refs.fieldNombre.getRenderedComponent().refs.inputRef.focus();
        addProducto(values.nombre.trim(), values.precio, values.stock, reset);
    }

    render() {
        const {showModalAddProducto, closeModalAddProducto } = this.props;
        const {formatMessage} = this.props.intl;
        return (
            <ProductoModal
                showModalProp={showModalAddProducto}
                closeModalAction={closeModalAddProducto}
                validateAndCreatePost={this.validateAndCreatePost}
                idModal="addProductoModal"
                titleModal={formatMessage(messages.nuevoProducto)}
                ref='productoModal'
                {...this.props}
                {...this.props.intl} />
        );
    }
}

const reduxFormConfig = {
    form: 'productoAddForm',
    validate,
    touchOnBlur: false
};

ProductoAdd = reduxForm(
    reduxFormConfig
)(ProductoAdd);

const mapStateToProps = (state) => ({
    showModalAddProducto: state.productos.showModalAddProducto
});

ProductoAdd = connect(
    mapStateToProps,
    actions
)(ProductoAdd);

export default injectIntl(ProductoAdd);