import React from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import HairStyleNavTabs from '../HairStyleNavTabs';
import ProductosSearch from './ProductosSearch';
import ProductoAdd from './ProductoAdd';
import ProductoEdit from './ProductoEdit';
import ProductosTableContainer from './ProductosTableContainer';

const ProductsTab = () => (
    <div>
        <HairStyleNavTabs tabName='products' />
        <Grid fluid>
            <ProductosSearch />
            <ProductoAdd />
            <ProductoEdit />
            <ProductosTableContainer />
        </Grid>
    </div>
);

export default ProductsTab;