import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Button, Glyphicon } from 'react-bootstrap';
import messages from './../../i18n/messages';
import { formatMessage } from './../../i18n';
import { fetchProductosByCriteria } from '../../actions/productos';
import './ProductosSearch.css';

let ProductosSearch = ({dispatch}) => {

    function searchIfEnter(e, inputNombre, inputPrecio, inputStock) {
        if (e.keyCode === 13) {
            searchByCriteria(inputNombre, inputPrecio, inputStock);
        }
    }

    function searchByCriteria(inputNombre, inputPrecio, inputStock) {
        dispatch(fetchProductosByCriteria({
            nombre: inputNombre.value.trim(),
            precio: inputPrecio ? inputPrecio.value : "",
            stock: inputStock ? inputStock.value : ""
        }));
    }

    function clean(inputNombre, inputPrecio, inputStock) {
        inputNombre.value = "";
        inputPrecio.value = "";
        inputStock.value = "";
    }

    let inputNombre, inputPrecio, inputStock;
    return (
        <Row bsClass="row-fluid" className="ProductosSearch">
            <Col md={4} sm={4} xs={12}>
                <input
                    ref={node => { inputNombre = node; }}
                    className="form-control"
                    type="text"
                    placeholder={formatMessage(messages.nombre)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputPrecio, inputStock)}
                />
            </Col>
            <Col md={2} sm={2} xs={12}>
                <input
                    ref={node => { inputPrecio = node; }}
                    className="form-control"
                    type="number"
                    placeholder={formatMessage(messages.precio)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputPrecio, inputStock)}
                />
            </Col>
            <Col md={2} sm={2} xs={12}>
                <input
                    ref={node => { inputStock = node; }}
                    className="form-control"
                    type="number"
                    placeholder={formatMessage(messages.stock)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputPrecio, inputStock)}
                />
            </Col>
            <Col md={4} sm={4} xs={12}>
                <Button bsStyle="primary" type="button" 
                    onClick={() => searchByCriteria(inputNombre, inputPrecio, inputStock)} >
                    <Glyphicon glyph="search" /> {formatMessage(messages.buscar)}
                </Button>
                <Button type="button" style={{ marginLeft: '5px' }}
                    onClick={() => clean(inputNombre, inputPrecio, inputStock)} >
                    <Glyphicon glyph="erase" /> {formatMessage(messages.limpiar)}
                </Button>
            </Col>
        </Row>
    );
};

ProductosSearch = connect()(ProductosSearch);

export default ProductosSearch;