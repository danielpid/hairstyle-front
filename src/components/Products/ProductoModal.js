import React, { Component } from 'react';
import { Field } from 'redux-form';
import Modal from 'react-bootstrap/lib/Modal';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import renderField from './../Validation/RenderField';
import messages from './../../i18n/messages';

class ProductoModal extends Component {

    render() {
        const {showModalProp, closeModalAction, handleSubmit, formatMessage, validateAndCreatePost, reset,
            idModal, titleModal} = this.props;
        return (
            <Modal id={idModal} show={showModalProp} onHide={() => closeModalAction()} onExited={reset}>
                <form onSubmit={handleSubmit(validateAndCreatePost)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{titleModal}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup>
                            <Field
                                name="nombre" type="text" component={renderField}
                                label={formatMessage(messages.nombre) + '*'}
                                placeholder={formatMessage(messages.nombre)}
                                ref='fieldNombre'
                                withRef={true}
                            />
                            <Field
                                name="precio" type="number" step="0.01" component={renderField}
                                label={formatMessage(messages.precio) + '*'}
                                placeholder={formatMessage(messages.precio)}
                            />
                            <Field
                                name="stock" type="number" component={renderField}
                                label={formatMessage(messages.stock)}
                                placeholder={formatMessage(messages.stock)}
                            />
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={() => closeModalAction()} >
                            <Glyphicon glyph="remove" /> {formatMessage(messages.cerrar)}
                        </Button>
                        <Button bsStyle="primary" type="submit">
                            <Glyphicon glyph="floppy-disk" /> {formatMessage(messages.guardar)}
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}

export default ProductoModal;