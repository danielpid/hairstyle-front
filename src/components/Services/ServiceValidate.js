import * as validations from './../Validation/Validations';
import { formatMessage } from './../../i18n';
import messages from './../../i18n/messages';

const validate = (values) => {
    const maxlengthNombre = 100;
    const maxlengthPrecio = 7;
    const errors = {};
    // nombre
    if (validations.isEmpty(values.nombre)) {
        errors.nombre = formatMessage(messages.nombreObligatorio);
    } else if (validations.maxlength(values.nombre, maxlengthNombre)) {
        errors.nombre = formatMessage(messages.longitudMaxima, { limit: maxlengthNombre });
    }
    // precio
    if (validations.isEmpty(values.precio)) {
        errors.precio = formatMessage(messages.precioObligatorio);
    } else if (validations.maxlength(values.precio, maxlengthPrecio)) {
        errors.precio = formatMessage(messages.longitudMaxima, { limit: maxlengthPrecio });
    } else if (validations.isNegative(values.precio)) {
        errors.precio = formatMessage(messages.precioPositivo)
    }

    return errors;
};

export default validate;