import React, { Component } from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import * as connector from '../../reducers/services/services';
import { getServicesDeleteSorted } from '../../reducers/services/servicesDelete';
import * as connectorsPeluqueria from '../../reducers/peluqueria/peluqueria';
import * as actions from '../../actions/servicios';
import * as confirmRemove from '../../actions/confirmRemove';
import messages from './../../i18n/messages';
import { Row, Col, Button, Glyphicon, ListGroup, ListGroupItem } from 'react-bootstrap';
import ServicesFixedDataTable from './ServicesFixedDataTable';
import './ServicesTableContainer.css';

class ServicesTableContainer extends Component {

    fetchServices() {
        const {fetchServices} = this.props;
        fetchServices();
    }

    componentDidMount() {
        this.fetchServices();
    }

    hasIdsServicios() {
        const {servicesDelete} = this.props
        return servicesDelete.length > 0;
    }

    onClickRemoveServices() {
        const {openModalConfirmRemove, removeServices, servicesDelete} = this.props;
        const {formatMessage} = this.props.intl;
        openModalConfirmRemove(removeServices, servicesDelete, 
            formatMessage(messages.producto, {num: servicesDelete.length}));
    }

    render() {
        const {isFetching, services, openModalAddService} = this.props;
        const {formatMessage} = this.props.intl;
        if (isFetching) {
            return (<p></p>);
        }
        let msgNoRows = "";
        if (services.length === 0) {
            msgNoRows = (
                <ListGroup>
                    <ListGroupItem className="ServicesTableContainer-msg-no-row">{formatMessage(messages.sinResultados)}</ListGroupItem>
                </ListGroup>
            );
        }
        return (
            <div>
                <Row bsClass="row-fluid">
                    <Col md={12} sm={12} className="ServicesTableContainer-col">
                        <ServicesFixedDataTable {...this.props} />
                        {msgNoRows}
                    </Col>
                </Row>
                <Row bsClass="row-fluid">
                    <Col md={12} sm={12} className="ServicesTableContainer-col">
                        <Button bsStyle="danger" onClick={() => this.onClickRemoveServices()} disabled={this.hasIdsServicios() ? false : true}>
                            <Glyphicon glyph="minus-sign" /> {formatMessage(messages.eliminarServicios)}
                        </Button>
                        <Button bsStyle="success" className="pull-right" onClick={() => openModalAddService()}>
                            <Glyphicon glyph="plus-sign" /> {formatMessage(messages.anadirServicio)}
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    services: connector.getServices(state),
    servicesListWrapper: connector.getServicesListWrapper(state),
    allIds: connector.getAllIdsSorted(state),
    colSortDir: connector.getColSortDir(state),
    isFetching: connector.getIsFetching(state),
    servicesDelete: getServicesDeleteSorted(state),
    peluqueria: connectorsPeluqueria.getDataPeluqueria(state)
});

ServicesTableContainer = connect(
    mapStateToProps,
     Object.assign(actions, confirmRemove)
)(ServicesTableContainer);

export default injectIntl(ServicesTableContainer);