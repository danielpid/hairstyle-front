import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import * as actions from '../../actions/servicios';
import messages from './../../i18n/messages';
import validate from './ServiceValidate';
import ServiceModal from './ServiceModal';

class ServiceAdd extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const {addService, reset} = this.props;
        this.refs.serviceModal.refs.fieldNombre.getRenderedComponent().refs.inputRef.focus();
        addService(values.nombre.trim(), values.precio, reset);
    }

    render() {
        const {showModalAddService, closeModalAddService } = this.props;
        const {formatMessage} = this.props.intl;
        return (
            <ServiceModal
                showModalProp={showModalAddService}
                closeModalAction={closeModalAddService}
                validateAndCreatePost={this.validateAndCreatePost}
                idModal="addServicioModal"
                titleModal={formatMessage(messages.nuevoServicio)}
                ref='serviceModal'
                {...this.props}
                {...this.props.intl} />
        );
    }
}

const reduxFormConfig = {
    form: 'serviceAddForm',
    validate,
    touchOnBlur: false
};

ServiceAdd = reduxForm(
    reduxFormConfig
)(ServiceAdd);

const mapStateToProps = (state) => ({
    showModalAddService: state.services.showModalAddService
});

ServiceAdd = connect(
    mapStateToProps,
    actions
)(ServiceAdd);

export default injectIntl(ServiceAdd);