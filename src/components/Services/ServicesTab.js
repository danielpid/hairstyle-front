import React from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import HairStyleNavTabs from '../HairStyleNavTabs';
import ServicesSearch from './ServicesSearch';
import ServiceAdd from './ServiceAdd';
import ServiceEdit from './ServiceEdit';
import ServicesTableContainer from './ServicesTableContainer';

const ServicesTab = () => (
    <div>
        <HairStyleNavTabs tabName='services' />
        <Grid fluid>
            <ServicesSearch />
            <ServiceAdd />
            <ServiceEdit />
            <ServicesTableContainer />
        </Grid>
    </div>
);

export default ServicesTab;