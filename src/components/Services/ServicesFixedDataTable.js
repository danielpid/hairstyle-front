import React, { Component } from 'react';
import Dimensions from 'react-dimensions';
import { Table, Column, Cell } from 'fixed-data-table';
import { injectIntl } from 'react-intl';
import isEqual from 'lodash/isEqual';
import messages from './../../i18n/messages';
import TextCell from './../FixedDataTable/TextCell';
import MoneyCell from './../FixedDataTable/MoneyCell';
import CheckboxCell from './../FixedDataTable/CheckboxCell';
import IconCell from './../FixedDataTable/IconCell';
import SortHeaderCell from './../FixedDataTable/SortHeaderCell';
import {getHeightFixedDataTable} from '../FixedDataTable/utils'

class ServicesFixedDataTable extends Component {

    constructor(props) {
        super(props);
        this.onClickCheckbox = this.onClickCheckbox.bind(this);
    }

    onClickCheckbox(input) {
        const { addIdServiceDelete, removeIdServiceDelete, servicesDelete } = this.props;
        const idServicio = input.value * 1;
        if (servicesDelete.includes(idServicio)) {
            removeIdServiceDelete(idServicio);
        } else {
            addIdServiceDelete(idServicio);
        }
    }

    toggleCheckAll() {
        const { addAllServicesDelete, removeAllServicesDelete, services, allIds, servicesDelete } = this.props;
        if (allIds.length > 0) {
            if (isEqual(allIds, servicesDelete)) {
                removeAllServicesDelete();
            } else {
                addAllServicesDelete(services);
            }
        }
    }

    render() {
        const { services, servicesListWrapper, servicesDelete, allIds, containerWidth, containerHeight,
            sortChangeServices, colSortDir, openModalEditService, peluqueria } = this.props;
        const { formatMessage } = this.props.intl;
        const cursorStyle = {
            cursor: 'pointer'
        }
        return (
            <Table
                rowHeight={40}
                headerHeight={40}
                rowsCount={servicesListWrapper.getSize()}
                width={containerWidth}
                maxHeight={containerHeight}>
                <Column
                    header={
                        <Cell style={cursorStyle} onClick={() => this.toggleCheckAll()}>
                            <input
                                type="checkbox"
                                checked={allIds.length > 0 && isEqual(allIds, servicesDelete)}
                                style={cursorStyle}
                            />
                        </Cell>}
                    cell={
                        <CheckboxCell
                            dataListWrapper={servicesListWrapper}
                            onClickCheckbox={this.onClickCheckbox}
                            arrayDelete={servicesDelete}
                            col="idServicio" />
                    }
                    fixed={true}
                    width={30}
                    style={cursorStyle} />
                <Column
                    columnKey="nombre"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeServices}
                            sortDir={colSortDir.nombre}
                            data={services}>
                            {formatMessage(messages.servicios)}
                        </SortHeaderCell>}
                    cell={
                        <TextCell
                            data={servicesListWrapper}
                            col="nombre" />}
                    flexGrow={3}
                    width={300} />
                <Column
                    columnKey="precio"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeServices}
                            sortDir={colSortDir.precio}
                            data={services}>
                            {formatMessage(messages.precio)}
                        </SortHeaderCell>}
                    cell={
                        <MoneyCell
                            data={servicesListWrapper}
                            col="precio"
                            moneySymbol={peluqueria.simboloMoneda} />}
                    flexGrow={1}
                    width={100} />
                <Column
                    columnKey="edit"
                    cell={
                        <IconCell
                            dataListWrapper={servicesListWrapper}
                            icon="edit"
                            title={formatMessage(messages.editar)}
                            onClick={openModalEditService} />}
                    width={55} />
            </Table>
        );
    }
}

export default Dimensions({
    getHeight: getHeightFixedDataTable
})(injectIntl(ServicesFixedDataTable));