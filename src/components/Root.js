import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Provider, connect } from 'react-redux';
import { Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import Dimensions from 'react-dimensions';
import Alert from 'react-s-alert';
import * as connector from '../reducers/login';
import { XS, SM } from '../reducers/usuario';
import * as actionsUsuario from '../actions/usuario';
import * as actions from '../actions/login';
import PrivateRoute from './PrivateRoute'
import Login from './Public/Login';
import CreateAccount from './Public/CreateAccount';
import { ServiceTerms } from './Public/ServiceTerms';
import ForgotPassword from './Public/ForgotPassword';
import ResetPassword from './Public/ResetPassword';
import HomeTab from './Home/HomeTab';
import ClientsTab from './Clients/ClientsTab';
import ProductsTab from './Products/ProductsTab';
import ServicesTab from './Services/ServicesTab';
import './Root.css';

class Root extends Component {

    constructor(props) {
        super(props);
        this.xsRef = React.createRef();
        this.smRef = React.createRef();
    }

    componentWillMount() {
        const { loggedIn } = this.props;
        // handling stay logged in
        if (localStorage.getItem('hs_tkn_session')) {
            loggedIn();
        }
    }

    componentDidUpdate() {
        const {setDeviceSize} = this.props;
        const isXs = this.xsRef.current && this.xsRef.current.getBoundingClientRect().width === 0 ? true : false;
        if (isXs) {
            setDeviceSize(XS);
        } else {
            const isSm = this.smRef.current && this.smRef.current.getBoundingClientRect().width === 0 ? true : false;
            if (isSm) {
                setDeviceSize(SM);
            } else {
                setDeviceSize('other');
            }
        }
    }

    render() {
        const { containerHeight, store, isAuthenticated } = this.props;
        return (
            <div className="Root" style={{ height: containerHeight }}>
                <div ref={this.xsRef} className="hidden-xs" />
                <div ref={this.smRef} className="hidden-sm" />
                <Provider store={store}>
                    <BrowserRouter>
                        <div>
                            <Route path="/login" component={Login} />
                            <Route path="/terms" component={ServiceTerms} />
                            <Route path="/createAccount" component={CreateAccount} />
                            <Route path="/forgotPassword" component={ForgotPassword} />
                            <Route path="/resetpassword" component={ResetPassword} />
                            <PrivateRoute exact path="/" component={HomeTab} isAuthenticated={isAuthenticated} />
                            <PrivateRoute exact path="/home" component={HomeTab} isAuthenticated={isAuthenticated} />
                            <PrivateRoute path="/clients" component={ClientsTab} isAuthenticated={isAuthenticated} />
                            <PrivateRoute path="/products" component={ProductsTab} isAuthenticated={isAuthenticated} />
                            <PrivateRoute path="/services" component={ServicesTab} isAuthenticated={isAuthenticated} />
                        </div>
                    </BrowserRouter>
                </Provider>
                <Alert stack={{ limit: 3 }} position='top-right' effect='stackslide' timeout={5000} />
            </div>
        );
    }
};

Root.propTypes = {
    store: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    isAuthenticated: connector.getIsAuthenticated(state)
});

Root = connect(
    mapStateToProps,
    Object.assign({}, actions, actionsUsuario)
)(Root);

export default Dimensions({
    getHeight: () => (window.innerHeight - 50)
})(Root);