import React, { Component } from 'react'
import { connect } from 'react-redux'
import { reduxForm, Field } from 'redux-form'
import { Link } from 'react-router-dom'
import { getMonedas } from '../../reducers/monedas'
import * as actionsMonedas from '../../actions/monedas'
import * as actionsUsuarios from '../../actions/usuario'
import * as actionsLogin from '../../actions/login'
import * as actionsPeluqueria from '../../actions/peluqueria'
import * as connector from '../../reducers/login'
import NotLoggedInContainerWithWait from './NotLoggedInContainerWithWait'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import Button from 'react-bootstrap/lib/Button'
import RenderField from '../Validation/RenderField'
import RenderSelect from '../Validation/RenderSelect'
import messages from '../../i18n/messages'
import { formatMessage } from '../../i18n'
import moment from 'moment'
import validate, { asyncValidate } from './CreateAccountValidate'
import './CreateAccount.css'

class CreateAccount extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    componentWillMount() {
        const {isAuthenticated} = this.props;
        if (isAuthenticated === true) {
            this.props.history.push("/home")    
        }
    }


    componentDidMount() {
        const { fetchMonedas } = this.props;
        fetchMonedas();
    }

    validateAndCreatePost(values) {
        const { addPeluqueria } = this.props;
        addPeluqueria(values.nombre, values.email, moment().hour(9).minute(0).second(0).toDate(),
            moment().hour(21).minute(0).second(0).toDate(), values.passwordNueva, values.passwordNueva, 
            values.idMoneda, this.props.history);
    }

    render() {
        const { handleSubmit, change, monedas } = this.props;
        return (
            <NotLoggedInContainerWithWait>
                <form onSubmit={handleSubmit(this.validateAndCreatePost)}>
                    <FormGroup>
                        <Field
                            name="nombre" type="text" component={RenderField}
                            placeholder={formatMessage(messages.nombrePeluqueria)}
                        />
                        <Field
                            name="email" type="text" component={RenderField}
                            placeholder={formatMessage(messages.email)}
                        />
                        <Field
                            name="idMoneda" type="text" component={RenderSelect}
                            placeholder={formatMessage(messages.moneda)}
                            labelKey='simbolo'
                            valueKey='idMoneda'
                            options={monedas}
                            clearable={false}
                            change={change}
                        />
                        <Field
                            name="passwordNueva" type="password" component={RenderField}
                            placeholder={formatMessage(messages.password)}
                        />
                        <Field
                            name="passwordRepita" type="password" component={RenderField}
                            placeholder={formatMessage(messages.passwordRepita)}
                        />
                    </FormGroup>
                    <div className="CreateAccount-terms">
                        {formatMessage(messages.registrandomeAcepto)}
                        <Link to="/terms">
                            <Button bsStyle="link" className="CreateAccount-button-link">
                                {formatMessage(messages.terminosServicio)}
                            </Button>
                        </Link>
                    </div>
                    <Button bsStyle="success" className="CreateAccount-button" type="submit">
                        {formatMessage(messages.registrarse)}
                    </Button>
                    <div className="CreateAccount-login">
                        {formatMessage(messages.tienesCuenta)}
                        <Link to="/login">
                            <Button bsStyle="link" className="CreateAccount-button-link">
                                {formatMessage(messages.entrar)}
                            </Button>
                        </Link>
                    </div>
                </form >
            </NotLoggedInContainerWithWait>
        );
    }
}

const reduxFormConfig = {
    form: 'registerForm',
    validate,
    asyncValidate,
    asyncBlurFields: ['email']
};

CreateAccount = reduxForm(
    reduxFormConfig
)(CreateAccount);

const mapStateToProps = (state) => ({
    isAuthenticated: connector.getIsAuthenticated(state),
    monedas: getMonedas(state)
});

CreateAccount = connect(
    mapStateToProps,
    Object.assign(actionsMonedas, actionsUsuarios, actionsLogin, actionsPeluqueria)
)(CreateAccount);

export default CreateAccount;