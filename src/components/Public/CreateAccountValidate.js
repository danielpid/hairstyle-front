import * as validations from '../Validation/Validations';
import { formatMessage } from '../../i18n';
import messages from '../../i18n/messages';
import { checkEmailAvailability } from '../../api/email';

const validate = (values) => {
    const errors = {};
    // nombrePeluqueria 
    validations.validateNombrePeluqueria(errors, values.nombre);
    // email
    validations.validateEmail(errors, values.email);
    // moneda
    if (validations.isEmpty(values.idMoneda)) {
        errors.idMoneda = formatMessage(messages.monedaObligatoria);
    }
    // passwordNueva
    validations.validatePasswordNueva(errors, values.passwordNueva);
    // passwordRepita
    validations.validatePasswordRepita(errors, values.passwordNueva, values.passwordRepita);
    return errors;
}

export default validate;

export const asyncValidate = (values) => 
    validations.validateEmailAvailability(checkEmailAvailability, values.email);
