import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as connector from '../../reducers/login';
import * as actions from '../../actions/login';
import * as actionsPeluqueria from '../../actions/peluqueria';
import NotLoggedInContainer from './NotLoggedInContainer';
import { FormGroup, Alert, Button, Grid, Row, Col } from 'react-bootstrap';
import messages from '../../i18n/messages';
import { formatMessage } from '../../i18n';
import ReactPlayer from 'react-player'
import './Login.css';

class Login extends Component {

    componentWillMount() {
        const {isAuthenticated} = this.props;
        if (isAuthenticated === true) {
            this.props.history.push("/home")    
        }
    }

    doLoginIfEnter(e, email, password) {
        if (e.keyCode === 13) {
            this.doLogin(email, password);
        }
    }

    doLogin(email, password) {
        const { login } = this.props;
        login(email.value.trim(), password.value.trim(), this.props.history);
    }

    hideAlert() {
        const { hideInvalidCredentials } = this.props;
        hideInvalidCredentials();
    }

    render() {
        const { invalidCredentials, error } = this.props;
        let email, password = "";
        let msgInvalidCredentials = "";
        if (invalidCredentials) {
            msgInvalidCredentials = (
                <Alert bsStyle="danger" onDismiss={() => this.hideAlert()}>
                    <p>{formatMessage(messages.credencialesInvalidas)}</p>
                </Alert>
            );
        } else if (error) {
            msgInvalidCredentials = (
                <Alert bsStyle="danger" onDismiss={() => this.hideAlert()}>
                    <p>{formatMessage(messages.errorGenerico)}</p>
                </Alert>
            );
        }
        return (
            <Grid fluid>
                <Row bsClass="row-fluid">
                    <Col md={6} sm={12} xs={12}>
                        <NotLoggedInContainer>
                            <FormGroup>
                                <div className="form-group">
                                    <input placeholder={formatMessage(messages.email)} ref={node => email = node} className="form-control" type="text" />
                                </div>
                                <div className="form-group">
                                    <input
                                        placeholder={formatMessage(messages.password)}
                                        ref={node => password = node}
                                        className="form-control"
                                        type="password"
                                        onKeyUp={e => this.doLoginIfEnter(e, email, password)}
                                    />
                                </div>
                                <Link to="/forgotPassword">
                                    <Button bsStyle="link" className="Login-forgot">
                                        {formatMessage(messages.olvidastePassword)}
                                    </Button>
                                </Link>
                            </FormGroup>
                            {msgInvalidCredentials}
                            <Button bsStyle="success" className="Login-button" type="button" onClick={() => this.doLogin(email, password)} >
                                {formatMessage(messages.entrar)}
                            </Button>
                            <Link to="/createAccount">
                                <Button bsStyle="link" className="Login-signup">
                                    {formatMessage(messages.crearCuenta)}
                                </Button>
                            </Link>
                        </NotLoggedInContainer>
                        <div className="Login-hairstyle">
                            <p className="description">{formatMessage(messages.hairstyleDescripcion)}</p>
                            {/* <a href={formatMessage(messages.ayudaLinkYoutube)} target="_blank">{formatMessage(messages.hairstyleVerVideo)}</a> */}
                        </div>
                    </Col>
                    <Col md={6} sm={12} xs={12} style={{margin: '0 auto', marginTop: '100px'}}>
                        <ReactPlayer 
                            url={formatMessage(messages.ayudaLinkYoutube)} 
                            width={'100%'}
                            volume={0}
                            playing 
                            loop 
                            playsinline
                            config={{
                                youtube: {
                                  playerVars: { rel: 0, fs: 0, iv_load_policy: 3, modestbranding: 1 },
                                  embedOptions: { 
                                    host: 'https://www.youtube-nocookie.com' 
                                  } 
                                }
                            }}
                        />
                    </Col>
                </Row>
            </Grid>
        );
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated: connector.getIsAuthenticated(state),
    invalidCredentials: connector.getIsInvalidCredentials(state),
    error: connector.getError(state)
});

Login = connect(
    mapStateToProps,
    Object.assign({}, actions, actionsPeluqueria)
)(Login);

export default Login;