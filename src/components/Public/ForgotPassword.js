import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router-dom';
import * as actions from '../../actions/email';
import * as connector from '../../reducers/login'
import NotLoggedInContainerWithWait from './NotLoggedInContainerWithWait'
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Button from 'react-bootstrap/lib/Button';
import renderField from '../Validation/RenderField';
import messages from '../../i18n/messages';
import { formatMessage } from '../../i18n';
import validate from './ForgotPasswordValidate';
import './ForgotPassword.css';

class ForgotPassword extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    componentWillMount() {
        const {isAuthenticated} = this.props;
        if (isAuthenticated === true) {
            this.props.history.push("/home")    
        }
    }

    validateAndCreatePost(values) {
        const { sendEmailResetPassword } = this.props;
        sendEmailResetPassword(values.email);
    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <NotLoggedInContainerWithWait>
                <p className="ForgotPassword-title">{formatMessage(messages.passwordRestablecer)}</p>
                <p className="ForgotPassword-message">{formatMessage(messages.passwordRestablecerMensaje)}</p>
                <form onSubmit={handleSubmit(this.validateAndCreatePost)}>
                    <FormGroup>
                        <Field
                            name="email" type="text" component={renderField}
                            placeholder={formatMessage(messages.email)}
                        />
                    </FormGroup>
                    <Button bsStyle="success" className="ForgotPassword-button" type="submit">
                        {formatMessage(messages.enviar)}
                    </Button>
                    <div className="ForgotPassword-login">
                        {formatMessage(messages.tienesCuenta)}
                        <Link to="/login">
                            <Button bsStyle="link" className="ForgotPassword-button-link">
                                {formatMessage(messages.entrar)}
                            </Button>
                        </Link>
                    </div>
                </form >
            </NotLoggedInContainerWithWait>
        );
    }
}

const reduxFormConfig = {
    form: 'forgotPasswordForm',
    validate
};

ForgotPassword = reduxForm(
    reduxFormConfig
)(ForgotPassword);

const mapStateToProps = (state) => ({
    isAuthenticated: connector.getIsAuthenticated(state)
});

ForgotPassword = connect(
    mapStateToProps,
    actions
)(ForgotPassword);

export default ForgotPassword;