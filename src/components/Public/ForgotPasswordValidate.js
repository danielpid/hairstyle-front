import * as validations from '../Validation/Validations';

const validate = (values) => {
    const errors = {};
    // email
    validations.validateEmail(errors, values.email);
    return errors;
}

export default validate;