import React from 'react'
import { Link } from 'react-router-dom'
import messages from '../../i18n/messages'
import { formatMessage } from '../../i18n'
import { Button } from 'react-bootstrap'
import './ServiceTerms.css'

export const ServiceTerms = () => (
    <div className="ServiceTerms">
        <Link to="/createAccount">
            <Button bsStyle="link" className="ServiceTerms-button">
                {formatMessage(messages.volver)}
            </Button>
        </Link>
        <h1>{formatMessage(messages.terminosServicio)}</h1>
        <p>{formatMessage(messages.terminosServicioIntro)}</p>
        <h3>{formatMessage(messages.terminosServicioServicioTitulo)}</h3>
        <p>{formatMessage(messages.terminosServicioServicioTexto)}</p>
        <h3>{formatMessage(messages.terminosServicioRestriccionesTitulo)}</h3>
        <p>{formatMessage(messages.terminosServicioRestriccionesTexto)}</p>
        <h3>{formatMessage(messages.terminosServicioPagoTitulo)}</h3>
        <p>{formatMessage(messages.terminosServicioPagoTexto)}</p>
        <h3>{formatMessage(messages.terminosServicioPropiedadInfoTitulo)}</h3>
        <p>{formatMessage(messages.terminosServicioPropiedadInfoTexto)}</p>
        <h3>{formatMessage(messages.terminosServicioGeneralTitulo)}</h3>
        <p>{formatMessage(messages.terminosServicioGeneralTexto)}</p>
        <h3>{formatMessage(messages.terminosServicioGarantiaTitulo)}</h3>
        <p>{formatMessage(messages.terminosServicioGarantiaTexto)}</p>
        <h3>{formatMessage(messages.terminosServicioResponsabilidadTitulo)}</h3>
        <p>{formatMessage(messages.terminosServicioResponsabilidadTexto)}</p>
        <Link to="/createAccount">
            <Button bsStyle="link" className="ServiceTerms-button">
                {formatMessage(messages.volver)}
            </Button>
        </Link>
    </div>
);