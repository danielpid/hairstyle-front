import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import queryString from 'query-string'
import * as actions from '../../actions/usuario';
import { FormGroup, Button} from 'react-bootstrap';
import renderField from '../Validation/RenderField';
import messages from '../../i18n/messages';
import { formatMessage } from '../../i18n';
import validate from './ResetPasswordValidate';
import NotLoggedInContainerWithWait from './NotLoggedInContainerWithWait'
import './ResetPassword.css';

class ResetPassword extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const { resetUserPassword } = this.props;
        const token = queryString.parse(this.props.location.search).token
        resetUserPassword(token, values.passwordNueva, this.props.history);
    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <NotLoggedInContainerWithWait>
                <p className="ResetPassword-title">{formatMessage(messages.passwordRestablecer)}</p>
                <p className="ResetPassword-message">{formatMessage(messages.passwordRestablecerNuevaPassword)}</p>
                <form onSubmit={handleSubmit(this.validateAndCreatePost)}>
                    <FormGroup>
                        <Field
                            name="passwordNueva" type="password" component={renderField}
                            placeholder={formatMessage(messages.password)}
                        />
                        <Field
                            name="passwordRepita" type="password" component={renderField}
                            placeholder={formatMessage(messages.passwordRepita)}
                        />
                    </FormGroup>
                    <Button bsStyle="success" className="ResetPassword-button" type="submit">
                        {formatMessage(messages.reset)}
                    </Button>
                    <div className="ResetPassword-login">
                        {formatMessage(messages.tienesCuenta)}
                        <Button bsStyle="link" className="ResetPassword-button-link"
                            href=".">
                            {formatMessage(messages.entrar)}
                        </Button>
                    </div>
                </form >
            </NotLoggedInContainerWithWait>
        );
    }
}

const reduxFormConfig = {
    form: 'resetPasswordForm',
    validate
};

ResetPassword = reduxForm(
    reduxFormConfig
)(ResetPassword);

const mapStateToProps = (state) => ({});

ResetPassword = connect(
    mapStateToProps,
    actions
)(ResetPassword);

export default ResetPassword;