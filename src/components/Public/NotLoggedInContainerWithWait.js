import React from 'react';
import NotLoggedInContainer from './NotLoggedInContainer';
import Wait from '../Wait';

const NotLoggedInContainerWithWait = ({ children }) => 
    <div>
        <NotLoggedInContainer children={children} />
        <Wait />
    </div>

export default NotLoggedInContainerWithWait;