import * as validations from '../Validation/Validations';

const validate = (values) => {
    const errors = {};
    // passwordNueva
    validations.validatePasswordNueva(errors, values.passwordNueva);
    // passwordRepita
    validations.validatePasswordRepita(errors, values.passwordNueva, values.passwordRepita);
    return errors;
}

export default validate;