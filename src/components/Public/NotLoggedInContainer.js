import React from 'react';
import { Image } from 'react-bootstrap';
import logo from '../transparent-icon.png';
import './NotLoggedInContainer.css';

const NotLoggedInContainer = ({ children }) => 
    <div className="NotLoggedInContainer">
        <Image src={logo} rounded className="NotLoggedInContainer-image" />
        <p className="NotLoggedInContainer-name">HairStyleApp</p>
        <div className="NotLoggedInContainer-div">
            {children}
        </div>
    </div>

export default NotLoggedInContainer;