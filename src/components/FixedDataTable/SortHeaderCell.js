import React, { Component } from 'react';
import { Cell } from 'fixed-data-table';
import SortTypes from './SortTypes';
import './SortHeaderCell.css';

function reverseSortDirection(sortDir) {
    return sortDir === SortTypes.DESC ? SortTypes.ASC : SortTypes.DESC;
}

class SortHeaderCell extends Component {
    constructor(props) {
        super(props);
        this._onSortChange = this._onSortChange.bind(this);
    }

    render() {
        let { sortDir, children } = this.props;
        return (
            <Cell>
                <a onClick={this._onSortChange} className="SortHeaderCell">
                    {children} {sortDir ? (sortDir === SortTypes.DESC ? '↓' : '↑') : ''}
                </a>
            </Cell>
        );
    }

    _onSortChange(e) {
        e.preventDefault();
        if (this.props.onSortChange) {
            this.props.onSortChange(
                this.props.columnKey,
                this.props.sortDir ?
                    reverseSortDirection(this.props.sortDir) :
                    SortTypes.DESC,
                this.props.data
            );
        }
    }
}

export default SortHeaderCell;