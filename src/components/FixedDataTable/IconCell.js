import React from 'react';
import { Cell } from 'fixed-data-table';
import {Glyphicon, Tooltip, OverlayTrigger}  from 'react-bootstrap';

const IconCell = ({rowIndex, dataListWrapper, icon, title, onClick}) => {
    const servicio = dataListWrapper.getObjectAt(rowIndex);
    const tooltip = (<Tooltip id="tooltip">{title}</Tooltip>);
    const iconCellStyle = {
        marginTop: '4px'
    }
    return (
        <OverlayTrigger placement="left" overlay={tooltip}>
            <Cell 
                style={{cursor: 'pointer'}}  
                onClick={() => onClick(servicio)}>
                <Glyphicon
                    glyph={icon}
                    style={iconCellStyle} 
                />
            </Cell>
        </OverlayTrigger>
    );
};

export default IconCell;