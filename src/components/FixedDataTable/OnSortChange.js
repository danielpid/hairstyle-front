import SortTypes from './SortTypes';

const onSortChange = (columnKey, sortDir, data, oldSortIndexes) => {
    let sortIndexes = oldSortIndexes.slice();
    sortIndexes.sort((indexA, indexB) => {
        let valueA = data[indexA][columnKey];
        valueA = typeof (valueA) === "string" ? valueA.toLowerCase() : valueA;
        let valueB = data[indexB][columnKey];
        valueB = typeof (valueB) === "string" ? valueB.toLowerCase() : valueB;
        let sortVal = 0;
        if (valueA > valueB
            || (valueA == null && valueB != null)) {
            sortVal = 1;
        }
        if ((valueA < valueB && valueA != null)
            || (valueA != null && valueB == null)) {
            sortVal = -1;
        }
        if (sortVal !== 0 && sortDir === SortTypes.ASC) {
            sortVal = sortVal * -1;
        }

        return sortVal;
    });
    return sortIndexes;
}

export default onSortChange; 