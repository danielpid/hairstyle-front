import * as validations from '../Validation/Validations';
import { formatMessage } from '../../i18n';
import messages from '../../i18n/messages';
import { checkEmailAvailability } from '../../api/usuario';

const validate = (values, {initialValues}) => {
    const errors = {};
    // nombrePeluqueria 
    validations.validateNombrePeluqueria(errors, values.nombre);
    // moneda
    if (validations.isEmpty(values.idMoneda)) {
        errors.idMoneda = formatMessage(messages.monedaObligatoria);
    }
    // email
    if (initialValues 
            && values.email !== initialValues.email
            && validations.isEmpty(values.passwordEmail)) {
        validations.validateEmail(errors, values.email);
        errors.passwordEmail = formatMessage(messages.passwordEmailObligatoria);
    }
    // password
    if (!validations.isEmpty(values.passwordActual)
        || !validations.isEmpty(values.passwordNueva)
        || !validations.isEmpty(values.passwordRepita)) {
        // actual
        if (validations.isEmpty(values.passwordActual)) {
            errors.passwordActual = formatMessage(messages.passwordActualObligatorio);
        } else {
            validations.validatePasswordLength(errors, values.passwordActual, 'passwordActual');
        }
        // nueva
        validations.validatePasswordNueva(errors, values.passwordNueva);
        // passwordRepita
        validations.validatePasswordRepita(errors, values.passwordNueva, values.passwordRepita);
    }
    return errors;
};

export default validate;

export const asyncValidate = (values) =>
    validations.validateEmailAvailability(checkEmailAvailability, values.email); 