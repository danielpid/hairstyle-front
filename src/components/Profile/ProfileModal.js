import React from 'react'
import { connect } from 'react-redux'
import { reduxForm, Field } from 'redux-form'
import { Modal, FormGroup, Button, Collapse, Glyphicon } from 'react-bootstrap'
import RenderField from './../Validation/RenderField'
import RenderSelect from './../Validation/RenderSelect'
import RenderCheckBox from '../Validation/RenderCheckBox'
import messages from './../../i18n/messages'
import { formatMessage } from './../../i18n'
import * as connectors from '../../reducers/peluqueria/showModalProfile'
import { getMonedas } from '../../reducers/monedas'
import * as actions from '../../actions/peluqueria'
import * as actionsMonedas from '../../actions/monedas'
import validate, { asyncValidate } from './ProfileValidate'
import './ProfileModal.css'

class ProfileModal extends Modal {

    constructor(props) {
        super(props);
        this.state = {};
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
        this.closeModalProfileCallback = this.closeModalProfileCallback.bind(this);
    }

    componentDidMount() {
        const { fetchMonedas } = this.props;
        fetchMonedas();
    }

    validateAndCreatePost(values) {
        const { editPeluqueria, initialValues } = this.props;
        editPeluqueria(values.nombre, values.email, values.passwordEmail, initialValues.horaApertura.format('HH:mm'),
            initialValues.horaCierre.format('HH:mm'), values.passwordActual, values.passwordNueva, values.idMoneda, 
            values.mostrarTotalDiario, messages.perfilActualizado, this.closeModalProfileCallback);
    }

    toggleChangeEmail() {
        const changeEmailOpen = !this.state.changeEmailOpen;
        this.setState({ changeEmailOpen: changeEmailOpen });
        if (!changeEmailOpen) {
            const { change, initialValues: { email } } = this.props;
            change('email', email);
            change('passwordEmail', null);
        }
    }

    toggleChangePassword() {
        const changePasswordOpen = !this.state.changePasswordOpen;
        this.setState({ changePasswordOpen: changePasswordOpen });
        if (!changePasswordOpen) {
            const { change } = this.props;
            change('passwordActual', null);
            change('passwordNueva', null);
            change('passwordRepita', null);
        }
    }

    closeModalProfileCallback() {
        const { closeModalProfile, change } = this.props;
        this.setState({ changePasswordOpen: false });
        this.setState({ changeEmailOpen: false });
        change('passwordEmail', null);
        closeModalProfile();
    }

    render() {
        const { showModalProfile, handleSubmit, reset, monedas, change } = this.props;
        const symbolChangeEmailOpen = this.state.changeEmailOpen ? "-" : "+";
        const symbolChangePasswordOpen = this.state.changePasswordOpen ? "-" : "+";
        return (
            <Modal id="profileEdit" show={showModalProfile} onHide={this.closeModalProfileCallback} onExited={reset}>
                <form onSubmit={handleSubmit(this.validateAndCreatePost)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{formatMessage(messages.perfil)}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup>
                            <Field
                                name="nombre" type="text" component={RenderField}
                                label={formatMessage(messages.nombre) + ' *'}
                                placeholder={formatMessage(messages.nombre)}
                            />
                            <Field
                                name="idMoneda" type="text" component={RenderSelect}
                                label={formatMessage(messages.moneda) + ' *'}
                                placeholder={formatMessage(messages.moneda)}
                                labelKey='simbolo'
                                valueKey='idMoneda'
                                options={monedas}
                                clearable={false}
                                change={change}
                            />
                            <Field
                                name="mostrarTotalDiario"
                                type="checkbox"
                                component={RenderCheckBox}
                                label={formatMessage(messages.mostrarTotalDiario)}
                            />
                            { /* ----- EMAIL ----- */}
                            <Button onClick={() => this.toggleChangeEmail()}
                                bsStyle="link" className="ProfileModal-button-link">
                                {symbolChangeEmailOpen} {formatMessage(messages.cambiarEmail)}
                            </Button>
                            <Collapse in={this.state.changeEmailOpen}>
                                <div>
                                    <Field
                                        name="email" type="text" component={RenderField}
                                        label={formatMessage(messages.email) + ' *'}
                                        placeholder={formatMessage(messages.email)}
                                    />
                                    <Field
                                        name="passwordEmail" type="password" component={RenderField}
                                        label={formatMessage(messages.password) + ' *'}
                                        placeholder={formatMessage(messages.password)}
                                    />
                                </div>
                            </Collapse>
                            { /* ----- PASSWORD ----- */}
                            <Button onClick={() => this.toggleChangePassword()}
                                bsStyle="link" className="ProfileModal-button-link">
                                {symbolChangePasswordOpen} {formatMessage(messages.cambiarPassword)}
                            </Button>
                            <Collapse in={this.state.changePasswordOpen}>
                                <div>
                                    <Field
                                        name="passwordActual" type="password" component={RenderField}
                                        label={formatMessage(messages.passwordActual) + ' *'}
                                        placeholder={formatMessage(messages.passwordActual)}
                                    />
                                    <Field
                                        name="passwordNueva" type="password" component={RenderField}
                                        label={formatMessage(messages.passwordNueva) + ' *'}
                                        placeholder={formatMessage(messages.passwordNueva)}
                                    />
                                    <Field
                                        name="passwordRepita" type="password" component={RenderField}
                                        label={formatMessage(messages.passwordRepita) + ' *'}
                                        placeholder={formatMessage(messages.passwordRepita)}
                                    />
                                </div>
                            </Collapse>
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.closeModalProfileCallback} >
                            <Glyphicon glyph="remove" /> {formatMessage(messages.cerrar)}
                        </Button>
                        <Button bsStyle="primary" type="submit">
                            <Glyphicon glyph="floppy-disk" /> {formatMessage(messages.guardar)}
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}

const reduxFormConfig = {
    form: 'profileForm',
    validate,
    asyncValidate,
    asyncBlurFields: ['email'],
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
};

ProfileModal = reduxForm(
    reduxFormConfig
)(ProfileModal);

const mapStateToProps = (state) => ({
    showModalProfile: connectors.getOpen(state),
    initialValues: connectors.getPeluqueria(state),
    monedas: getMonedas(state)
});

ProfileModal = connect(
    mapStateToProps,
    Object.assign({}, actions, actionsMonedas)
)(ProfileModal);

export default ProfileModal;