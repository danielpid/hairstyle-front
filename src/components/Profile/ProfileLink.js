import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ButtonToolbar, DropdownButton, MenuItem, Image, Glyphicon } from 'react-bootstrap';
import { isEmpty } from 'lodash';
import * as connector from '../../reducers/peluqueria/peluqueria';
import * as actions from '../../actions/peluqueria';
import * as loginActions from '../../actions/login';
import messages from '../../i18n/messages';
import { formatMessage } from '../../i18n';
import './ProfileLink.css';

class ProfileLink extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const { fetchPeluqueria } = this.props;
        fetchPeluqueria();
    }

    onSelectMenuItem(eventKey, event) {
        const { peluqueria, openModalProfile, logout } = this.props;
        switch (eventKey) {
            case '1':
                openModalProfile(peluqueria);
                break;
            case '2':
            case '3':
                break;
            case '4':
                logout();
                break;
            default: // do nothing
        }
    }

    render() {
        const { peluqueria } = this.props;
        if (isEmpty(peluqueria)) {
            return <div />;
        }
        return (
            <div className="pull-right">
                <ButtonToolbar id="profile-button-toolbar">
                    <Image src=".../../favicon.png" circle className="ProfileLink-img hidden-xs" />
                    <DropdownButton
                        id="profile-split-button"
                        pullRight
                        bsStyle="link"
                        title={peluqueria.nombre}
                        onSelect={(eventKey, event) => this.onSelectMenuItem(eventKey, event)}
                    >
                        <MenuItem eventKey="1">
                            <Glyphicon glyph="user" className="ProfileLink-Menu-Item-icon"/>{formatMessage(messages.perfil)}
                        </MenuItem>
                        <MenuItem divider />
                        <MenuItem eventKey="2" target="_blank" href={formatMessage(messages.ayudaLinkYoutube)}>
                            <Glyphicon glyph="question-sign" className="ProfileLink-Menu-Item-icon"/> {formatMessage(messages.ayuda)}
                        </MenuItem>
                        <MenuItem divider />
                        <MenuItem eventKey="3" href="mailto:hairstylewebapp@gmail.com">
                            <Glyphicon glyph="envelope" className="ProfileLink-Menu-Item-icon"/> {formatMessage(messages.envianosEmail)}
                        </MenuItem>
                        <MenuItem divider />
                        <MenuItem eventKey="4">
                            <Glyphicon glyph="log-out" className="ProfileLink-Menu-Item-icon"/> {formatMessage(messages.cerrarSesion)}
                        </MenuItem>
                    </DropdownButton>
                </ButtonToolbar>
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    peluqueria: connector.getDataPeluqueria(state)
});

ProfileLink = connect(
    mapStateToProps,
    Object.assign(actions, loginActions)
)(ProfileLink);
export default ProfileLink;