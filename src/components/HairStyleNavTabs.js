import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as connector from '../reducers/usuario';
import { Link } from 'react-router-dom';
import { Glyphicon } from 'react-bootstrap';
import messages from '../i18n/messages';
import { formatMessage } from '../i18n';
import Wait from './Wait';
import ConfirmRemove from './ConfirmRemove';
import ProfileLink from './Profile/ProfileLink';
import ProfileModal from './Profile/ProfileModal';

class HairStyleNavTabs extends Component {

    render() {
        const { tabName, isXsDeviceSize } = this.props;
        const inicio = isXsDeviceSize ? '' : formatMessage(messages.inicio);
        const clientes = isXsDeviceSize ? '' : formatMessage(messages.clientes);
        const productos = isXsDeviceSize ? '' : formatMessage(messages.productos);
        const servicios = isXsDeviceSize ? '' : formatMessage(messages.servicios);
        return (
            <div>
                <ProfileLink />
                <ul className="nav nav-tabs">
                    <li className={tabName === 'home' ? 'active' : ''}>
                        <Link to="/home"><Glyphicon glyph="home" /> {inicio}</Link>
                    </li>
                    <li className={tabName === 'clients' ? 'active' : ''}>
                        <Link to="/clients"><Glyphicon glyph="user" /> {clientes}</Link>
                    </li>
                    <li className={tabName === 'products' ? 'active' : ''}>
                        <Link to="/products"><Glyphicon glyph="briefcase" /> {productos}</Link>
                    </li>
                    <li className={tabName === 'services' ? 'active' : ''}>
                        <Link to="/services"><Glyphicon glyph="scissors" /> {servicios}</Link>
                    </li>
                </ul>
                <Wait />
                <ConfirmRemove />
                <ProfileModal />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    isXsDeviceSize: connector.isXsDeviceSize(state)
});

HairStyleNavTabs = connect(
    mapStateToProps
)(HairStyleNavTabs);

export default HairStyleNavTabs;