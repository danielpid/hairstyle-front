import React from 'react'
import { Table } from 'react-bootstrap'
import messages from './../../../i18n/messages'
import { formatMessage } from './../../../i18n'
import './FacturaTotalTable.css'

const FacturaTotalTable = ({ total, simboloMoneda }) => (
    <Table bordered className='FacturaTotalTable'>
        <tbody>
            <tr>
                <td><b>{formatMessage(messages.total)}</b></td>
                <td style={{float: 'right'}}><b>{total} {simboloMoneda}</b></td>
                <td style={{width: '32.8px'}} className="FacturaTable-td-icon"></td>
                <td style={{width: '32.8px'}} className="FacturaTable-td-icon"></td>
            </tr>
        </tbody>
    </Table>
);

export default FacturaTotalTable;
