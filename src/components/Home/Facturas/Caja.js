import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import * as connector from '../../../reducers/facturas/facturas';
import * as connectorsPeluqueria from '../../../reducers/peluqueria/peluqueria';
import * as actions from '../../../actions/facturas';
import { isSmDeviceSize, isXsDeviceSize } from '../../../reducers/usuario';
import messages from './../../../i18n/messages';
import { Row, Col, Alert, Button, Glyphicon, InputGroup } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import FacturaPanel from './FacturaPanel';
import FacturaTotalTable from './FacturaTotalTable';
import { getDateFormat, locale, formatMessage } from './../../../i18n';
import FacturaModal from './FacturaModal';
import './Caja.css';

class Caja extends Component {

    constructor(props) {
        super(props);
        this.state = {
            referenceDate: moment()
        };
    }

    componentDidMount() {
        this.getFacturasByFecha();
    }

    getFacturasByFecha() {
        const { fetchFacturasByCriteria } = this.props;
        const fechaDesde = moment(this.state.referenceDate).startOf('day');
        const fechaHasta = moment(this.state.referenceDate).endOf('day');
        fetchFacturasByCriteria(fechaDesde, fechaHasta);
    }

    handleOnChange(value) {
        this.setState({ referenceDate: value }, this.getFacturasByFecha);
    }

    openModalFactura() {
        const { openModalFactura } = this.props;
        openModalFactura({
            fechaAlta: this.state.referenceDate,
            servicios: [{}],
            productos: [{}]
        });
    }

    render() {
        const { facturas, homeTabStyles: { height, paddingTop, paddingBottom }, isSmDeviceSize, isXsDeviceSize, peluqueria } = this.props;
        const heightHeaderCaja = 35.6;
        const facturaListStyles = {
            // height: height - (heightHeaderCaja + paddingTop + paddingBottom),
            overflow: 'auto'
        }
        const dateFormat = getDateFormat();
        let datePickerClassName = "form-control";
        if (isXsDeviceSize) {
            datePickerClassName = "form-control xs"
        } else if (isSmDeviceSize) {
            datePickerClassName = "form-control sm"
        }

        const total = facturas.reduce((valorAnterior, facturaActual) => (valorAnterior + facturaActual.precio), 0);

        let facturasList;
        if (facturas.length === 0) {
            facturasList = (
                <Alert className="Caja-alert">
                    {formatMessage(messages.sinFacturas)}
                </Alert>
            );
        } else {
            facturasList = (
                <div>
                    {facturas.map(factura =>
                        <FacturaPanel
                            key={factura.idFactura}
                            factura={factura}
                            openModalFactura={() => this.openModalFactura(factura)}
                        />
                    )}
                    {peluqueria.mostrarTotalDiario 
                        && <FacturaTotalTable total={total} simboloMoneda={peluqueria.simboloMoneda} />}
                </div>
            );
        }
        return (
            <div id="caja-component">
                <Row bsClass="row-fluid" style={{ height: heightHeaderCaja }}>
                    <Col md={4} sm={4} xs={4} className="Caja-col-date-picker">
                        <InputGroup className="Caja-calendar-input-group">
                            <InputGroup.Addon className="Caja-calendar-input-group-addon hidden-sm hidden-xs">
                                <Glyphicon glyph="calendar" />
                            </InputGroup.Addon>
                            <DatePicker
                                todayButton={formatMessage(messages.hoy)}
                                dateFormat={dateFormat}
                                selected={this.state.referenceDate}
                                className={datePickerClassName}
                                locale={locale}
                                onChange={(oMoment) => this.handleOnChange(oMoment)}
                            />
                        </InputGroup>
                    </Col>
                    <Col md={4} sm={4} xs={4}>
                        <h3 className="Caja-title">{formatMessage(messages.caja)}</h3>
                    </Col>
                    <Col md={4} sm={4} xs={4} className="Caja-col-add">
                        <Button type="button" bsStyle="success" className="Caja-button pull-right" onClick={() => this.openModalFactura()}>
                            <Glyphicon className="Caja-glyphicon" glyph="plus-sign" /> {formatMessage(messages.factura)}
                        </Button>
                    </Col>
                </Row>
                <Row bsClass="row-fluid">
                    <Col md={12} sm={12} xs={12} className="Caja-facturas-list" style={facturaListStyles}>
                        {facturasList}
                    </Col>
                </Row>
                <FacturaModal />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    facturas: connector.getFacturas(state),
    isSmDeviceSize: isSmDeviceSize(state),
    isXsDeviceSize: isXsDeviceSize(state),
    peluqueria: connectorsPeluqueria.getDataPeluqueria(state)
});

Caja = connect(
    mapStateToProps,
    actions
)(Caja);

export default Caja;