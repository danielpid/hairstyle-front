import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Table, Glyphicon, Tooltip, OverlayTrigger}  from 'react-bootstrap';
import messages from './../../../i18n/messages';
import { formatMessage, formatTime } from './../../../i18n';
import * as confirmRemove from '../../../actions/confirmRemove';
import * as connectorsPeluqueria from '../../../reducers/peluqueria/peluqueria';
import * as actions from '../../../actions/facturas';
import './FacturaTable.css';

class FacturaTable extends Component {

    showEditFacturaModal(factura) {
        const {openModalFactura} = this.props;
        openModalFactura(factura);
    }

    showConfirmDeleteModal(factura) {
        const { openModalConfirmRemove, removeFactura } = this.props;
        openModalConfirmRemove(removeFactura, factura.idFactura,
            formatMessage(messages.eliminarFactura, { nombreCliente: factura.nombreCliente }));
    }

    buildToolTip(id, msg) {
        return (<Tooltip id={`tooltip-${id}`}>{msg}</Tooltip>);
    }

    render() {
        const { onClick, factura, peluqueria } = this.props;
        return (
            <Table striped bordered className="FacturaTable">
                <tbody>
                    <tr className="FacturaTable-tr">
                        <td onClick={onClick} className="FacturaTable-td-nombre">
                            {factura.nombreCliente}
                        </td>
                        <OverlayTrigger placement="top" overlay={this.buildToolTip(`hora-${factura.idFactura}`,formatMessage(messages.horaCreacion))}>
                            <td onClick={onClick} className="FacturaTable-td-fecha">
                                {formatTime(factura.fechaAlta, {})}
                            </td>
                        </OverlayTrigger>
                        <td onClick={onClick} className="FacturaTable-td-precio">
                            {factura.precio} {peluqueria.simboloMoneda}
                        </td>
                        <OverlayTrigger placement="top" 
                                overlay={this.buildToolTip(`edit-${factura.idFactura}`, formatMessage(messages.editar))}>
                            <td onClick={() => this.showEditFacturaModal(factura)} className="FacturaTable-td-icon">
                                <Glyphicon glyph="edit"/>
                            </td>
                        </OverlayTrigger>
                        <OverlayTrigger placement="top" 
                                overlay={this.buildToolTip(`delete-${factura.idFactura}`, formatMessage(messages.eliminar))}>
                            <td onClick={() => this.showConfirmDeleteModal(factura)} className="FacturaTable-td-icon">
                                <Glyphicon glyph="trash"/>
                            </td>
                        </OverlayTrigger>
                    </tr>
                </tbody>
            </Table>
        );
    }
}

const mapStateToProps = (state) => ({
    peluqueria: connectorsPeluqueria.getDataPeluqueria(state)
});

export default FacturaTable = connect(
    mapStateToProps,
    Object.assign(actions, confirmRemove)
)(FacturaTable);