import React, { Component } from 'react';
import {Panel, ListGroup, ListGroupItem} from 'react-bootstrap';
import FacturaTable from './FacturaTable';
import LineaFacturaTable from './LineaFacturaTable';
import './FacturaPanel.css';

class FacturaPanel extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    toggleDetail() {
        this.setState({ open: !this.state.open });
    }

    render() {
        const { factura } = this.props;
        const styleFacturaPanel = {
            borderWidth: this.state.open ? '1px' : 0
        }
        return (
            <div style={{display: 'grid'}}>
                <FacturaTable
                    factura={factura}
                    onClick={() => this.toggleDetail()}
                />
                <Panel expanded={this.state.open} onToggle={()=>{}} className="FacturaPanel" style={styleFacturaPanel}>
                    <Panel.Collapse>
                        <Panel.Body>
                            <ListGroup className="FacturaPanel-list-group">
                                <ListGroupItem className="FacturaPanel-list-group-item">
                                    <LineaFacturaTable factura={factura} />
                                </ListGroupItem>
                            </ListGroup>
                        </Panel.Body>
                    </Panel.Collapse>
                </Panel>
            </div>
        );
    }
}

export default FacturaPanel;