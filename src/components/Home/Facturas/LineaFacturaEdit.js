import React, { Component } from 'react'
import { Field } from 'redux-form'
import Row from 'react-bootstrap/lib/Row'
import Col from 'react-bootstrap/lib/Col'
import { isEmpty } from 'lodash'
import RenderSelectAsync from './../../Validation/RenderSelectAsync'
import renderField from './../../Validation/RenderField'
import messages from './../../../i18n/messages'
import { formatMessage } from './../../../i18n'
import { PRODUCTO } from './FacturaModal'
import './LineaFacturaEdit.css'

class LineaFacturaEdit extends Component {

    handleOnChangeCantidad(name, value) {
        const { fields, index, nameLineaFactura, change, tipoLineaFactura } = this.props;
        const cantidad = value * 1;
        const lineaFactura = fields.get(index);
        const precio = lineaFactura.precioUnidad * (cantidad);
        change(`${nameLineaFactura}.precio`, precio);
        if (tipoLineaFactura === PRODUCTO && lineaFactura.stockOriginal) {
            // si es una actualización tendremos en cuenta cantidad de productos inicial para el cálculo del stock
            const cantidadOriginal = lineaFactura.cantidadOriginal || 0;
            const stock = lineaFactura.stockOriginal - (cantidad - cantidadOriginal);
            change(`${nameLineaFactura}.stock`, stock);
        }
    }

    handleOnSelectLineaFactura(name, value) {
        const { nameLineaFactura, tipoLineaFactura, fields, index, touch, nameFieldArray } = this.props;
        const precio = value.precio || 0;
        if (Array.isArray(value)) {
            return;
        } else if (value) {
            // se selecciono una línea de factura
            const lineaFactura = {
                cantidad: 1,
                precioUnidad: precio,
                precio: precio,
            };
            if (tipoLineaFactura === PRODUCTO) {
                lineaFactura.stockOriginal = value.stock;
                lineaFactura.stock = value.stock && value.stock - 1;
            }
            lineaFactura[tipoLineaFactura] = value;
            const currentValue = fields.get(index);
            if (isEmpty(currentValue)) {
                // añadir
                fields.insert(fields.length - 1, lineaFactura);
            } else {
                // sustituir
                fields.remove(index);
                fields.insert(index, lineaFactura);
            }
            this.refs[nameLineaFactura + '.cantidad'].getRenderedComponent().state.disabled = "";
        } else {
            // se eliminó una linea de factura
            fields.remove(index);
            touch(nameFieldArray);
            this.refs[nameLineaFactura + '.cantidad'].getRenderedComponent().state.disabled = "disabled";
        }
    }

    render() {
        const { nameLineaFactura, tipoLineaFactura, fetchByName, valueKey, labelKey, clearValueText, placeholder,
            change } = this.props;
        const disabledCantidad = this.props.fields.get(this.props.index).precio ? '' : 'disabled';
        let stockField = null;
        if (tipoLineaFactura === PRODUCTO) {
            stockField = (
                <Col md={2} sm={2} className="LineaFacturaEdit-col">
                    <Field
                        name={`${nameLineaFactura}.stock`}
                        ref={`${nameLineaFactura}.stok`}
                        type="number"
                        component={renderField}
                        placeholder={formatMessage(messages.stock)}
                        className="LineaFacturaEdit-number"
                        disabled="disabled"
                    />
                </Col>
            );
        }
        return (
            <Row bsClass="row-fluid">
                <Col md={4} sm={4} className="LineaFacturaEdit-col">
                    <Field
                        name={`${nameLineaFactura}.${tipoLineaFactura}`}
                        ref={`${nameLineaFactura}.${tipoLineaFactura}`}
                        type="text"
                        component={RenderSelectAsync}
                        loadOptions={fetchByName}
                        valueKey={valueKey}
                        labelKey={labelKey}
                        clearValueText={clearValueText}
                        placeholder={placeholder}
                        change={change}
                        additionalActions={(name, value) => this.handleOnSelectLineaFactura(name, value)}
                    />
                </Col>
                <Col md={2} sm={2} className="LineaFacturaEdit-col">
                    <Field
                        name={`${nameLineaFactura}.cantidad`}
                        ref={`${nameLineaFactura}.cantidad`}
                        type="number"
                        min="1"
                        component={renderField}
                        placeholder={formatMessage(messages.cantidad)}
                        className="LineaFacturaEdit-number"
                        change={change}
                        withRef={true}
                        disabled={disabledCantidad}
                        additionalActions={(name, value) => this.handleOnChangeCantidad(name, value)}
                    />
                </Col>
                <Col md={2} sm={2} className="LineaFacturaEdit-col">
                    <Field
                        name={`${nameLineaFactura}.precioUnidad`}
                        ref={`${nameLineaFactura}.precioUnidad`}
                        type="number"
                        component={renderField}
                        placeholder={formatMessage(messages.precioUnidad)}
                        className="LineaFacturaEdit-number"
                        disabled="disabled"
                    />
                </Col>
                <Col md={2} sm={2} className="LineaFacturaEdit-col">
                    <Field
                        name={`${nameLineaFactura}.precio`}
                        ref={`${nameLineaFactura}.precio`}
                        type="number"
                        component={renderField}
                        placeholder={formatMessage(messages.precio)}
                        className="LineaFacturaEdit-number"
                        disabled="disabled"
                    />
                </Col>
                {stockField}
            </Row>
        );
    }
}

export default LineaFacturaEdit;