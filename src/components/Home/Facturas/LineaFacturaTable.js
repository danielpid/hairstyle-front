import React from 'react';
import { connect } from 'react-redux';
import * as connectorsPeluqueria from '../../../reducers/peluqueria/peluqueria';
import Table from 'react-bootstrap/lib/Table';
import messages from './../../../i18n/messages';
import { formatMessage } from './../../../i18n';
import './LineaFacturaTable.css';

let LineaFacturaTable = ({factura, peluqueria}) => (
    <Table striped bordered condensed hover className="LineaFacturaTable">
        <thead>
            <tr>
                <th>{formatMessage(messages.descripcion)}</th>
                <th>{formatMessage(messages.cantidad)}</th>
                <th className="hidden-sm">{formatMessage(messages.precioUnidad)}</th>
                <th>{formatMessage(messages.precio)}</th>
            </tr>
        </thead>
        <tbody>
            {
                factura.detalle.map(lineaFactura =>
                    <tr key={lineaFactura.idLineaFactura + 'LF'}>
                        <td key={lineaFactura.idLineaFactura + 'Nombre'}>{lineaFactura.nombreServicio || lineaFactura.nombreProducto}</td>
                        <td key={lineaFactura.idLineaFactura + 'Ctdad'}>{lineaFactura.cantidad}</td>
                        <td className="hidden-sm" key={lineaFactura.idLineaFactura + 'Unidad'}>{lineaFactura.precioUnidad} {peluqueria.simboloMoneda}</td>
                        <td key={lineaFactura.idLineaFactura + 'Precio'}>{lineaFactura.precioLineaFactura} {peluqueria.simboloMoneda}</td>
                    </tr>
                )
            }
        </tbody>
    </Table>
);

const mapStateToProps = (state) => ({
    peluqueria: connectorsPeluqueria.getDataPeluqueria(state)
});

LineaFacturaTable = connect(
    mapStateToProps
)(LineaFacturaTable);

export default LineaFacturaTable;