import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field, FieldArray, getFormValues } from 'redux-form';
import { Modal, Form, Row, Col, Button, Glyphicon, InputGroup, FormControl } from 'react-bootstrap';
import * as apiClientes from '../../../api/clientes';
import * as apiServicios from '../../../api/servicios';
import * as apiProductos from '../../../api/productos';
import * as actions from '../../../actions/facturas';
import * as connectors from '../../../reducers/facturas/showModalFactura';
import * as connectorsPeluqueria from '../../../reducers/peluqueria/peluqueria';
import validate from './FacturaValidate';
import renderSelectCreatable from './../../Validation/RenderSelectCreatable';
import messages from './../../../i18n/messages';
import { formatMessage } from './../../../i18n';
import RenderLineaFacturaArray from './../../Validation/RenderLineaFacturaArray';
import './FacturaModal.css';

class FacturaModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            total: 0
        }
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
        this.fetchServicioByName = this.fetchServicioByName.bind(this);
        this.fetchProductoByName = this.fetchProductoByName.bind(this);
    }

    validateAndCreatePost(values) {
        const { addFactura, editFactura, closeModalFactura, initialValues } = this.props;
        const idCliente = Number.isFinite(values.cliente.idCliente) ? values.cliente.idCliente : null;
        const precio = this.refs.total.props.value;
        const detalle = [];
        values.servicios.forEach((servicio, index) => {
            if (servicio.servicio) {
                detalle.push({
                    idLineaFactura: servicio.idLineaFactura,
                    idServicio: servicio.servicio.idServicio,
                    nombreServicio: servicio.servicio.nombre,
                    cantidad: servicio.cantidad,
                    precioUnidad: servicio.servicio.precio,
                    precioLineaFactura: servicio.servicio.precio * servicio.cantidad
                });
            }
        });
        values.productos.forEach((producto, index) => {
            if (producto.producto) {
                detalle.push({
                    idLineaFactura: producto.idLineaFactura,
                    idProducto: producto.producto.idProducto,
                    nombreProducto: producto.producto.nombre,
                    cantidad: producto.cantidad,
                    precioUnidad: producto.producto.precio,
                    precioLineaFactura: producto.producto.precio * producto.cantidad
                });
            }
        });
        if (initialValues.idFactura) {
            editFactura(initialValues.idFactura, idCliente, values.cliente.nombre, initialValues.fechaAlta, precio, 
                detalle, closeModalFactura);
        } else {
            addFactura(idCliente, values.cliente.nombre, initialValues.fechaAlta, precio, detalle, closeModalFactura);
        }
    }

    // ----- CLIENTE -----

    fetchClienteByName(val) {
        if (val) {
            return apiClientes.fetchClientesByCriteria({ nombre: val }).then((json) => {
                return { options: json }
            });
        }
        return Promise.resolve({ options: [] });
    }

    isOptionClienteUnique(params) {
        for (const cliente of params.options) {
            if (cliente.nombre.toUpperCase() === params.option.nombre.toUpperCase()) {
                return false;
            }
        }
        return true;
    }

    promptClienteTextCreator(label) {
        return formatMessage(messages.crearCliente) + ' ' + label;
    }

    // ----- SERVICIO -----

    fetchServicioByName(val) {
        if (val) {
            const idsExcluir = this.props.facturaFormValues.servicios
                .filter(s => s.servicio)
                .map(s => s.servicio.idServicio);
            return apiServicios.fetchServicesByCriteria({ nombre: val, idsExcluir }).then((json) => {
                return { options: json }
            });
        }
        return Promise.resolve({ options: [] });
    }

    // ----- PRODUCTO -----

    fetchProductoByName(val) {
        if (val) {
            const idsExcluir = this.props.facturaFormValues.productos
                .filter(p => p.producto)
                .map(p => p.producto.idProducto);
            return apiProductos.fetchProductosByCriteria({ nombre: val, idsExcluir }).then((json) => {
                return { options: json }
            });
        }
        return Promise.resolve({ options: [] });
    }

    // ----- COMMON -----

    handleOnExited() {
        const { reset } = this.props;
        reset();
    }

    handleOnChangeTotal(e) {
        this.setState({ total: e.currentTarget.value });
    }

    updatePrecioTotal() {
        let total = 0;
        for (const ref of Object.values(this.refs)) {
            total += ref.state && ref.state.precio ? ref.state.precio : 0;
        }
        this.setState({ total: total });
    }

    suma(previousValue, currentValue) {
        return (previousValue !== undefined && currentValue !== undefined) ?
            previousValue + currentValue : previousValue;
    }

    // ----------

    render() {
        const { showModalFactura, closeModalFactura, handleSubmit, change, facturaFormValues, touch, 
            peluqueria } = this.props;
        const precioServicios = facturaFormValues && facturaFormValues.servicios ?
            facturaFormValues.servicios.map(servicio => servicio.precio).reduce(this.suma, 0) : 0;
        const precioProductos = facturaFormValues && facturaFormValues.productos ?
            facturaFormValues.productos.map(producto => producto.precio).reduce(this.suma, 0) : 0;
        return (
            <Modal id="modalFactura" show={showModalFactura} onHide={closeModalFactura} onExited={() => this.handleOnExited()}>
                <Form onSubmit={handleSubmit(this.validateAndCreatePost)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{formatMessage(messages.factura)}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Field
                            name="cliente" type="text" component={renderSelectCreatable}
                            label={formatMessage(messages.clienteNombre) + ' *'}
                            loadOptions={this.fetchClienteByName}
                            valueKey="idCliente"
                            labelKey="nombre"
                            clearValueText={formatMessage(messages.clienteBorrarValor)}
                            placeholder={formatMessage(messages.seleccioneCliente)}
                            isOptionUnique={this.isOptionClienteUnique}
                            promptTextCreator={this.promptClienteTextCreator}
                            change={change}
                        />
                        <label className="control-label">{formatMessage(messages.servicio)}</label>
                        <FieldArray
                            name="servicios"
                            tipoLineaFactura={SERVICIO}
                            component={RenderLineaFacturaArray}
                            fetchByName={this.fetchServicioByName}
                            valueKey="idServicio"
                            labelKey="nombre"
                            clearValueText={formatMessage(messages.servicioBorrarValor)}
                            placeholder={formatMessage(messages.seleccioneServicio)}
                            change={change}
                            touch={touch}
                            onSelect={this.onSelectServicio}
                            onChangeCantidad={() => this.updatePrecioTotal()}
                        />
                        <label className="control-label">{formatMessage(messages.productoNombre)}</label>
                        <FieldArray
                            name="productos"
                            tipoLineaFactura={PRODUCTO}
                            component={RenderLineaFacturaArray}
                            fetchByName={this.fetchProductoByName}
                            valueKey="idProducto"
                            labelKey="nombre"
                            clearValueText={formatMessage(messages.productoBorrarValor)}
                            placeholder={formatMessage(messages.seleccioneProducto)}
                            change={change}
                            touch={touch}
                            onSelect={this.onSelectProducto}
                            onChangeCantidad={() => this.updatePrecioTotal()}
                        />
                        <Row bsClass="row-fluid" className="FacturaModal-row-total">
                            <Col md={9} sm={9} className="FacturaModal-col" />
                            <Col md={3} sm={3} className="FacturaModal-col">
                                <InputGroup>
                                    <FormControl 
                                        ref="total"
                                        value={precioServicios + precioProductos}
                                        className="FacturaModal-number"
                                        placeholder={formatMessage(messages.total)}
                                        type="number"
                                        step="0.01"
                                        onChange={(e) => this.handleOnChangeTotal(e)}
                                    />
                                    <InputGroup.Addon>
                                        {peluqueria.simboloMoneda}
                                    </InputGroup.Addon>
                                </InputGroup>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={() => closeModalFactura()} >
                            <Glyphicon glyph="remove" /> {formatMessage(messages.cerrar)}
                        </Button>
                        <Button bsStyle="primary" type="submit">
                            <Glyphicon glyph="floppy-disk" /> {formatMessage(messages.guardar)}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        );
    }
}

const reduxFormConfig = {
    form: 'facturaForm',
    validate,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
};

FacturaModal = reduxForm(
    reduxFormConfig
)(FacturaModal);

const mapStateToProps = (state) => ({
    showModalFactura: connectors.getOpen(state),
    initialValues: connectors.getFactura(state),
    facturaFormValues: getFormValues('facturaForm')(state),
    peluqueria: connectorsPeluqueria.getDataPeluqueria(state)
});

FacturaModal = connect(
    mapStateToProps,
    actions
)(FacturaModal);

export default FacturaModal;

export const SERVICIO = 'servicio';
export const PRODUCTO = 'producto';