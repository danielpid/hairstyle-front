import * as validations from './../../Validation/Validations';
import { formatMessage } from './../../../i18n';
import messages from './../../../i18n/messages';
import { maxlengthNombre } from '../../Clients/ClienteValidate';

const validate = (values) => {
    const errors = {};
    // cliente
    if (validations.isEmpty(values.cliente)) {
        errors.cliente = formatMessage(messages.clienteObligatorio);
    } else if (validations.maxlength(values.cliente.nombre, maxlengthNombre)) {
        errors.cliente = formatMessage(messages.longitudMaxima, { limit: maxlengthNombre });
    }
    // at least one service or product
    if ((!values.servicios
        || values.servicios.length === 1)
        && (!values.productos
            || values.productos.length === 1)) {
        errors.servicios = { _error: formatMessage(messages.servicioProductoObligatorio) };
        errors.productos = { _error: formatMessage(messages.servicioProductoObligatorio) };
    } else {
        // validates each service 
        if (values.servicios) {
            const serviciosArrayErrors = [];
            values.servicios.forEach((servicio, index) => {
                const servicioErrors = {};
                if (servicio.servicio && (!servicio.cantidad || servicio.cantidad < 1)) {
                    servicioErrors.cantidad = formatMessage(messages.cantidadIncorrecta);
                    serviciosArrayErrors[index] = servicioErrors;
                }
            });
            if (serviciosArrayErrors.length) {
                errors.servicios = serviciosArrayErrors;
            }
        }
        // validates each product
        if (values.productos) {
            const productosArrayErrors = [];
            values.productos.forEach((producto, index) => {
                const productoErrors = {};
                if (producto.producto && (!producto.cantidad || producto.cantidad < 1)) {
                    productoErrors.cantidad = formatMessage(messages.cantidadIncorrecta);
                    productosArrayErrors[index] = productoErrors;
                }
            });
            if (productosArrayErrors.length) {
                errors.productos = productosArrayErrors;
            }
        }
    }
    return errors;
};

export default validate;