import { formatMessage, formatDate } from './../../../i18n';
import messages from './../../../i18n/messages';

// Bug not showing appoinments in agenda view
export const views = ['month', 'week', 'day', 'agenda'];
// export const views = ['month', 'week', 'day'];

export const messagesCalendar = {
    allDay: formatMessage(messages.todoElDia),
    previous: '<',
    next: '>',
    today: formatMessage(messages.hoy),
    month: formatMessage(messages.mes),
    week: formatMessage(messages.semana),
    day: formatMessage(messages.dia),
    agenda: formatMessage(messages.agenda),
    date: formatMessage(messages.fecha),
    time: formatMessage(messages.hora),
    event: formatMessage(messages.evento)
};

export const messagesCalendarXs = {
    allDay: '',
    previous: '<',
    next: '>',
    today: formatMessage(messages.hoyXs),
    month: formatMessage(messages.mesXs),
    week: formatMessage(messages.semanaXs),
    day: formatMessage(messages.diaXs),
    agenda: formatMessage(messages.agendaXs)
}

export const formats = {
    timeGutterFormat: 'HH:mm',
    dayFormat: 'ddd D',
    dayRangeHeaderFormat: ({ start, end }) =>
        getMonth(start) + ' ' + getYear(start),
    dayHeaderFormat: (date) =>
        getDayOfTheWeek(date) + ' ' + getDayOfTheMonth(date) + ' ' + getMonth(date),
    monthHeaderFormat: (date) =>
        getMonth(date) + ' ' + getYear(date),
    selectRangeFormat: ({ start, end }) =>
        getHourMinute24(start) + '-' + getHourMinute24(end),
    agendaTimeRangeFormat: ({ start, end }) =>
        getHourMinute24(start) + '-' + getHourMinute24(end),
    eventTimeRangeFormat: ({ start, end }) =>
        getHourMinute24(start) + '-' + getHourMinute24(end)
};

const getHourMinute24 = (date) =>
    formatDate(date, { hour12: false, hour: 'numeric', minute: 'numeric' })

const getDayOfTheWeek = (date) => {
    const diaSemana = formatDate(date, { weekday: 'long' });
    return diaSemana.charAt(0).toUpperCase() + diaSemana.slice(1);
}

const getDayOfTheMonth = (date) =>
    formatDate(date, { day: 'numeric' });

const getMonth = (date) => {
    const mes = formatDate(date, { month: 'long' })
    return mes.charAt(0).toUpperCase() + mes.slice(1);
}

const getYear = (date) =>
    formatDate(date, { year: 'numeric' })