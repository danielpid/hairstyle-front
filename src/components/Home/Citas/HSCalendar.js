import React, { Component } from 'react';
import { connect } from 'react-redux';
import BigCalendar from 'react-big-calendar';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd'
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import moment from 'moment';
import { isEmpty } from 'lodash';
import * as actions from '../../../actions/citas';
import * as connectorCitas from '../../../reducers/citas/citas';
import * as connectorPeluqueria from '../../../reducers/peluqueria/peluqueria';
import * as connectorUsuario from '../../../reducers/usuario';
import { locale } from './../../../i18n';
import { views, messagesCalendar, messagesCalendarXs, formats } from './HSCalendarConfig';

// Setup the localizer by providing the moment (or globalize) Object to the correct localizer.
BigCalendar.momentLocalizer(moment);
const DragAndDropCalendar = withDragAndDrop(BigCalendar);

class HSCalendar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            view: 'week',
            referenceDate: new Date()
        };
    }

    componentDidMount() {
        this.getEvents('week');
    }

    onView(value) {
        this.setState({ view: value });
        this.getEvents(value);
    }

    onNavigate(date, view) {
        this.setState({ referenceDate: date }, () => this.getEvents(view));
    }

    onSelectSlot(slotInfo) {
        const { openModalCita } = this.props;
        openModalCita({
            fecha: moment(slotInfo.start),
            inicio: moment(slotInfo.start),
            fin: moment(slotInfo.end),
            cliente: null
        });
    }

    onSelectEvent(event) {
        const { openModalCita } = this.props;
        openModalCita({
            idCita: event.idCita,
            fecha: moment(event.fechaDesde),
            inicio: moment(event.fechaDesde),
            fin: moment(event.fechaHasta),
            cliente: {
                idCliente: event.idCliente,
                nombre: event.nombreCliente
            },
            observaciones: event.observaciones
        });
    }

    onEventDrop({ event, start, end }) {
        const { editCita } = this.props;
        const newCita = {
            idCita: event.idCita,
            idCliente: event.idCliente,
            nombreCliente: event.nombreCliente,
            fechaDesde: moment(start),
            fechaHasta: moment(start).add(moment(event.fechaHasta).diff(moment(event.fechaDesde)))
        };
        editCita(newCita.idCita, newCita.idCliente, newCita.nombreCliente, newCita.fechaDesde, newCita.fechaHasta);
    }

    getEvents(view) {
        const { fetchCitasByCriteria } = this.props;
        let fechaDesde, fechaHasta;
        if (view === 'agenda') {
            fechaDesde = moment(this.state.referenceDate).locale(locale).startOf('day');
            fechaHasta = moment(this.state.referenceDate).add('M', 1).locale(locale).endOf('day');
        } else {
            fechaDesde = moment(this.state.referenceDate).locale(locale).startOf(view);
            fechaHasta = moment(this.state.referenceDate).locale(locale).endOf(view);
        }       
        // el calendario puede mostrar fechas de otros meses, por eso recuperamos citas de otros meses
        if (view === 'month') {
            fechaDesde.subtract(7, 'day');
            fechaHasta.add(7, 'day');
        } else if (view === 'day') {
            fechaDesde.subtract(1, 'day');
            fechaHasta.add(1, 'day');
        }
        fetchCitasByCriteria(fechaDesde, fechaHasta);
    }

    render() {
        const { citas, peluqueria, isXsDeviceSize, isSmDeviceSize } = this.props;
        const minHour = peluqueria.horaApertura ?
            moment(peluqueria.horaApertura, 'HH:mm').toDate() : moment().hours(8).minutes(0).seconds(0).toDate();
        const maxHour = peluqueria.horaCierre ?
            moment(peluqueria.horaCierre, 'HH:mm').toDate() : moment().hours(22).minutes(0).seconds(0).toDate();
        const messages = isXsDeviceSize || isSmDeviceSize ? messagesCalendarXs : messagesCalendar;
        if (isEmpty(peluqueria)) {
            // evita descuadre calendar
            return (
                <div />
            );
        }
        return (
            <DragAndDropCalendar
                date={this.state.referenceDate}
                selectable='ignoreEvents'
                popup
                events={citas}
                titleAccessor='nombreCliente'
                startAccessor='fechaDesde'
                endAccessor='fechaHasta'
                tooltipAccessor='nombreCliente'
                views={views}
                defaultView='week'
                culture={locale}
                min={minHour}
                max={maxHour}
                step={30}
                timeslot={10}
                onView={value => this.onView(value)}
                onNavigate={(date, view) => this.onNavigate(date, view)}
                onSelectSlot={(slotInfo) => this.onSelectSlot(slotInfo)}
                onSelectEvent={event => this.onSelectEvent(event)}
                onEventDrop={(args) => this.onEventDrop(args)}
                messages={messages}
                formats={formats}
                ref='hsCalendar'
            />
        );
    }
}

const mapStateToProps = (state) => ({
    citas: connectorCitas.getCitas(state),
    peluqueria: connectorPeluqueria.getDataPeluqueria(state),
    isXsDeviceSize: connectorUsuario.isXsDeviceSize(state),
    isSmDeviceSize: connectorUsuario.isSmDeviceSize(state)
});

HSCalendar = connect(
    mapStateToProps,
    actions
)(HSCalendar);

export default DragDropContext(HTML5Backend)(HSCalendar);
