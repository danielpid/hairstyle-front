import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import Modal from 'react-bootstrap/lib/Modal';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import { Field } from 'redux-form';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import * as connector from '../../../reducers/peluqueria/peluqueria';
import * as connectorUsuario from '../../../reducers/usuario';
import * as actions from '../../../actions/peluqueria';
import renderTimePicker from './../../Validation/RenderTimePicker';
import messages from './../../../i18n/messages';
import { formatMessage, locale } from './../../../i18n';
import validate from './../PeluqueriaValidate';
import './ConfigCalendarModal.css';

class ConfigCalendarModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModalConfigCalendar: false
        };
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const { editPeluqueria, initialValues } = this.props;
        editPeluqueria(initialValues.nombre, initialValues.email, null, values.horaApertura.seconds(0).format('HH:mm'),
            values.horaCierre.seconds(0).format('HH:mm'), null, null, initialValues.idMoneda, 
            initialValues.mostrarTotalDiario, messages.horarioActualizado, this.closeModalConfigCalendar.bind(this));
    }

    showModalConfigCalendar() {
        this.setState({ showModalConfigCalendar: true });
    }

    closeModalConfigCalendar() {
        this.setState({ showModalConfigCalendar: false });
    }

    getCogStyleClass() {
        const { isSmDeviceSize, isXsDeviceSize } = this.props;
        if (isSmDeviceSize) {
            return "ConfigCalendarModal-cog-sm";
        } else if (isXsDeviceSize) {
            return "ConfigCalendarModal-cog-xs";
        }
        return "ConfigCalendarModal-cog";
    }

    getCogInlineStyles() {
        const cogRight = locale.toLowerCase() === 'it' ? '290px' : '250px';
        return {
            right: cogRight
        }
    }

    render() {
        const { handleSubmit, change } = this.props;
        const cogStyleClass = this.getCogStyleClass();
        const cogInlineStyle = "ConfigCalendarModal-cog" === cogStyleClass ? this.getCogInlineStyles() : {};
        return (
            <div>
                <Glyphicon
                    glyph="cog"
                    onClick={() => this.showModalConfigCalendar()}
                    className={cogStyleClass}
                    style={cogInlineStyle}
                />
                <Modal id='configCalendar' show={this.state.showModalConfigCalendar} onHide={() => this.closeModalConfigCalendar()} >
                    <form onSubmit={handleSubmit(this.validateAndCreatePost)}>
                        <Modal.Header closeButton>
                            <Modal.Title>{formatMessage(messages.horario)}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <FormGroup>
                                <Field
                                    name="horaApertura" type="text" component={renderTimePicker}
                                    label={formatMessage(messages.horaInicio) + ' *'}
                                    placeholder={formatMessage(messages.horaInicio)}
                                    change={change}
                                />
                                <Field
                                    name="horaCierre" type="text" component={renderTimePicker}
                                    label={formatMessage(messages.horaFin) + ' *'}
                                    placeholder={formatMessage(messages.horaFin)}
                                    change={change}
                                />
                            </FormGroup>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={() => this.closeModalConfigCalendar()} >
                                <Glyphicon glyph="remove" /> {formatMessage(messages.cerrar)}
                            </Button>
                            <Button bsStyle="primary" type="submit">
                                <Glyphicon glyph="floppy-disk" /> {formatMessage(messages.guardar)}
                            </Button>
                        </Modal.Footer>
                    </form>
                </Modal>
            </div>
        )
    }
}

const reduxFormConfig = {
    form: 'calendarModalForm',
    validate,
    enableReinitialize: true
};

ConfigCalendarModal = reduxForm(
    reduxFormConfig
)(ConfigCalendarModal);

const mapStateToProps = (state) => ({
    initialValues: connector.getDataPeluqueria(state),
    isSmDeviceSize: connectorUsuario.isSmDeviceSize(state),
    isXsDeviceSize: connectorUsuario.isXsDeviceSize(state)
});

ConfigCalendarModal = connect(
    mapStateToProps,
    actions
)(ConfigCalendarModal);

export default ConfigCalendarModal;