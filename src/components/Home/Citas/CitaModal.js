import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { injectIntl } from 'react-intl';
import trim from 'lodash/trim';
import moment from 'moment';
import { Modal, FormGroup, Button, Glyphicon } from 'react-bootstrap';
import renderDatePicker from './../../Validation/RenderDatePicker';
import renderTimePicker from './../../Validation/RenderTimePicker';
import renderSelectCreatable from './../../Validation/RenderSelectCreatable';
import RenderField from './../../Validation/RenderField';
import messages from './../../../i18n/messages';
import { formatMessage } from './../../../i18n';
import * as connectors from '../../../reducers/citas/showModalCita';
import * as actions from '../../../actions/citas';
import validate from './CitaValidate';
import * as api from '../../../api/clientes';
import './CitaModal.css';

class CitaModal extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const { addCita, editCita } = this.props;
        const idCliente = Number.isFinite(values.cliente.idCliente) ? values.cliente.idCliente : null;
        const fecha = values.fecha;
        const horaDesde = values.inicio;
        const fechaDesde = moment(fecha).hour(horaDesde.hour()).minute(horaDesde.minute());
        const horaHasta = values.fin;
        const fechaHasta = moment(fecha).hour(horaHasta.hour()).minute(horaHasta.minute());
        const observaciones = values.observaciones;
        const idCita = this.props.initialValues.idCita;
        if (idCita) {
            editCita(idCita, idCliente, trim(values.cliente.nombre), fechaDesde, fechaHasta, trim(observaciones));
        } else {
            addCita(idCliente, trim(values.cliente.nombre), fechaDesde, fechaHasta, trim(observaciones));
        }
    }

    fetchByName(val) {
        if (val) {
            return api.fetchClientesByCriteria({ nombre: val }).then((json) => {
                return { options: json }
            });
        }
        return Promise.resolve({ options: [] });
    }

    removeCita() {
        const { removeCita, initialValues } = this.props;
        removeCita(initialValues.idCita);
    }

    isOptionUnique(params) {
        for (const cliente of params.options) {
            if (cliente.nombre.toUpperCase() === params.option.nombre.toUpperCase()) {
                return false;
            }
        }
        return true;
    }

    promptTextCreator(label) {
        return formatMessage(messages.crearCliente) + ' ' + label;
    }

    render() {
        const { showModalCita, closeModalCita, reset, handleSubmit, change, initialValues } = this.props;
        const styleDisplayNone = initialValues && initialValues.idCita ? {} : { display: 'none' };
        return (
            <Modal id='modalCita' show={showModalCita} onHide={closeModalCita} onExited={reset}>
                <form onSubmit={handleSubmit(this.validateAndCreatePost)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{formatMessage(messages.cita)}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup>
                            <Field
                                name="fecha" type="text" component={renderDatePicker}
                                label={formatMessage(messages.dia) + ' *'}
                                placeholder={formatMessage(messages.dia)}
                                change={change}
                            />
                            <Field
                                name="inicio" type="text" component={renderTimePicker}
                                label={formatMessage(messages.horaInicio) + ' *'}
                                placeholder={formatMessage(messages.horaInicio)}
                                change={change}
                            />
                            <Field
                                name="fin" type="text" component={renderTimePicker}
                                label={formatMessage(messages.horaFin) + ' *'}
                                placeholder={formatMessage(messages.horaFin)}
                                change={change}
                            />
                            <Field
                                name="cliente" type="text" component={renderSelectCreatable}
                                label={formatMessage(messages.clienteNombre) + ' *'}
                                loadOptions={this.fetchByName}
                                valueKey="idCliente"
                                labelKey="nombre"
                                clearValueText={formatMessage(messages.clienteBorrarValor)}
                                placeholder={formatMessage(messages.seleccioneCliente)}
                                isOptionUnique={this.isOptionUnique}
                                promptTextCreator={this.promptTextCreator}
                                change={change}
                            />
                            <Field
                                name="observaciones" type="text" component={RenderField}
                                label={formatMessage(messages.observaciones)}
                                placeholder={formatMessage(messages.observaciones)}
                                className='observaciones'
                            />
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button style={styleDisplayNone} bsStyle="danger" className="pull-left" onClick={() => this.removeCita()} >
                            <Glyphicon glyph="trash" /> {formatMessage(messages.eliminar)}
                        </Button>
                        <Button onClick={() => closeModalCita()} >
                            <Glyphicon glyph="remove" /> {formatMessage(messages.cerrar)}
                        </Button>
                        <Button bsStyle="primary" type="submit">
                            <Glyphicon glyph="floppy-disk" /> {formatMessage(messages.guardar)}
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}

const reduxFormConfig = {
    form: 'citaForm',
    validate,
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
};

CitaModal = reduxForm(
    reduxFormConfig
)(CitaModal);

const mapStateToProps = (state) => ({
    showModalCita: connectors.getOpen(state),
    initialValues: connectors.getCita(state)
});

CitaModal = connect(
    mapStateToProps,
    actions
)(CitaModal);

export default injectIntl(CitaModal);