import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import HairStyleNavTabs from '../HairStyleNavTabs';
import HSCalendar from './Citas/HSCalendar';
import CitaModal from './Citas/CitaModal';
import ConfigCalendarModal from './Citas/ConfigCalendarModal';
import Caja from './Facturas/Caja';

class HomeTab extends Component {

    render() {
        const height = window.innerHeight - 120;
        const cssStyles = {
            // height: '100%',
            paddingTop: 25,
            paddingBottom: 25,
            paddingRight: 0,
            paddingLeft: 0
        };
        return (
            <div>
                <HairStyleNavTabs tabName='home' />
                <Grid fluid>
                    <Row bsClass="row-fluid">
                        <Col md={8} sm={7} xs={12} style={cssStyles}>
                            <ConfigCalendarModal />
                            <HSCalendar />
                            <CitaModal />
                        </Col>
                        <Col md={4} sm={5} xs={12} style={cssStyles}>
                            <Caja homeTabStyles={cssStyles} />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default HomeTab;