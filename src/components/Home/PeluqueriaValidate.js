import * as validations from './../Validation/Validations';
import { formatMessage } from './../../i18n';
import messages from './../../i18n/messages';

const validate = (values) => {
    const errors = {};
    // inicio
    if (validations.isEmpty(values.horaApertura)) {
        errors.horaApertura = formatMessage(messages.horaInicioObligatorio);
    }
    // fin
    if (validations.isEmpty(values.horaCierre)) {
        errors.horaCierre = formatMessage(messages.horaFinObligatorio);
    }
    // inicio > fin
    if (!validations.isEmpty(values.horaApertura)
        && !validations.isEmpty(values.horaCierre)
        && values.horaApertura.isAfter(values.horaCierre)) {
        errors.horaCierre = formatMessage(messages.horaInicioMayorFin);
    }
    return errors;
};

export default validate;