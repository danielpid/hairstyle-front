import * as validations from './../Validation/Validations';
import { formatMessage } from './../../i18n';
import messages from './../../i18n/messages';

export const maxlengthNombre = 100;

const validate = (values) => {
    const maxlengthObservaciones = 255;
    const maxlengthDescripcionMedioComunicacion = 100;
    const errors = {};
    // nombre
    if (validations.isEmpty(values.nombre)) {
        errors.nombre = formatMessage(messages.nombreObligatorio);
    } else if (validations.maxlength(values.nombre, maxlengthNombre)) {
        errors.nombre = formatMessage(messages.longitudMaxima, { limit: maxlengthNombre });
    }
    // observaciones
    if (validations.maxlength(values.observaciones, maxlengthObservaciones)) {
        errors.observaciones = formatMessage(messages.longitudMaxima, { limit: maxlengthObservaciones });
    }
    // movil
    if (validations.maxlength(values.descripcionMedioComunicacionMovil, maxlengthDescripcionMedioComunicacion)) {
        errors.descripcionMedioComunicacionMovil = formatMessage(messages.longitudMaxima,
            { limit: maxlengthDescripcionMedioComunicacion });
    }
    // casa
    if (validations.maxlength(values.descripcionMedioComunicacionCasa, maxlengthDescripcionMedioComunicacion)) {
        errors.descripcionMedioComunicacionCasa = formatMessage(messages.longitudMaxima,
            { limit: maxlengthDescripcionMedioComunicacion });
    }
    // trabajo
    if (validations.maxlength(values.descripcionMedioComunicacionTrabajo, maxlengthDescripcionMedioComunicacion)) {
        errors.descripcionMedioComunicacionTrabajo = formatMessage(messages.longitudMaxima,
            { limit: maxlengthDescripcionMedioComunicacion });
    }
    // email
    if (validations.maxlength(values.descripcionMedioComunicacionEmail, maxlengthDescripcionMedioComunicacion)) {
        errors.descripcionMedioComunicacionEmail = formatMessage(messages.longitudMaxima,
            { limit: maxlengthDescripcionMedioComunicacion });
    } else if (validations.emailER(values.descripcionMedioComunicacionEmail)) {
        errors.descripcionMedioComunicacionEmail = formatMessage(messages.emailIncorrecto);
    }

    return errors;
};

export default validate;