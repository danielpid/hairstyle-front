import React, { Component } from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import * as connector from '../../reducers/clientes/clientes';
import { getClientesDeleteSorted } from '../../reducers/clientes/clientesDelete';
import * as actions from '../../actions/clientes';
import * as confirmRemove from '../../actions/confirmRemove';
import messages from './../../i18n/messages';
import { Row, Col, Button, Glyphicon, ListGroup, ListGroupItem } from 'react-bootstrap';
import ClientesFixedDataTable from './ClientesFixedDataTable';
import './ClientesTableContainer.css';

class ClientesTableContainer extends Component {
    
    fetchClientes() {
        const {fetchClientes} = this.props;
        fetchClientes();
    }

    componentDidMount() {
        this.fetchClientes();
    }

    hasIdsClientes() {
        const {clientesDelete} = this.props
        return clientesDelete.length > 0;
    }

    onClickRemoveClientes() {
        const {openModalConfirmRemove, removeClientes, clientesDelete} = this.props;
        const {formatMessage} = this.props.intl;
        openModalConfirmRemove(removeClientes, clientesDelete,
            formatMessage(messages.cliente, { num: clientesDelete.length }));
    }

    render() {
        const {isFetching, clientes, openModalAddCliente} = this.props;
        const {formatMessage} = this.props.intl;
        if (isFetching) {
            return (<p></p>);
        }
        let msgNoRows = "";
        if (clientes.length === 0) {
            msgNoRows = (
                <ListGroup>
                    <ListGroupItem className="ClientesTableContainer-msg-no-row">{formatMessage(messages.sinResultados)}</ListGroupItem>
                </ListGroup>
            );
        }
        return (
            <div>
                <Row bsClass="row-fluid">
                    <Col md={12} sm={12} className="ClientesTableContainer-col">
                        <ClientesFixedDataTable {...this.props} />
                        {msgNoRows}
                    </Col>
                </Row>
                <Row bsClass="row-fluid">
                    <Col md={12} sm={12} className="ClientesTableContainer-col">
                        <Button bsStyle="danger" onClick={() => this.onClickRemoveClientes()} disabled={this.hasIdsClientes() ? false : true}>
                            <Glyphicon glyph="minus-sign" /> {formatMessage(messages.eliminarClientes)}
                        </Button>
                        <Button bsStyle="success" className="pull-right" onClick={() => openModalAddCliente()}>
                            <Glyphicon glyph="plus-sign" /> {formatMessage(messages.anadirCliente)}
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    clientes: connector.getClientes(state),
    clientesListWrapper: connector.getClientesListWrapper(state),
    allIds: connector.getAllIdsSorted(state),
    colSortDir: connector.getColSortDir(state),
    isFetching: connector.getIsFetching(state),
    clientesDelete: getClientesDeleteSorted(state)
});

ClientesTableContainer = connect(
    mapStateToProps,
    Object.assign(actions, confirmRemove)
)(ClientesTableContainer);

export default injectIntl(ClientesTableContainer);