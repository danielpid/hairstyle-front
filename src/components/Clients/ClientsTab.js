import React from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import HairStyleNavTabs from '../HairStyleNavTabs';
import ClientesSearch from './ClientesSearch';
import ClientesTableContainer from './ClientesTableContainer';
import ClienteAdd from './ClienteAdd';
import ClienteEdit from './ClienteEdit';

const ClientsTab = () => (
    <div>
        <HairStyleNavTabs tabName='clients' />
        <Grid fluid>
            <ClientesSearch />
            <ClientesTableContainer />
            <ClienteAdd />
            <ClienteEdit />
        </Grid>
    </div>
);

export default ClientsTab;