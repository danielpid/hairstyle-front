import React from 'react';
import { connect } from 'react-redux';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import trim from 'lodash/trim';
import messages from './../../i18n/messages';
import { formatMessage } from './../../i18n';
import { fetchClientesByCriteria } from '../../actions/clientes';
import './ClientesSearch.css';

let ClientesSearch = ({dispatch}) => {

    function searchIfEnter(e, inputNombre, inputObservaciones, inputMedioComunicacion) {
        if (e.keyCode === 13) {
            searchByCriteria(inputNombre, inputObservaciones, inputMedioComunicacion);
        }
    }

    function searchByCriteria(inputNombre, inputObservaciones, inputMedioComunicacion) {
        dispatch(fetchClientesByCriteria({
            nombre: trim(inputNombre.value),
            observaciones: trim(inputObservaciones.value),
            descripcionMedioComunicacion: trim(inputMedioComunicacion.value)
        }));
    }

    function clean(inputNombre, inputObservaciones, inputMedioComunicacion) {
        inputNombre.value = "";
        inputObservaciones.value = "";
        inputMedioComunicacion.value = "";
    }

    let inputNombre, inputObservaciones, inputMedioComunicacion;
    return (
        <Row bsClass="row-fluid" className="ClientesSearch">
            <Col md={3} sm={4} xs={12}>
                <input
                    ref={node => { inputNombre = node; }}
                    className="form-control"
                    type="text"
                    placeholder={formatMessage(messages.nombre)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputObservaciones, inputMedioComunicacion)}
                />
            </Col>
            <Col md={3} sm={4} xs={12} smHidden>
                <input
                    ref={node => { inputObservaciones = node; }}
                    className="form-control"
                    type="text"
                    placeholder={formatMessage(messages.observaciones)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputObservaciones, inputMedioComunicacion)}
                />
            </Col>
            <Col md={3} sm={4} xs={12}>
                <input
                    ref={node => { inputMedioComunicacion = node; }}
                    className="form-control"
                    type="text"
                    placeholder={formatMessage(messages.medioComunicacion)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputObservaciones, inputMedioComunicacion)}
                />
            </Col>
            <Col md={3} sm={4} xs={12}>
                <Button bsStyle="primary" type="button" 
                    onClick={() => searchByCriteria(inputNombre, inputObservaciones, inputMedioComunicacion)} >
                    <Glyphicon glyph="search" /> {formatMessage(messages.buscar)}
                </Button>
                <Button type="button" style={{ marginLeft: '5px' }}
                    onClick={() => clean(inputNombre, inputObservaciones, inputMedioComunicacion)} >
                    <Glyphicon glyph="erase" /> {formatMessage(messages.limpiar)}
                </Button>
            </Col>
        </Row>
    );
};

ClientesSearch = connect()(ClientesSearch);

export default ClientesSearch;