import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import trim from 'lodash/trim';
import * as actions from '../../actions/clientes';
import messages from './../../i18n/messages';
import validate from './ClienteValidate';
import ClienteModal from './ClienteModal';

class ClienteAdd extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const { addCliente, reset } = this.props;
        this.refs.clienteModal.refs.fieldNombre.getRenderedComponent().refs.inputRef.focus();
        addCliente(trim(values.nombre), trim(values.observaciones), trim(values.descripcionMedioComunicacionMovil), 
            trim(values.descripcionMedioComunicacionCasa), trim(values.descripcionMedioComunicacionTrabajo), 
            trim(values.descripcionMedioComunicacionEmail), reset);
    }

    render() {
        const { showModalAddCliente, closeModalAddCliente } = this.props;
        const { formatMessage } = this.props.intl;
        return (
            <ClienteModal
                showModalProp={showModalAddCliente}
                closeModalAction={closeModalAddCliente}
                validateAndCreatePost={this.validateAndCreatePost}
                idModal="addClienteModal"
                titleModal={formatMessage(messages.nuevoCliente)}
                ref='clienteModal'
                {...this.props}
                {...this.props.intl} />
        );
    }
}

const reduxFormConfig = {
    form: 'clienteAddForm',
    validate,
    touchOnBlur: false
};

ClienteAdd = reduxForm(
    reduxFormConfig
)(ClienteAdd);

const mapStateToProps = (state) => ({
    showModalAddCliente: state.clientes.showModalAddCliente
});

ClienteAdd = connect(
    mapStateToProps,
    actions
)(ClienteAdd);

export default injectIntl(ClienteAdd);