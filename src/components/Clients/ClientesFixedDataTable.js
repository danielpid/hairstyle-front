import React, { Component } from 'react';
import Dimensions from 'react-dimensions';
import { Table, Column, Cell } from 'fixed-data-table';
import { injectIntl } from 'react-intl';
import isEqual from 'lodash/isEqual';
import messages from './../../i18n/messages';
import TextCell from './../FixedDataTable/TextCell';
import CheckboxCell from './../FixedDataTable/CheckboxCell';
import IconCell from './../FixedDataTable/IconCell';
import SortHeaderCell from './../FixedDataTable/SortHeaderCell';
import {getHeightFixedDataTable} from '../FixedDataTable/utils'

class ClientesFixedDataTable extends Component {

    constructor(props) {
        super(props);
        this.onClickCheckbox = this.onClickCheckbox.bind(this);
    }

    onClickCheckbox(input) {
        const {addIdClienteDelete, removeIdClienteDelete, clientesDelete} = this.props;
        const idCliente = input.value * 1;
        if (clientesDelete.includes(idCliente)) {
            removeIdClienteDelete(idCliente);
        } else {
            addIdClienteDelete(idCliente);
        }
    }

    toggleCheckAll() {
        const {addAllClientesDelete, removeAllClientesDelete, clientes, allIds, clientesDelete} = this.props;
        if (allIds.length > 0) {
            if (isEqual(allIds, clientesDelete)) {
                removeAllClientesDelete();
            } else {
                addAllClientesDelete(clientes);
            }
        }
    }

    render() {
        const {clientes, clientesListWrapper, clientesDelete, allIds, containerWidth, containerHeight,
            sortChangeClientes, colSortDir, openModalEditCliente} = this.props;
        const {formatMessage} = this.props.intl;
        const cursorStyle = {
            cursor: 'pointer'
        }
        return (
            <Table
                rowHeight={40}
                headerHeight={40}
                rowsCount={clientesListWrapper.getSize()}
                width={containerWidth}
                maxHeight={containerHeight}>
                <Column
                    header={
                        <Cell style={cursorStyle} onClick={() => this.toggleCheckAll()}>
                            <input
                                type="checkbox"
                                checked={allIds.length > 0 && isEqual(allIds, clientesDelete)}
                                style={cursorStyle}
                            />
                        </Cell>}
                    cell={
                        <CheckboxCell
                            dataListWrapper={clientesListWrapper}
                            onClickCheckbox={this.onClickCheckbox}
                            arrayDelete={clientesDelete}
                            col="idCliente" />
                    }
                    fixed={true}
                    width={30} 
                    style={cursorStyle}/>
                <Column
                    columnKey="nombre"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeClientes}
                            sortDir={colSortDir.nombre}
                            data={clientes}>
                            {formatMessage(messages.clientes)}
                        </SortHeaderCell>}
                    cell={
                        <TextCell
                            data={clientesListWrapper}
                            col="nombre" />}
                            flexGrow={1.5}
                            width={150} />
                <Column
                    columnKey="observaciones"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeClientes}
                            sortDir={colSortDir.observaciones}
                            data={clientes}>
                            {formatMessage(messages.observaciones)}
                        </SortHeaderCell>}
                    cell={
                        <TextCell
                            data={clientesListWrapper}
                            col="observaciones" />}
                            flexGrow={3}
                            width={300} />
                <Column
                    columnKey="descripcionMedioComunicacionMovil"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeClientes}
                            sortDir={colSortDir.descripcionMedioComunicacionMovil}
                            data={clientes}>
                            {formatMessage(messages.movil)}
                        </SortHeaderCell>}
                    cell={
                        <TextCell
                            data={clientesListWrapper}
                            col="descripcionMedioComunicacionMovil" />}
                    flexGrow={1}
                    width={100} />
                <Column
                    columnKey="descripcionMedioComunicacionCasa"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeClientes}
                            sortDir={colSortDir.descripcionMedioComunicacionCasa}
                            data={clientes}>
                            {formatMessage(messages.casa)}
                        </SortHeaderCell>}
                    cell={
                        <TextCell
                            data={clientesListWrapper}
                            col="descripcionMedioComunicacionCasa" />}
                    flexGrow={1}
                    width={100} />
                <Column
                    columnKey="descripcionMedioComunicacionTrabajo"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeClientes}
                            sortDir={colSortDir.descripcionMedioComunicacionTrabajo}
                            data={clientes}>
                            {formatMessage(messages.trabajo)}
                        </SortHeaderCell>}
                    cell={
                        <TextCell
                            data={clientesListWrapper}
                            col="descripcionMedioComunicacionTrabajo" />}
                    flexGrow={1}
                    width={100} />
                <Column
                    columnKey="descripcionMedioComunicacionEmail"
                    header={
                        <SortHeaderCell
                            onSortChange={sortChangeClientes}
                            sortDir={colSortDir.descripcionMedioComunicacionEmail}
                            data={clientes}>
                            {formatMessage(messages.email)}
                        </SortHeaderCell>}
                    cell={
                        <TextCell
                            data={clientesListWrapper}
                            col="descripcionMedioComunicacionEmail" />}
                    flexGrow={1.5}
                    width={150} />
                <Column
                    columnKey="edit"
                    cell={
                        <IconCell
                            dataListWrapper={clientesListWrapper}
                            icon="edit"
                            title={formatMessage(messages.editar)}
                            onClick={openModalEditCliente} />}
                    width={55} />
            </Table>
        );
    }
}

export default Dimensions({
    getHeight: getHeightFixedDataTable
})(injectIntl(ClientesFixedDataTable));