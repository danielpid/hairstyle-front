import React, { Component } from 'react';
import { Field } from 'redux-form';
import Modal from 'react-bootstrap/lib/Modal';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import renderField from './../Validation/RenderField';
import messages from './../../i18n/messages';

class ClienteModal extends Component {

    render() {
        const {showModalProp, closeModalAction, handleSubmit, formatMessage, validateAndCreatePost, reset,
            idModal, titleModal} = this.props;
        return (
            <Modal id={idModal} show={showModalProp} onHide={() => closeModalAction()} onExited={reset}>
                <form onSubmit={handleSubmit(validateAndCreatePost)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{titleModal}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup>
                            <Field
                                name="nombre" type="text" component={renderField}
                                label={formatMessage(messages.nombre) + '*'}
                                placeholder={formatMessage(messages.nombre)}
                                ref='fieldNombre'
                                withRef={true}
                            />
                            <Field
                                name="observaciones" type="text" component={renderField}
                                label={formatMessage(messages.observaciones)}
                                placeholder={formatMessage(messages.observaciones)}
                            />
                            <Field
                                name="descripcionMedioComunicacionMovil" type="text" component={renderField}
                                label={formatMessage(messages.movil)}
                                placeholder={formatMessage(messages.movil)}
                            />
                            <Field
                                name="descripcionMedioComunicacionCasa" type="text" component={renderField}
                                label={formatMessage(messages.casa)}
                                placeholder={formatMessage(messages.casa)}
                            />
                            <Field
                                name="descripcionMedioComunicacionTrabajo" type="text" component={renderField}
                                label={formatMessage(messages.trabajo)}
                                placeholder={formatMessage(messages.trabajo)}
                            />
                            <Field
                                name="descripcionMedioComunicacionEmail" type="text" component={renderField}
                                label={formatMessage(messages.email)}
                                placeholder={formatMessage(messages.email)}
                            />
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={() => closeModalAction()} >
                            <Glyphicon glyph="remove" /> {formatMessage(messages.cerrar)}
                        </Button>
                        <Button bsStyle="primary" type="submit">
                            <Glyphicon glyph="floppy-disk" /> {formatMessage(messages.guardar)}
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}

export default ClienteModal;