import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from './configureStore';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import 'fixed-data-table/dist/fixed-data-table.css';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/stackslide.css';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import 'react-big-calendar/lib/addons/dragAndDrop/styles.css';
import 'react-datepicker/dist/react-datepicker.css';
import 'rc-time-picker/assets/index.css';
import 'react-select/dist/react-select.css';
import './hairstyle.css';
// import App from './App';
import Root from './components/Root';
import { IntlProvider } from 'react-intl';
import { getLanguage, getLanguageWithoutRegionCode, getMessages } from './i18n';
// import registerServiceWorker from './registerServiceWorker';

// i18n
const language = getLanguage();
const languageWithoutRegionCode = getLanguageWithoutRegionCode(language);
const messages = getMessages(language, languageWithoutRegionCode);
// redux
const store = configureStore();

ReactDOM.render(
    <IntlProvider locale={languageWithoutRegionCode} messages={messages}>
        <Root store={store} />
    </IntlProvider>, 
    document.getElementById('root')
);
// registerServiceWorker();
