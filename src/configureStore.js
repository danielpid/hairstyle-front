import { createStore, applyMiddleware, compose  } from 'redux';
import thunk from 'redux-thunk';
import hairStyleAppReducers from './reducers';

const configureStore = () => {
    const middlewares = [thunk];
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    return createStore(
        hairStyleAppReducers,
        composeEnhancers(applyMiddleware(...middlewares))
    );
};

export default configureStore;