const productoDelete = (state, action) => {
    switch (action.type) {
        case "ADD_ID_PRODUCTO_DELETE":
            return [...state, action.idProducto];
        case "REMOVE_ID_PRODUCTO_DELETE":
            return state.filter((idProducto) => idProducto !== action.idProducto);
        case "ADD_ALL_PRODUCTOS_DELETE":
            return action.productos.map(producto => producto.idProducto);
        default:
            return state;
    }
};

const productosDelete = (state = [], action) => {
    switch (action.type) {
        case "ADD_ID_PRODUCTO_DELETE":
        case "REMOVE_ID_PRODUCTO_DELETE":
            return productoDelete(state, action);
        case "ADD_ALL_PRODUCTOS_DELETE":
            return productoDelete(state, action);
        case "REMOVE_PRODUCTOS_SUCCESS":
        case "REMOVE_ALL_PRODUCTOS_DELETE":
        case "FETCH_PRODUCTOS_SUCCESS":
            return [];
        default:
            return state;
    }
};

export default productosDelete;

export const getProductosDeleteSorted = (state) =>
    state.productos.productosDelete.slice().sort();