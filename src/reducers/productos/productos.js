import { combineReducers } from 'redux';
import DataListWrapper from '../../components/FixedDataTable/DataListWrapper';
import onSortChange from '../../components/FixedDataTable/OnSortChange';

const productosById = (state = {}, action) => {
    switch (action.type) {
        case "FETCH_PRODUCTOS_SUCCESS":
            return {...action.response.entities.productos}
        case "ADD_PRODUCTO_SUCCESS":
        case "EDIT_PRODUCTO_SUCCESS": 
            return {
                ...state,
                ...action.response.entities.productos
            };
        case "REMOVE_PRODUCTOS_SUCCESS":
            let copy = Object.assign({}, state);
            for (const idProducto of action.idsProducto) {
                delete copy[idProducto];
            }
            return copy;
        default:
            return state;
    }
};

const allIds = (state = [], action) => {
    switch (action.type) {
        case "FETCH_PRODUCTOS_SUCCESS":
            return action.response.result;
        case "ADD_PRODUCTO_SUCCESS":
            return [...state, action.response.result];
        case "REMOVE_PRODUCTOS_SUCCESS":
            return state.filter(idProducto => !action.idsProducto.includes(idProducto));
        default:
            return state;
    }
};

const sortIndexes = (state = [], action) => {
    switch (action.type) {
        case "FETCH_PRODUCTOS_SUCCESS":
            return action.response.result.map((idProducto, index) => index);
        case "SORT_CHANGE_PRODUCTOS":
            return onSortChange(action.columnKey, action.sortDir, action.productos, state);
        case "ADD_PRODUCTO_SUCCESS":
            return [...state, state.length];
        case "REMOVE_PRODUCTOS_SUCCESS":
            let newIndexes = [];
            for (const i of state) {
                if (i < (state.length - action.idsProducto.length)) {
                    newIndexes.push(i);
                }
            }
            return newIndexes;
        default:
            return state;
    }
};

const colSortDir = (state = {}, action) => {
    switch (action.type) {
        case "SORT_CHANGE_PRODUCTOS":
            return {
                [action.columnKey]: action.sortDir
            }
        case "FETCH_PRODUCTOS_SUCCESS":
            return {};
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case "FETCH_PRODUCTOS_REQUEST":
            return true;
        case "FETCH_PRODUCTOS_SUCCESS":
        case "FETCH_PRODUCTOS_FAILURE":
            return false;
        default:
            return state;
    }
};

const isAdding = (state = false, action) => {
    switch (action.type) {
        case "ADD_PRODUCTO_REQUEST":
            return true;
        case "ADD_PRODUCTO_SUCCESS":
        case "ADD_PRODUCTO_FAILURE":
            return false;
        default:
            return state;
    }
};

const isEditing = (state = false, action) => {
    switch (action.type) {
        case "EDIT_PRODUCTO_REQUEST":
            return true;
        case "EDIT_PRODUCTO_SUCCESS":
        case "EDIT_PRODUCTO_FAILURE":
            return false;
        default:
            return state;
    }
};

const isRemoving = (state = false, action) => {
    switch (action.type) {
        case "REMOVE_PRODUCTOS_REQUEST":
            return true;
        case "REMOVE_PRODUCTOS_SUCCESS":
        case "REMOVE_PRODUCTOS_FAILURE":
            return false;
        default:
            return state;
    }
};

const productos = combineReducers({
    productosById,
    allIds,
    sortIndexes,
    colSortDir,
    isFetching,
    isAdding,
    isEditing,
    isRemoving
});

export default productos;

export const getProductos = (state) =>
    state.productos.productos.allIds.map(idProducto => state.productos.productos.productosById[idProducto]);

export const getProductosListWrapper = (state) => {
    const productosArray = getProductos(state);
    return new DataListWrapper(state.productos.productos.sortIndexes, productosArray);
};

export const getAllIdsSorted = (state) =>
    state.productos.productos.allIds.slice().sort();

export const getColSortDir = (state) =>
    state.productos.productos.colSortDir;

export const getColSortDirColumnKey = (state) => {
    const colSortDir = getColSortDir(state);
    return Object.keys(colSortDir)[0];
}

export const getColSortDirSortDir = (state) => {
    const colSortDir = getColSortDir(state);
    const columnKey = getColSortDirColumnKey(state);
    return colSortDir[columnKey];
}

export const getIsFetching = (state) =>
    state.productos.productos.isFetching;

export const getIsAdding = (state) =>
    state.productos.productos.isAdding;

export const getIsEditing = (state) =>
    state.productos.productos.isEditing;

export const getIsRemoving = (state) =>
    state.productos.productos.isRemoving;