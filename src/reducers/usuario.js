import { combineReducers } from 'redux';

export const isResettingPassword = (state = false, action) => {
    switch (action.type) {
        case "RESET_PASSWORD_REQUEST":
            return true;
        case "RESET_PASSWORD_SUCCESS":
        case "RESET_PASSWORD_FAILURE":
            return false;
        default:
            return state;
    }
}

// ----- Device size -----

export const XS = 'xs';
export const SM = 'sm';

export const deviceSize = (state = 'md', action) => {
    switch (action.type) {
        case "SET_DEVICE_SIZE":
            return action.deviceSize;
        default:
            return state;
    }
}

const usuario = combineReducers({
    isResettingPassword,
    deviceSize
});

export default usuario;

export const getIsResettingPassword = (state) =>
    state.usuario.isResettingPassword;

export const isXsDeviceSize = (state) => 
    state.usuario.deviceSize === XS;

export const isSmDeviceSize = (state) => 
    state.usuario.deviceSize === SM;