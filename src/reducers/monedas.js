import { combineReducers } from 'redux';

const monedasById = (state = {}, action) => {
    switch (action.type) {
        case "FETCH_MONEDAS_SUCCESS":
            return {
                ...state,
                ...action.response.entities.monedas
            };
        default:
            return state;
    }
};

const allIds = (state = [], action) => {
    switch (action.type) {
        case "FETCH_MONEDAS_SUCCESS":
            return action.response.result;
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case "FETCH_MONEDAS_REQUEST":
            return true;
        case "FETCH_MONEDAS_SUCCESS":
        case "FETCH_MONEDAS_FAILURE":
            return false;
        default:
            return state;
    }
};

const monedas = combineReducers({
    monedasById,
    allIds,
    isFetching
});

export default monedas;

export const getMonedas = (state) =>
    state.monedas.allIds.map(idMoneda => state.monedas.monedasById[idMoneda]);

export const getIsFetching = (state) =>
    state.monedas.isFetching;