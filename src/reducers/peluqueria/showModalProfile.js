const showModalProfile = (state = false, action) => {
    switch (action.type) {
        case 'OPEN_MODAL_PROFILE':
            return {
                open: true,
                peluqueria: action.peluqueria
            };
        case 'CLOSE_MODAL_PROFILE':
        case 'EDIT_PROFILE_SUCCESS':
            return {
                open: false,
                peluqueria: null
            };
        default:
            return state;
    }
}

export default showModalProfile;

export const getOpen = (state) =>
    state.peluqueria.showModalProfile.open;

export const getPeluqueria = (state) =>
    state.peluqueria.showModalProfile.peluqueria;