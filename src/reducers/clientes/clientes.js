import { combineReducers } from 'redux';
import DataListWrapper from '../../components/FixedDataTable/DataListWrapper';
import onSortChange from '../../components/FixedDataTable/OnSortChange';

const clientesById = (state = {}, action) => {
    switch (action.type) {
        case "FETCH_CLIENTES_SUCCESS":
            return {...action.response.entities.clientes};
        case "ADD_CLIENTE_SUCCESS":
        case "EDIT_CLIENTE_SUCCESS":
            return {
                ...state,
                ...action.response.entities.clientes
            };
        case "REMOVE_CLIENTES_SUCCESS":
            if (action.idsCliente && action.idsCliente.length) {
                let copy = Object.assign({}, state);
                for (const idCliente of action.idsCliente) {
                    delete copy[idCliente];
                }
                return copy;
            }
            return state;
        default:
            return state;
    }
};

const allIds = (state = [], action) => {
    switch (action.type) {
        case "FETCH_CLIENTES_SUCCESS":
            return action.response.result;
        case "ADD_CLIENTE_SUCCESS":
            return [...state, action.response.result];
        case "REMOVE_CLIENTES_SUCCESS":
            return state.filter(idCliente => !action.idsCliente.includes(idCliente));
        default:
            return state;
    }
};

const sortIndexes = (state = [], action) => {
    switch (action.type) {
        case "FETCH_CLIENTES_SUCCESS":
            return action.response.result.map((idCliente, index) => index);
        case "SORT_CHANGE_CLIENTES":
            return onSortChange(action.columnKey, action.sortDir, action.clientes, state);
        case "ADD_CLIENTE_SUCCESS":
            return [...state, state.length];
        case "REMOVE_CLIENTES_SUCCESS":
            let newIndexes = [];
            for (const i of state) {
                if (i < (state.length - action.idsCliente.length)) {
                    newIndexes.push(i);
                }
            }
            return newIndexes;
        default:
            return state;
    }
};

const colSortDir = (state = {}, action) => {
    switch (action.type) {
        case "SORT_CHANGE_CLIENTES":
            return {
                [action.columnKey]: action.sortDir
            }
        case "FETCH_CLIENTES_SUCCESS":
            return {};
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case "FETCH_CLIENTES_REQUEST":
            return true;
        case "FETCH_CLIENTES_SUCCESS":
        case "FETCH_CLIENTES_FAILURE":
            return false;
        default:
            return state;
    }
};

const isAdding = (state = false, action) => {
    switch (action.type) {
        case "ADD_CLIENTE_REQUEST":
            return true;
        case "ADD_CLIENTE_SUCCESS":
        case "ADD_CLIENTE_FAILURE":
            return false;
        default:
            return state;
    }
};

const isEditing = (state = false, action) => {
    switch (action.type) {
        case "EDIT_CLIENTE_REQUEST":
            return true;
        case "EDIT_CLIENTE_SUCCESS":
        case "EDIT_CLIENTE_FAILURE":
            return false;
        default:
            return state;
    }
};

const isRemoving = (state = false, action) => {
    switch (action.type) {
        case "REMOVE_CLIENTES_REQUEST":
            return true;
        case "REMOVE_CLIENTES_SUCCESS":
        case "REMOVE_CLIENTES_FAILURE":
            return false;
        default:
            return state;
    }
};

const clientes = combineReducers({
    clientesById,
    allIds,
    sortIndexes,
    colSortDir,
    isFetching,
    isAdding,
    isEditing,
    isRemoving
});

export default clientes;

export const getClientes = (state) =>
    state.clientes.clientes.allIds.map(idCliente => state.clientes.clientes.clientesById[idCliente]);

export const getClientesListWrapper = (state) => {
    const clientesArray = getClientes(state);
    return new DataListWrapper(state.clientes.clientes.sortIndexes, clientesArray);
};

export const getAllIdsSorted = (state) =>
    state.clientes.clientes.allIds.slice().sort();

export const getColSortDir = (state) =>
    state.clientes.clientes.colSortDir;

export const getColSortDirColumnKey = (state) => {
    const colSortDir = getColSortDir(state);
    return Object.keys(colSortDir)[0];
}

export const getColSortDirSortDir = (state) => {
    const colSortDir = getColSortDir(state);
    const columnKey = getColSortDirColumnKey(state);
    return colSortDir[columnKey];
}

export const getIsFetching = (state) =>
    state.clientes.clientes.isFetching;

export const getIsAdding = (state) =>
    state.clientes.clientes.isAdding;

export const getIsEditing = (state) =>
    state.clientes.clientes.isEditing;

export const getIsRemoving = (state) =>
    state.clientes.clientes.isRemoving;