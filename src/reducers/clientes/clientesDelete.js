const clienteDelete = (state, action) => {
    switch (action.type) {
        case "ADD_ID_CLIENTE_DELETE":
            return [...state, action.idCliente];
        case "REMOVE_ID_CLIENTE_DELETE":
            return state.filter((idCliente) => idCliente !== action.idCliente);
        case "ADD_ALL_CLIENTES_DELETE":
            return action.clientes.map(cliente => cliente.idCliente);
        default:
            return state;
    }
};

const clientesDelete = (state = [], action) => {
    switch (action.type) {
        case "ADD_ID_CLIENTE_DELETE":
        case "REMOVE_ID_CLIENTE_DELETE":
            return clienteDelete(state, action);
        case "ADD_ALL_CLIENTES_DELETE":
            return clienteDelete(state, action);
        case "REMOVE_CLIENTES_SUCCESS":
        case "REMOVE_ALL_CLIENTES_DELETE":
        case "FETCH_CLIENTES_SUCCESS":
            return [];
        default:
            return state;
    }
};

export default clientesDelete;

export const getClientesDeleteSorted = (state) =>
    state.clientes.clientesDelete.slice().sort();