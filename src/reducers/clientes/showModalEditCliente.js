const showModalEditCliente = (state = {}, action) => {
    switch (action.type) {
        case 'OPEN_MODAL_EDIT_CLIENTE':
            return {
                open: true,
                cliente: action.cliente
            };
        case 'CLOSE_MODAL_EDIT_CLIENTE':
        case 'EDIT_CLIENTE_SUCCESS':
            return {
                open: false,
                cliente: null
            };
        default:
            return state;
    }
}

export default showModalEditCliente;

export const getOpen = (state) =>
    state.clientes.showModalEditCliente.open;

export const getCliente = (state) =>
    state.clientes.showModalEditCliente.cliente;