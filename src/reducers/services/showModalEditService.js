const showModalEditService = (state = {}, action) => {
    switch (action.type) {
        case 'OPEN_MODAL_EDIT_SERVICE':
            return {
                open: true,
                servicio: action.servicio
            };
        case 'CLOSE_MODAL_EDIT_SERVICE':
        case 'EDIT_SERVICE_SUCCESS':
            return {
                open: false,
                servicio: null
            };
        default:
            return state;
    }
}

export default showModalEditService;

export const getOpen = (state) => 
    state.services.showModalEditService.open;

export const getServicio = (state) => 
    state.services.showModalEditService.servicio;