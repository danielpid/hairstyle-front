const serviceDelete = (state, action) => {
    switch (action.type) {
        case "ADD_ID_SERVICE_DELETE":
            return [...state, action.idServicio];
        case "REMOVE_ID_SERVICE_DELETE":
            return state.filter((idServicio) => idServicio !== action.idServicio);
        case "ADD_ALL_SERVICES_DELETE":
            return action.services.map(servicio => servicio.idServicio);
        default:
            return state;
    }
};

const servicesDelete = (state = [], action) => {
    switch (action.type) {
        case "ADD_ID_SERVICE_DELETE":
        case "REMOVE_ID_SERVICE_DELETE":
            return serviceDelete(state, action);
        case "ADD_ALL_SERVICES_DELETE":
            return serviceDelete(state, action);
        case "REMOVE_SERVICES_SUCCESS":
        case "REMOVE_ALL_SERVICES_DELETE":
        case "FETCH_SERVICES_SUCCESS":
            return [];
        default:
            return state;
    }
};

export default servicesDelete;

export const getServicesDeleteSorted = (state) =>
    state.services.servicesDelete.slice().sort();