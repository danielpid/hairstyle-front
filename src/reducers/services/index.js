import { combineReducers } from 'redux';
import services from './services';
import servicesDelete from './servicesDelete';
import showModalAddService from './showModalAddService';
import showModalEditService from './showModalEditService';

const servicesReducers = combineReducers({
    services,
    servicesDelete,
    showModalAddService,
    showModalEditService
});

export default servicesReducers;