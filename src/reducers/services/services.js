import { combineReducers } from 'redux';
import DataListWrapper from '../../components/FixedDataTable/DataListWrapper';
import onSortChange from '../../components/FixedDataTable/OnSortChange';

const servicesById = (state = {}, action) => {
    switch (action.type) {
        case "FETCH_SERVICES_SUCCESS":
            return {...action.response.entities.services}
        case "ADD_SERVICE_SUCCESS":
        case "EDIT_SERVICE_SUCCESS": 
            return {
                ...state,
                ...action.response.entities.services
            };
        case "REMOVE_SERVICES_SUCCESS":
            let copy = Object.assign({}, state);
            for (const idServicio of action.idsServicio) {
                delete copy[idServicio];
            }
            return copy;
        default:
            return state;
    }
};

const allIds = (state = [], action) => {
    switch (action.type) {
        case "FETCH_SERVICES_SUCCESS":
            return action.response.result;
        case "ADD_SERVICE_SUCCESS":
            return [...state, action.response.result];
        case "REMOVE_SERVICES_SUCCESS":
            return state.filter(idServicio => !action.idsServicio.includes(idServicio));
        default:
            return state;
    }
};

const sortIndexes = (state = [], action) => {
    switch (action.type) {
        case "FETCH_SERVICES_SUCCESS":
            return action.response.result.map((idServicio, index) => index);
        case "SORT_CHANGE_SERVICES":
            return onSortChange(action.columnKey, action.sortDir, action.services, state);
        case "ADD_SERVICE_SUCCESS":
            return [...state, state.length];
        case "REMOVE_SERVICES_SUCCESS":
            let newIndexes = [];
            for (const i of state) {
                if (i < (state.length - action.idsServicio.length)) {
                    newIndexes.push(i);
                }
            }
            return newIndexes;
        default:
            return state;
    }
};

const colSortDir = (state = {}, action) => {
    switch (action.type) {
        case "SORT_CHANGE_SERVICES":
            return {
                [action.columnKey]: action.sortDir
            }
        case "FETCH_SERVICES_SUCCESS":
            return {};
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case "FETCH_SERVICES_REQUEST":
            return true;
        case "FETCH_SERVICES_SUCCESS":
        case "FETCH_SERVICES_FAILURE":
            return false;
        default:
            return state;
    }
};

const isAdding = (state = false, action) => {
    switch (action.type) {
        case "ADD_SERVICE_REQUEST":
            return true;
        case "ADD_SERVICE_SUCCESS":
        case "ADD_SERVICE_FAILURE":
            return false;
        default:
            return state;
    }
};

const isEditing = (state = false, action) => {
    switch (action.type) {
        case "EDIT_SERVICE_REQUEST":
            return true;
        case "EDIT_SERVICE_SUCCESS":
        case "EDIT_SERVICE_FAILURE":
            return false;
        default:
            return state;
    }
};

const isRemoving = (state = false, action) => {
    switch (action.type) {
        case "REMOVE_SERVICES_REQUEST":
            return true;
        case "REMOVE_SERVICES_SUCCESS":
        case "REMOVE_SERVICES_FAILURE":
            return false;
        default:
            return state;
    }
};

const services = combineReducers({
    servicesById,
    allIds,
    sortIndexes,
    colSortDir,
    isFetching,
    isAdding,
    isEditing,
    isRemoving
});

export default services;

export const getServices = (state) =>
    state.services.services.allIds.map(idServicio => state.services.services.servicesById[idServicio]);

export const getServicesListWrapper = (state) => {
    const servicesArray = getServices(state);
    return new DataListWrapper(state.services.services.sortIndexes, servicesArray);
};

export const getAllIdsSorted = (state) =>
    state.services.services.allIds.slice().sort();

export const getColSortDir = (state) =>
    state.services.services.colSortDir;

export const getColSortDirColumnKey = (state) => {
    const colSortDir = getColSortDir(state);
    return Object.keys(colSortDir)[0];
}

export const getColSortDirSortDir = (state) => {
    const colSortDir = getColSortDir(state);
    const columnKey = getColSortDirColumnKey(state);
    return colSortDir[columnKey];
}

export const getIsFetching = (state) =>
    state.services.services.isFetching;

export const getIsAdding = (state) =>
    state.services.services.isAdding;

export const getIsEditing = (state) =>
    state.services.services.isEditing;

export const getIsRemoving = (state) =>
    state.services.services.isRemoving;