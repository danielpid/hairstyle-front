import { combineReducers } from 'redux';

const isLoging = (state = false, action) => {
    switch (action.type) {
        case "LOGIN_REQUEST":
            return true;
        case "LOGIN_SUCCESS":
        case "LOGIN_FAILURE":
        case "LOGIN_INVALID_CREDENTIALS":
            return false;
        default:
            return state;
    }
};

const isAuthenticated = (state = false, action) => {
    switch (action.type) {
        case "LOGIN_SUCCESS":
            return true;
        case "LOGIN_REQUEST":
        case "LOGIN_FAILURE":
        case "LOGIN_INVALID_CREDENTIALS":
        case "FORBIDDEN":
            return false;
        default:
            return state;
    }
};

const isInvalidCredentials = (state = false, action) => {
    switch (action.type) {
        case "LOGIN_INVALID_CREDENTIALS":
            return true;
        case "LOGIN_REQUEST":
        case "LOGIN_FAILURE":
        case "LOGIN_SUCCESS":
        case "LOGIN_HIDE_INVALID_CREDENTIALS":
        case "CREATE_ACCOUNT":
        case "FORGOT_PASSWORD":
            return false;
        default:
            return state;
    }
};

const error = (state = false, action) => {
    switch (action.type) {
        case "LOGIN_FAILURE":
            return true;
        case "LOGIN_REQUEST":
        case "LOGIN_SUCCESS":
        case "LOGIN_INVALID_CREDENTIALS":
        case "LOGIN_HIDE_INVALID_CREDENTIALS":
            return false;
        default:
            return state;
    }
}

const login = combineReducers({
    isLoging,
    isAuthenticated,
    isInvalidCredentials,
    error
});

export default login;

export const getIsLoging = (state) =>
    state.login.isLoging

export const getIsAuthenticated = (state) =>
    state.login.isAuthenticated

export const getIsInvalidCredentials = (state) =>
    state.login.isInvalidCredentials

export const getError = (state) =>
    state.login.error