import { combineReducers } from 'redux';

const facturasById = (state = {}, action) => {
    switch (action.type) {
        case "FETCH_FACTURAS_SUCCESS":
            return {...action.response.entities.facturas}
        case "ADD_FACTURA_SUCCESS":
        case "EDIT_FACTURA_SUCCESS":
            return {
                ...state,
                ...action.response.entities.facturas
            };
        case "REMOVE_FACTURA_SUCCESS":
            let copy = Object.assign({}, state);
            delete copy[action.idFactura];
            return copy;
        default:
            return state;
    }
};

const allIds = (state = [], action) => {
    switch (action.type) {
        case "FETCH_FACTURAS_SUCCESS":
            return action.response.result;
        case "ADD_FACTURA_SUCCESS":
            return [...state, action.response.result];
        case "REMOVE_FACTURA_SUCCESS":
            return state.filter(idFactura => action.idFactura !== idFactura);
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case "FETCH_FACTURAS_REQUEST":
            return true;
        case "FETCH_FACTURAS_SUCCESS":
        case "FETCH_FACTURAS_FAILURE":
            return false;
        default:
            return state;
    }
};

const isAdding = (state = false, action) => {
    switch (action.type) {
        case "ADD_FACTURA_REQUEST":
            return true;
        case "ADD_FACTURA_SUCCESS":
        case "ADD_FACTURA_FAILURE":
            return false;
        default:
            return state;
    }
};

const isEditing = (state = false, action) => {
    switch (action.type) {
        case "EDIT_FACTURA_REQUEST":
            return true;
        case "EDIT_FACTURA_SUCCESS":
        case "EDIT_FACTURA_FAILURE":
            return false;
        default:
            return state;
    }
};

const isRemoving = (state = false, action) => {
    switch (action.type) {
        case "REMOVE_FACTURA_REQUEST":
            return true;
        case "REMOVE_FACTURA_SUCCESS":
        case "REMOVE_FACTURA_FAILURE":
            return false;
        default:
            return state;
    }
};

const facturas = combineReducers({
    facturasById,
    allIds,
    isFetching,
    isAdding,
    isEditing,
    isRemoving
});

export default facturas;

export const getFacturas = (state) =>
    state.facturas.facturas.allIds.map(idFactura => state.facturas.facturas.facturasById[idFactura]);

export const getIsFetching = (state) =>
    state.facturas.facturas.isFetching;

export const getIsAdding = (state) =>
    state.facturas.facturas.isAdding;

export const getIsEditing = (state) =>
    state.facturas.facturas.isEditing;

export const getIsRemoving = (state) =>
    state.facturas.facturas.isRemoving;