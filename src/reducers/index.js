import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import login from './login';
import showModalWait from './showModalWait';
import showModalConfirmRemove from './showModalConfirmRemove';
import services from './services';
import productos from './productos';
import clientes from './clientes';
import citas from './citas';
import facturas from './facturas';
import peluqueria from './peluqueria';
import { isSendingEmailResetPassword } from './email';
import usuario from './usuario';
import monedas from './monedas';

const hairStyleApp = combineReducers({
    form: formReducer,
    login,
    showModalWait,
    showModalConfirmRemove,
    services,
    productos,
    clientes,
    citas,
    facturas,
    peluqueria,
    isSendingEmailResetPassword,
    usuario,
    monedas
});

export default hairStyleApp;