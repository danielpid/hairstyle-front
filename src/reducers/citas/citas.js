import { combineReducers } from 'redux';

const citasById = (state = {}, action) => {
    switch (action.type) {
        case "FETCH_CITAS_SUCCESS":
            return {...action.response.entities.citas};
        case "ADD_CITA_SUCCESS":
        case "EDIT_CITA_SUCCESS":
            return {
                ...state,
                ...action.response.entities.citas
            };
        case "REMOVE_CITA_SUCCESS":
            let copy = Object.assign({}, state);
            delete copy[action.idCita];
            return copy;
        default:
            return state;
    }
};

const allIds = (state = [], action) => {
    switch (action.type) {
        case "FETCH_CITAS_SUCCESS":
            return action.response.result;
        case "ADD_CITA_SUCCESS":
            return [...state, action.response.result];
        case "REMOVE_CITA_SUCCESS":
            return state.filter(idCita => action.idCita !== idCita);
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case "FETCH_CITAS_REQUEST":
            return true;
        case "FETCH_CITAS_SUCCESS":
        case "FETCH_CITAS_FAILURE":
            return false;
        default:
            return state;
    }
};

const isAdding = (state = false, action) => {
    switch (action.type) {
        case "ADD_CITA_REQUEST":
            return true;
        case "ADD_CITA_SUCCESS":
        case "ADD_CITA_FAILURE":
            return false;
        default:
            return state;
    }
};

const isEditing = (state = false, action) => {
    switch (action.type) {
        case "EDIT_CITA_REQUEST":
            return true;
        case "EDIT_CITA_SUCCESS":
        case "EDIT_CITA_FAILURE":
            return false;
        default:
            return state;
    }
};

const isRemoving = (state = false, action) => {
    switch (action.type) {
        case "REMOVE_CITA_REQUEST":
            return true;
        case "REMOVE_CITA_SUCCESS":
        case "REMOVE_CITA_FAILURE":
            return false;
        default:
            return state;
    }
};

const citas = combineReducers({
    citasById,
    allIds,
    isFetching,
    isAdding,
    isEditing,
    isRemoving
});

export default citas;

export const getCitas = (state) =>
    state.citas.citas.allIds.map(idCita => state.citas.citas.citasById[idCita]);

export const getIsFetching = (state) =>
    state.citas.citas.isFetching;

export const getIsAdding = (state) =>
    state.citas.citas.isAdding;

export const getIsEditing = (state) =>
    state.citas.citas.isEditing;

export const getIsRemoving = (state) =>
    state.citas.citas.isRemoving;